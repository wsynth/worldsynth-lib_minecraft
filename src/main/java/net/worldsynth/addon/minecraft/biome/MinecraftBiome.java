/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.biome;

import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import net.worldsynth.biome.Biome;
import net.worldsynth.color.WsColor;

@JsonDeserialize(using = MinecraftBiomeJsonDeserializer.class)
public class MinecraftBiome extends Biome {
	
	public static final MinecraftBiome UNKNOWN =
			new MinecraftBiomeBuilder("minecraft:unknown", "Unknown")
			.addNumericAnvilId("1.12", -1)
			.createBiome();
	
	protected TreeMap<String, Integer> versionedAnvilIdNumeric;
	protected TreeMap<String, String> versionedAnvilIdFlattened;
	
	public MinecraftBiome(String idName, String displayName, WsColor color, Set<String> tags, TreeMap<String, Integer> versionedAnvilIdNumeric, TreeMap<String, String> versionedAnvilIdFlattened) {
		super(idName, displayName, color, tags);
		
		this.versionedAnvilIdNumeric = versionedAnvilIdNumeric;
		this.versionedAnvilIdFlattened = versionedAnvilIdFlattened;
	}
	
	public Integer getNumericAnvilId(String versionKey) {
		if (MinecraftBiomeProfile.anvilVersionKeyComarator.compare(versionKey, "1.18") >= 0) {
			throw new IllegalArgumentException("Cannot get numeric anvil biome id for a versions newer than or equal to 1.18");
		}
		
		Entry<String, Integer> e = versionedAnvilIdNumeric.floorEntry(versionKey);
		if (e != null) {
			return e.getValue();
		}
		else {
			return -1;
		}
	}
	
	public String getFlattenedAnvilId(String versionKey) {
		if (MinecraftBiomeProfile.anvilVersionKeyComarator.compare(versionKey, "1.18") < 0) {
			throw new IllegalArgumentException("Cannot get flattened anvil biome id for a versions older than 1.18");
		}
		
		Entry<String, String> e = versionedAnvilIdFlattened.floorEntry(versionKey);
		if (e != null) {
			return e.getValue();
		}
		else {
			return idName;
		}
	}
}
