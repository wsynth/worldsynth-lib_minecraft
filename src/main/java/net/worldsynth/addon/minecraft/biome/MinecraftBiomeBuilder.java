/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.biome;

import java.io.File;
import java.util.TreeMap;

import com.fasterxml.jackson.databind.JsonNode;

import net.worldsynth.addon.minecraft.material.MinecraftMaterialProfile;
import net.worldsynth.biome.BiomeBuilder;

public class MinecraftBiomeBuilder extends BiomeBuilder<MinecraftBiome> {
	
	protected TreeMap<String, Integer> anvilIdNumeric = new TreeMap<String, Integer>(MinecraftMaterialProfile.anvilVersionKeyComarator);
	protected TreeMap<String, String> anvilIdFlattened = new TreeMap<String, String>(MinecraftMaterialProfile.anvilVersionKeyComarator);
	
	public MinecraftBiomeBuilder(String idName, String displayname) {
		super(idName, displayname);
	}

	public MinecraftBiomeBuilder(String idName, JsonNode node, File biomesFile) {
		super(idName, node, biomesFile);
		
		//Anvil id
		node.fields().forEachRemaining(t -> {
			if (t.getKey().matches("anvil_1\\.(\\d+\\.?)+")) {
				String versionKey = t.getKey().substring(6);
				if (MinecraftMaterialProfile.anvilVersionKeyComarator.compare(versionKey, "1.18") < 0) {
					anvilIdNumeric.put(versionKey, t.getValue().get("id").asInt());
				}
				else {
					anvilIdFlattened.put(versionKey, t.getValue().get("id").asText());
				}
			}
		});
	}
	
	public MinecraftBiomeBuilder addNumericAnvilId(String versionKey, int id) {
		anvilIdNumeric.put(versionKey, id);
		return this;
	}
	
	public MinecraftBiome createBiome() {
		return new MinecraftBiome(idName, displayName, color, tags, anvilIdNumeric, anvilIdFlattened);
	}
}
