/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.biome;

import java.util.Comparator;
import java.util.HashMap;
import java.util.TreeMap;

import com.fasterxml.jackson.core.type.TypeReference;

import net.worldsynth.addon.minecraft.material.MinecraftMaterialProfile;
import net.worldsynth.biome.BiomeProfile;

public class MinecraftBiomeProfile extends BiomeProfile<MinecraftBiome> {
	
	public static Comparator<String> anvilVersionKeyComarator = (String versionKey1, String versionKey2) -> {
		if (versionKey1.equals(versionKey2)) {
			return 0;
		}
		String[] versionKey1parts = versionKey1.split("\\.");
		String[] versionKey2parts = versionKey2.split("\\.");

		for (int i = 0; i < Math.min(versionKey1parts.length, versionKey2parts.length); i++) {
			int keyPart1Value = Integer.parseInt(versionKey1parts[i]);
			int keyPart2Value = Integer.parseInt(versionKey2parts[i]);

			if (keyPart1Value != keyPart2Value) {
				return keyPart1Value - keyPart2Value;
			}
		}

		return versionKey1parts.length - versionKey2parts.length;
	};

	private TreeMap<String, NumericLookup> numericVersionLookups = new TreeMap<String, NumericLookup>(MinecraftMaterialProfile.anvilVersionKeyComarator);
	private TreeMap<String, FlattenedLookup> flattenedVersionLookups = new TreeMap<String, FlattenedLookup>(anvilVersionKeyComarator);

	public MinecraftBiomeProfile() {
		super("minecraft");
	}
	
	@Override
	protected TypeReference<MinecraftBiome> getBiomeTypeReference() {
		return new TypeReference<MinecraftBiome>() {};
	}
	
	public synchronized FlattenedLookup getFlattenedLookup(String versionKey) {
		if (!flattenedVersionLookups.containsKey(versionKey)) {
			flattenedVersionLookups.put(versionKey, new FlattenedLookup(versionKey));
		}
		return flattenedVersionLookups.get(versionKey);
	}
	
	public synchronized NumericLookup getNumericLookup(String versionKey) {
		if (!numericVersionLookups.containsKey(versionKey)) {
			numericVersionLookups.put(versionKey, new NumericLookup(versionKey));
		}
		return numericVersionLookups.get(versionKey);
	}
	
	public class FlattenedLookup {
		private HashMap<String, MinecraftBiome> flattenedIdBiomesMaps;
		
		public FlattenedLookup(String versionKey) {
			if (anvilVersionKeyComarator.compare(versionKey, "1.18") < 0) {
				throw new IllegalArgumentException("Cannot create a flattened biome lookup for a versions older than 1.18");
			}
			
			flattenedIdBiomesMaps = new HashMap<String, MinecraftBiome>(biomes.size());
			for (MinecraftBiome biome: biomes.values()) {
				if (biome.getFlattenedAnvilId(versionKey) != null) {
					flattenedIdBiomesMaps.put(biome.getFlattenedAnvilId(versionKey), biome);
				}
			}
		}
		
		public MinecraftBiome getBiome(String id) {
			return flattenedIdBiomesMaps.get(id);
		}
	}
	
	public class NumericLookup {
		private HashMap<Integer, MinecraftBiome> numericIdBiomesMaps;
		
		NumericLookup(String versionKey) {
			if (anvilVersionKeyComarator.compare(versionKey, "1.18") >= 0) {
				throw new IllegalArgumentException("Cannot create a numeric biome lookup for a versions newer than or equal to 1.18");
			}
			
			numericIdBiomesMaps = new HashMap<Integer, MinecraftBiome>(biomes.size());
			for (MinecraftBiome biome: biomes.values()) {
				if (biome.getNumericAnvilId(versionKey) != null) {
					numericIdBiomesMaps.put(biome.getNumericAnvilId(versionKey), biome);
				}
			}
		}
		
		public MinecraftBiome getBiome(int id) {
			return numericIdBiomesMaps.get(id);
		}
	}
}
