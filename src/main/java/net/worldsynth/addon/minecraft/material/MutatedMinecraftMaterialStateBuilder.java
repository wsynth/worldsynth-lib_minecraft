/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.material;

import com.github.steveice10.opennbt.tag.builtin.CompoundTag;

import net.worldsynth.addon.minecraft.material.anvildata.VersionedMaterialStateDataFlattened;
import net.worldsynth.addon.minecraft.material.anvildata.VersionedMaterialStateDataNumeric;
import net.worldsynth.color.WsColor;
import net.worldsynth.material.StateProperties;
import net.worldsynth.material.Texture;

public class MutatedMinecraftMaterialStateBuilder {
	
	protected MinecraftMaterial material;
	
	// From MaterialState
	protected StateProperties properties = new StateProperties();
	protected boolean isDefault;
	
	protected String displayName;
	protected WsColor color;
	protected Texture texture;
	
	// From MinecraftMaterialState
	protected VersionedMaterialStateDataNumeric parentVersionedDataNumeric;
	protected VersionedMaterialStateDataFlattened parentVersionedDataFlattened;
	
	// Overriding anvil data
	protected CompoundTag tileEntity;
	protected CompoundTag tileTick;
	protected CompoundTag liquidTick;
	
	protected Integer lightLevel;
	protected Integer lightDampening;
	
	public MutatedMinecraftMaterialStateBuilder(MinecraftMaterialState state) {
		material = state.getMaterial();
		
		properties = state.getProperties();
		isDefault = state.isDefault();
		displayName = state.getDisplayName();
		color = state.getWsColor();
		texture = state.getTexture();
		
		parentVersionedDataNumeric = state.versionedDataNumeric;
		parentVersionedDataFlattened = state.versionedDataFlattened;
		
		lightLevel = state.lightLevel;
		lightDampening = state.lightDampening;
	}
	
	/**
	 * Sets the tile entity of the mutated state for all MC versions
	 */
	public MutatedMinecraftMaterialStateBuilder setTileEntity(CompoundTag tileEntity) {
		this.tileEntity = tileEntity;
		return this;
	}
	
	/**
	 * Sets the tile tick of the mutated state for all MC versions
	 */
	public MutatedMinecraftMaterialStateBuilder setTileTick(CompoundTag tileTick) {
		this.tileTick = tileTick;
		return this;
	}
	
	/**
	 * Sets the liquid tick of the mutated state for all MC versions
	 */
	public MutatedMinecraftMaterialStateBuilder setLiquidTick(CompoundTag liquidTick) {
		this.liquidTick = liquidTick;
		return this;
	}
	
	/**
	 * Sets the light level of the mutated state for all MC versions
	 */
	public MutatedMinecraftMaterialStateBuilder lightLevel(int lightLevel) {
		this.lightLevel = lightLevel;
		return this;
	}
	
	/**
	 * Sets the light dampening of the mutated state for all MC versions
	 */
	public MutatedMinecraftMaterialStateBuilder lightDampening(int lightDampening) {
		this.lightDampening = lightDampening;
		return this;
	}
	
	public MinecraftMaterialState createMaterialState() {
		VersionedMaterialStateDataNumeric versionedDataNumeric = new VersionedMaterialStateDataNumeric(parentVersionedDataNumeric, tileEntity, tileTick, liquidTick);
		VersionedMaterialStateDataFlattened versionedDataFlattened = new VersionedMaterialStateDataFlattened(parentVersionedDataFlattened, tileEntity, tileTick, liquidTick);
		
		MinecraftMaterialState state = new MinecraftMaterialState(properties, isDefault, displayName, color, texture, versionedDataNumeric, versionedDataFlattened, lightLevel, lightDampening);
		state.setMaterial(material);
		return state;
	}
}
