/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.material.anvildata;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.github.steveice10.opennbt.tag.builtin.CompoundTag;

public class VersionedMaterialDataFlattened {
	
	private String defaultId;
	private CompoundTag defaultTileEntity;
	private CompoundTag defaultTileTick;
	private CompoundTag defaultLiquidTick;
	
	private boolean inheritanceBuilt;
	private TreeMap<String, AnvilDataFlattened> anvilData;
	
	public VersionedMaterialDataFlattened(TreeMap<String, AnvilDataFlattened> anvilData, String defaultId, CompoundTag defaultTileEntity, CompoundTag defaultTileTick, CompoundTag defaultLiquidTick) {
		this.anvilData = anvilData;
		
		this.defaultId = defaultId;
		this.defaultTileEntity = defaultTileEntity;
		this.defaultTileTick = defaultTileTick;
		this.defaultLiquidTick = defaultLiquidTick;
		
		buildInheritedAnvilData();
	}
	
	public AnvilDataFlattened getAnvilData(String versionKey) {
		if (!inheritanceBuilt) {
			throw new IllegalStateException("Inheritance isn't built");
		}
		return anvilData.floorEntry(versionKey).getValue();
	}
	
	Map<String, AnvilDataFlattened> getAnvilDataMap() {
		if (!inheritanceBuilt) {
			throw new IllegalStateException("Inheritance isn't built");
		}
		return anvilData;
	}
	
	private void buildInheritedAnvilData() {
		if (inheritanceBuilt) {
			throw new IllegalStateException("Inheritance is already built");
		}
		AnvilDataFlattened parentData = new AnvilDataFlattened(defaultId, null, defaultTileEntity, defaultTileTick, defaultLiquidTick);
		if (!anvilData.containsKey("1.13")) {
			anvilData.put("1.13", parentData);
		}
		
		for (Entry<String, AnvilDataFlattened> entry: anvilData.entrySet()) {
			entry.setValue(createInheritedData(parentData, entry.getValue()));
			parentData = entry.getValue();
		}
		
		inheritanceBuilt = true;
	}
	
	private AnvilDataFlattened createInheritedData(AnvilDataFlattened parent, AnvilDataFlattened childDiff) {
		CompoundTag tileEntity = childDiff.getTileEntity();
		CompoundTag tileTick = childDiff.getTileTick();
		CompoundTag liquidTick = childDiff.getLiquidTick();
		
		if (tileEntity == null) {
			tileEntity = parent.getTileEntity();
		}
		
		if (tileTick == null) {
			tileTick = parent.getTileTick();
		}
		
		if (liquidTick == null) {
			liquidTick = parent.getLiquidTick();
		}
		
		String id = childDiff.getId();
		
		if (id == null) {
			id = parent.getId();
		}
		
		return new AnvilDataFlattened(id, null, tileEntity, tileTick, liquidTick);
	}
}
