/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.material.anvildata;

import java.io.File;
import java.util.LinkedHashMap;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;

import net.worldsynth.material.StateProperties;

public class AnvilDataFlattened extends AnvilData {
	protected String id;
	protected StateProperties properties;
	
	public AnvilDataFlattened(String id, StateProperties properties, CompoundTag tileEntity, CompoundTag tileTick, CompoundTag liquidTick) {
		super(tileEntity, tileTick, liquidTick);
		this.id = id;
		this.properties = properties;
	}
	
	public AnvilDataFlattened(JsonNode node, File materialsFile) {
		super(node, materialsFile);
		
		if (node.has("id")) {
			id = node.get("id").asText();
		}
		
		if (node.has("properties")) {
			LinkedHashMap<String, String> properties = new ObjectMapper().convertValue(node.get("properties"), new TypeReference<LinkedHashMap<String, String>>(){});
			this.properties = new StateProperties(properties);
		}
	}
	
	public String getId() {
		return id;
	}
	
	public StateProperties getProperties() {
		return properties;
	}
}
