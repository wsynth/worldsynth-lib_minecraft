/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.material.anvildata;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.github.steveice10.opennbt.tag.builtin.CompoundTag;

public class VersionedMaterialStateDataNumeric {
	
	private int defaultId;
	private byte defaultMeta;
	private CompoundTag defaultTileEntity;
	private CompoundTag defaultTileTick;
	private CompoundTag defaultLiquidTick;
	
	private boolean inheritanceBuilt;
	private TreeMap<String, AnvilDataNumeric> anvilData;
	
	public VersionedMaterialStateDataNumeric(TreeMap<String, AnvilDataNumeric> anvilData, int defaultId, byte defaultMeta, CompoundTag defaultTileEntity, CompoundTag defaultTileTick, CompoundTag defaultLiquidTick) {
		this.anvilData = anvilData;
		
		this.defaultId = defaultId;
		this.defaultMeta = defaultMeta;
		this.defaultTileEntity = defaultTileEntity;
		this.defaultTileTick = defaultTileTick;
		this.defaultLiquidTick = defaultLiquidTick;
	}
	
	/**
	 * Creates a mutated version of the parent instance with the additions and changes from the overriding data.
	 * 
	 * @param parent
	 * @param overridingData
	 */
	@SuppressWarnings("unchecked")
	public VersionedMaterialStateDataNumeric(VersionedMaterialStateDataNumeric parent, Map<String, AnvilDataNumeric> overridingData) {
		this.anvilData = (TreeMap<String, AnvilDataNumeric>) parent.anvilData.clone();
		
		this.defaultId = parent.defaultId;
		this.defaultMeta = parent.defaultMeta;
		this.defaultTileEntity = parent.defaultTileEntity;
		this.defaultTileTick = parent.defaultTileTick;
		this.defaultLiquidTick = parent.defaultLiquidTick;
		
		overridingData.forEach((version, data) -> {
			anvilData.put(version, data);
		});
	}
	
	/**
	 * Creates a mutated version of the parent instance where tile entity, tile tick and liquid tick is replaced for all versions
	 * @param parent
	 * @param tileEntity
	 * @param tileTick
	 * @param liquidTick
	 */
	public VersionedMaterialStateDataNumeric(VersionedMaterialStateDataNumeric parent, CompoundTag tileEntity, CompoundTag tileTick, CompoundTag liquidTick) {
		anvilData = new TreeMap<String, AnvilDataNumeric>();
		
		this.defaultId = parent.defaultId;
		this.defaultMeta = parent.defaultMeta;
		this.defaultTileEntity = tileEntity;
		this.defaultTileTick = tileTick;
		this.defaultLiquidTick = liquidTick;
		
		parent.anvilData.forEach((version, data) -> {
			anvilData.put(version, new AnvilDataNumeric(data.id, data.meta, tileEntity, tileTick, liquidTick));
		});
		
		// Since this is a mutation of existing data, the inheritance should already have been built for the parent
		inheritanceBuilt = true;
	}
	
	public AnvilDataNumeric getAnvilData(String versionKey) {
		if (!inheritanceBuilt) {
			throw new IllegalStateException("Inheritance isn't built");
		}
		return anvilData.floorEntry(versionKey).getValue();
	}
	
	public void buildInheritedAnvilData(VersionedMaterialDataNumeric parentMaterialData) {
		if (inheritanceBuilt) {
			throw new IllegalStateException("Inheritance is already built");
		}
		AnvilDataNumeric defaultData = new AnvilDataNumeric(defaultId, defaultMeta, defaultTileEntity, defaultTileTick, defaultLiquidTick);
		
		//Inherit the existence knowledge of versions from parent material
		for (Entry<String, AnvilDataNumeric> entry: parentMaterialData.getAnvilDataMap().entrySet()) {
			if (!anvilData.containsKey(entry.getKey())) {
				anvilData.put(entry.getKey(), new AnvilDataNumeric(null, null, null, null, null));
			}
		}
		
		if (anvilData.floorEntry("1.12") == null) {
			anvilData.put("1.12", defaultData);
		}
		
		for (Entry<String, AnvilDataNumeric> entry: anvilData.entrySet()) {
			entry.setValue(createInheritedData(parentMaterialData.getAnvilData(entry.getKey()), defaultData, entry.getValue()));
			defaultData = entry.getValue();
		}
		
		inheritanceBuilt = true;
	}
	
	private boolean inheritTileEntityFromState = false;
	private boolean inheritTileTickFromState = false;
	private boolean inheritLiquidTickFromState = false;
	private boolean inheritNumericIdFromState = false;
	private boolean inheritNumericMetaFromState = false;
	
	private AnvilDataNumeric createInheritedData(AnvilDataNumeric materialParent, AnvilDataNumeric stateParent, AnvilDataNumeric childDiff) {
		//Tile entity inheritance
		CompoundTag tileEntity = childDiff.getTileEntity();
		if (tileEntity == null) {
			//When there is no value for tile entity, it has not been assigned in the current state data version and should therefore be inherited.
			if (inheritTileEntityFromState) {
				//If tile entity has at some previous point been assigned in a state data version, then it should be inherit from the parent state data version.
				tileEntity = stateParent.getTileEntity();
			}
			else {
				//If tile entity has never previously been assigned in a state data version, then it should be inherited from the parent material data version.
				tileEntity = materialParent.getTileEntity();
			}
		}
		else {
			//When there is a value for tile entity, it has been assigned in the current state data version and should not be inherited.
			//Further inheritance of tile entity after this should be done from parent state data.
			inheritTileEntityFromState = true;
		}
		
		//Tile tick inheritance
		CompoundTag tileTick = childDiff.getTileTick();
		if (tileTick == null) {
			//When there is no value for tile tick, it has not been assigned in the current state data version and should therefore be inherited.
			if (inheritTileTickFromState) {
				//If tile tick has at some previous point been assigned in a state data version, then it should be inherit from the parent state data version.
				tileTick = stateParent.getTileTick();
			}
			else {
				//If tile tick has never previously been assigned in a state data version, then it should be inherited from the parent material data version.
				tileTick = materialParent.getTileTick();
			}
		}
		else {
			//When there is a value for tile tick, it has been assigned in the current state data version and should not be inherited.
			//Further inheritance of tile tick after this should be done from parent state data.
			inheritTileTickFromState = true;
		}
		
		//Liquid tick inheritance
		CompoundTag liquidTick = childDiff.getLiquidTick();
		if (liquidTick == null) {
			//When there is no value for liquid tick, it has not been assigned in the current state data version and should therefore be inherited.
			if (inheritLiquidTickFromState) {
				//If liquid tick has at some previous point been assigned in a state data version, then it should be inherit from the parent state data version.
				liquidTick = stateParent.getLiquidTick();
			}
			else {
				//If liquid tick has never previously been assigned in a state data version, then it should be inherited from the parent material data version.
				liquidTick = materialParent.getLiquidTick();
			}
		}
		else {
			//When there is a value for liquid tick, it has been assigned in the current state data version and should not be inherited.
			//Further inheritance of liquid tick after this should be done from parent state data.
			inheritLiquidTickFromState = true;
		}
		
		//Numeric id inheritance
		Integer id = childDiff.getId();
		if (id == null) {
			//When there is no value for numeric id (aka -1), it has not been assigned in the current state data version and should therefore be inherited.
			if (inheritNumericIdFromState) {
				//If numeric id has at some previous point been assigned in a state data version, then it should be inherit from the parent state data version.
				id = stateParent.getId();
			}
			else {
				//If numeric id has never previously been assigned in a state data version, then it should be inherited from the parent material data version.
				id = materialParent.getId();
			}
		}
		else {
			//When there is a value for numeric id (aka not -1), it has been assigned in the current state data version and should not be inherited.
			//Further inheritance of numeric id after this should be done from parent state data.
			inheritNumericIdFromState = true;
		}
		
		//Numeric meta inheritance
		Byte meta = childDiff.getMeta();
		if (meta == null) {
			//When there is no value for numeric meta (aka -1), it has not been assigned in the current state data version and should therefore be inherited.
			if (inheritNumericMetaFromState) {
				//If numeric meta has at some previous point been assigned in a state data version, then it should be inherit from the parent state data version.
				meta = stateParent.getMeta();
			}
			else {
				//If numeric meta has never previously been assigned in a state data version, then it should be inherited from the parent material data version.
				meta = materialParent.getMeta();
			}
		}
		else {
			//When there is a value for numeric meta (aka not -1), it has been assigned in the current state data version and should not be inherited.
			//Further inheritance of numeric meta after this should be done from parent state data.
			inheritNumericIdFromState = true;
		}
		
		return new AnvilDataNumeric(id, meta, tileEntity, tileTick, liquidTick);
	}
}
