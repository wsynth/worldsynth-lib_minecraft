/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.material.anvildata;

import java.io.File;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;

public class AnvilDataNumeric extends AnvilData {
	protected Integer id;
	protected Byte meta;
	
	public AnvilDataNumeric(Integer id, Byte meta, CompoundTag tileEntity, CompoundTag tileTick, CompoundTag liquidTick) {
		super(tileEntity, tileTick, liquidTick);
		this.id = id;
		this.meta = meta;
	}
	
	public AnvilDataNumeric(JsonNode node, File materialsFile) {
		super(node, materialsFile);
		
		if (node.has("id")) {
			id = node.get("id").asInt();
		}
		
		if (node.has("meta")) {
			meta = (byte) node.get("meta").asInt();
		}
	}
	
	public Integer getId() {
		return id;
	}
	
	public Byte getMeta() {
		return meta;
	}
}
