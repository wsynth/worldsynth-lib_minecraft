/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.material.anvildata;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.github.steveice10.opennbt.tag.builtin.CompoundTag;

public class VersionedMaterialDataNumeric {
	
	private int defaultId;
	private byte defaultMeta;
	private CompoundTag defaultTileEntity;
	private CompoundTag defaultTileTick;
	private CompoundTag defaultLiquidTick;
	
	private boolean inheritanceBuilt;
	private TreeMap<String, AnvilDataNumeric> anvilData;
	
	public VersionedMaterialDataNumeric(TreeMap<String, AnvilDataNumeric> anvilData, int defaultId, byte defaultMeta, CompoundTag defaultTileEntity, CompoundTag defaultTileTick, CompoundTag defaultLiquidTick) {
		this.anvilData = anvilData;
		
		this.defaultId = defaultId;
		this.defaultMeta = defaultMeta;
		this.defaultTileEntity = defaultTileEntity;
		this.defaultTileTick = defaultTileTick;
		this.defaultLiquidTick = defaultLiquidTick;
		
		buildInheritedAnvilData();
	}
	
	public AnvilDataNumeric getAnvilData(String versionKey) {
		if (!inheritanceBuilt) {
			throw new IllegalStateException("Inheritance isn't built");
		}
		return anvilData.floorEntry(versionKey).getValue();
	}
	
	Map<String, AnvilDataNumeric> getAnvilDataMap() {
		if (!inheritanceBuilt) {
			throw new IllegalStateException("Inheritance isn't built");
		}
		return anvilData;
	}
	
	private void buildInheritedAnvilData() {
		if (inheritanceBuilt) {
			throw new IllegalStateException("Inheritance is already built");
		}
		AnvilDataNumeric parentData = new AnvilDataNumeric(defaultId, defaultMeta, defaultTileEntity, defaultTileTick, defaultLiquidTick);
		if (anvilData.isEmpty()) {
			anvilData.put("1.12", parentData);
		}
		
		for (Entry<String, AnvilDataNumeric> entry: anvilData.entrySet()) {
			entry.setValue(createInheritedData(parentData, entry.getValue()));
			parentData = entry.getValue();
		}
		
		inheritanceBuilt = true;
	}
	
	private AnvilDataNumeric createInheritedData(AnvilDataNumeric parent, AnvilDataNumeric childDiff) {
		CompoundTag tileEntity = childDiff.getTileEntity();
		CompoundTag tileTick = childDiff.getTileTick();
		CompoundTag liquidTick = childDiff.getLiquidTick();
		
		if (tileEntity == null) {
			tileEntity = parent.getTileEntity();
		}
		
		if (tileTick == null) {
			tileTick = parent.getTileTick();
		}
		
		if (liquidTick == null) {
			liquidTick = parent.getLiquidTick();
		}
		
		Integer id = childDiff.getId();
		if (id == null) {
			id = parent.getId();
		}
		
		Byte meta = childDiff.getMeta();
		if (meta == null) {
			meta = parent.getMeta();
		}
		
		return new AnvilDataNumeric(id, meta, tileEntity, tileTick, liquidTick);
	}
}
