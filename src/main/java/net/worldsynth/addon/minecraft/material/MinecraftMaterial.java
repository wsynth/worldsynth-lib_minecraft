/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.material;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import net.worldsynth.addon.minecraft.material.anvildata.AnvilDataFlattened;
import net.worldsynth.addon.minecraft.material.anvildata.AnvilDataNumeric;
import net.worldsynth.addon.minecraft.material.anvildata.VersionedMaterialDataFlattened;
import net.worldsynth.addon.minecraft.material.anvildata.VersionedMaterialDataNumeric;
import net.worldsynth.color.WsColor;
import net.worldsynth.material.Material;
import net.worldsynth.material.Texture;

@JsonDeserialize(using = MinecraftMaterialJsonDeserializer.class)
public class MinecraftMaterial extends Material<MinecraftMaterial, MinecraftMaterialState> {
	
	public static final MinecraftMaterial AIR =
			new MinecraftMaterialBuilder("minecraft:air", "Air")
			.addNumericAnvilData("1.12", new AnvilDataNumeric(0, (byte) 0, null, null, null))
			.addFlattenedAnvilData("1.13", new AnvilDataFlattened("minecraft:air", null, null, null, null))
			.lightDampening(0)
			.isAir(true)
			.createMaterial();
	public static final MinecraftMaterial BEDROCK =
			new MinecraftMaterialBuilder("minecraft:bedrock", "Bedrock")
			.addNumericAnvilData("1.12", new AnvilDataNumeric(7, (byte) 0, null, null, null))
			.addFlattenedAnvilData("1.13", new AnvilDataFlattened("minecraft:bedrock", null, null, null, null))
			.isAir(false)
			.color(83, 83, 83)
			.createMaterial();
	
	protected VersionedMaterialDataNumeric versionedDataNumeric;
	protected VersionedMaterialDataFlattened versionedDataFlattened;
	
	protected int lightLevel;
	protected int lightDampening;
	
	protected MinecraftMaterial(
			String idName,
			String displayName,
			WsColor color,
			Texture texture,
			Set<String> tags,
			boolean isAir,
			Map<String, List<String>> properties,
			Set<MinecraftMaterialState> states,
			VersionedMaterialDataNumeric versionedDataNumeric,
			VersionedMaterialDataFlattened versionedDataFlattened,
			int lightLevel,
			int lightDampening) {
		super(idName, displayName, color, texture, tags, isAir, properties, states);
		
		this.versionedDataNumeric = versionedDataNumeric;
		this.versionedDataFlattened = versionedDataFlattened;
		
		this.lightLevel = lightLevel;
		this.lightDampening = lightDampening;
		
		for (MinecraftMaterialState state: states) {
			state.versionedDataNumeric.buildInheritedAnvilData(versionedDataNumeric);
			state.versionedDataFlattened.buildInheritedAnvilData(versionedDataFlattened);
		}
	}
	
	public AnvilDataNumeric getNumericAnvilData(String versionKey) {
		if (MinecraftMaterialProfile.anvilVersionKeyComarator.compare(versionKey, "1.13") >= 0) {
			throw new IllegalArgumentException("Cannot get numeric anvil material data for a versions newer than or equal to 1.13");
		}
		return versionedDataNumeric.getAnvilData(versionKey);
	}
	
	public AnvilDataFlattened getFlattenedAnvilData(String versionKey) {
		if (MinecraftMaterialProfile.anvilVersionKeyComarator.compare(versionKey, "1.13") < 0) {
			throw new IllegalArgumentException("Cannot get flattened anvil materrial data for a versions older than 1.13");
		}
		return versionedDataFlattened.getAnvilData(versionKey);
	}
	
	public int getLightLevel() {
		return lightLevel;
	}
	
	public int getLightDampening() {
		return lightDampening;
	}
}
