/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.material;

import java.util.Comparator;
import java.util.HashMap;
import java.util.TreeMap;

import com.fasterxml.jackson.core.type.TypeReference;

import net.worldsynth.addon.minecraft.material.anvildata.AnvilDataFlattened;
import net.worldsynth.addon.minecraft.material.anvildata.AnvilDataNumeric;
import net.worldsynth.material.MaterialProfile;
import net.worldsynth.material.StateProperties;

public class MinecraftMaterialProfile extends MaterialProfile<MinecraftMaterial, MinecraftMaterialState> {
	
	public static Comparator<String> anvilVersionKeyComarator = (String versionKey1, String versionKey2) -> {
		if (versionKey1.equals(versionKey2)) {
			return 0;
		}
		String[] versionKey1parts = versionKey1.split("\\.");
		String[] versionKey2parts = versionKey2.split("\\.");

		for (int i = 0; i < Math.min(versionKey1parts.length, versionKey2parts.length); i++) {
			int keyPart1Value = Integer.parseInt(versionKey1parts[i]);
			int keyPart2Value = Integer.parseInt(versionKey2parts[i]);

			if (keyPart1Value != keyPart2Value) {
				return keyPart1Value - keyPart2Value;
			}
		}

		return versionKey1parts.length - versionKey2parts.length;
	};
	
	private TreeMap<String, NumericLookup> numericVersionLookups = new TreeMap<String, NumericLookup>(anvilVersionKeyComarator);
	private TreeMap<String, FlattenedLookup> flattenedVersionLookups = new TreeMap<String, FlattenedLookup>(anvilVersionKeyComarator);
	
	public MinecraftMaterialProfile() {
		super("minecraft");
		addMaterial(MinecraftMaterial.AIR);
		addMaterial(MinecraftMaterial.BEDROCK);
	}
	
	@Override
	protected TypeReference<MinecraftMaterial> getMaterialTypeReference() {
		return new TypeReference<MinecraftMaterial>() {};
	}
	
	public synchronized FlattenedLookup getFlattenedLookup(String versionKey) {
		if (!flattenedVersionLookups.containsKey(versionKey)) {
			flattenedVersionLookups.put(versionKey, new FlattenedLookup(versionKey));
		}
		return flattenedVersionLookups.get(versionKey);
	}
	
	public synchronized NumericLookup getNumericLookup(String versionKey) {
		if (!numericVersionLookups.containsKey(versionKey)) {
			numericVersionLookups.put(versionKey, new NumericLookup(versionKey));
		}
		return numericVersionLookups.get(versionKey);
	}
	
	public class FlattenedLookup {
		private HashMap<String, HashMap<StateProperties, MinecraftMaterialState>> flattenedIdPropStatesMaps; //Map<id, Map<properties, state>>
		
		public FlattenedLookup(String versionKey) {
			if (anvilVersionKeyComarator.compare(versionKey, "1.13") < 0) {
				throw new IllegalArgumentException("Cannot create a flattened material lookup for a versions older than 1.13");
			}
			
			flattenedIdPropStatesMaps = new HashMap<String, HashMap<StateProperties, MinecraftMaterialState>>(materials.size());
			for (MinecraftMaterialState state: materialStates.values()) {
				if (state.getFlattenedAnvilData(versionKey) != null) {
					addState(state, versionKey);
				}
			}
		}
		
		private void addState(MinecraftMaterialState state, String versionKey) {
			AnvilDataFlattened data = state.getFlattenedAnvilData(versionKey);
			if (!flattenedIdPropStatesMaps.containsKey(data.getId())) {
				flattenedIdPropStatesMaps.put(data.getId(), new HashMap<StateProperties, MinecraftMaterialState>());
			}
			flattenedIdPropStatesMaps.get(data.getId()).put(data.getProperties(), state);
		}
		
		public MinecraftMaterialState getMaterialState(String id, StateProperties propertiesKey) {
			HashMap<StateProperties, MinecraftMaterialState> numericPropertiesStatesMap = flattenedIdPropStatesMaps.get(id);
			
			if (numericPropertiesStatesMap == null) return null;
			return numericPropertiesStatesMap.get(propertiesKey);
		}
	}
	
	public class NumericLookup {
		private HashMap<Integer, HashMap<Byte, MinecraftMaterialState>> numericIdMetaStatesMaps; //Map<id, Map<meta, state>>
		
		public NumericLookup(String versionKey) {
			if (anvilVersionKeyComarator.compare(versionKey, "1.13") >= 0) {
				throw new IllegalArgumentException("Cannot create a numeric material lookup for a versions newer than or equal to 1.13");
			}
			
			numericIdMetaStatesMaps = new HashMap<Integer, HashMap<Byte, MinecraftMaterialState>>(materials.size());
			for (MinecraftMaterialState state: materialStates.values()) {
				if (state.getNumericAnvilData(versionKey) != null) {
					addState(state, versionKey);
				}
			}
		}
		
		private void addState(MinecraftMaterialState state, String versionKey) {
			AnvilDataNumeric data = state.getNumericAnvilData(versionKey);
			if (!numericIdMetaStatesMaps.containsKey(data.getId())) {
				numericIdMetaStatesMaps.put(data.getId(), new HashMap<Byte, MinecraftMaterialState>());
			}
			numericIdMetaStatesMaps.get(data.getId()).put(data.getMeta(), state);
		}
		
		public MinecraftMaterialState getMaterialState(int id, byte meta) {
			HashMap<Byte, MinecraftMaterialState> numericMetaStatesMap = numericIdMetaStatesMaps.get(id);
			
			if (numericMetaStatesMap == null) return null;
			return numericMetaStatesMap.get(meta);
		}
	}
}
