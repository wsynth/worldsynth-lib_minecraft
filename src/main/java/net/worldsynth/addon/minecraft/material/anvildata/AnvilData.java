/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.material.anvildata;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.steveice10.opennbt.NBTIO;
import com.github.steveice10.opennbt.SNBTIO;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;

public abstract class AnvilData {
	
	protected CompoundTag tileEntity;
	protected CompoundTag tileTick;
	protected CompoundTag liquidTick;
	
	public AnvilData(CompoundTag tileEntity, CompoundTag tileTick, CompoundTag liquidTick) {
		this.tileEntity = tileEntity;
		this.tileTick = tileTick;
		this.liquidTick = liquidTick;
	}
	
	public AnvilData(JsonNode node, File materialsFile) {
		if (node.has("tileEntity")) {
			File tileEntityFile = new File(materialsFile.getParent(), node.get("tileEntity").asText());
			if (tileEntityFile != null && tileEntityFile.exists()) {
				try {
					tileEntity = readNbt(tileEntityFile);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		if (node.has("tileTick")) {
			File tileTickFile = new File(materialsFile.getParent(), node.get("tileTick").asText());
			if (tileTickFile != null && tileTickFile.exists()) {
				try {
					tileTick = readNbt(tileTickFile);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		if (node.has("liquidTick")) {
			File liquidTickFile = new File(materialsFile.getParent(), node.get("liquidTick").asText());
			if (liquidTickFile != null && liquidTickFile.exists()) {
				try {
					liquidTick = readNbt(liquidTickFile);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private CompoundTag readNbt(File nbtFile) throws IOException {
		if (nbtFile.getName().endsWith(".snbt")) {
			return SNBTIO.readFile(nbtFile);
		}
		else if (nbtFile.getName().endsWith(".nbt")) {
			return NBTIO.readFile(nbtFile);
		}
		return null;
	}
	
	public CompoundTag getTileEntity() {
		return tileEntity;
	}
	
	public CompoundTag getTileTick() {
		return tileTick;
	}
	
	public CompoundTag getLiquidTick() {
		return liquidTick;
	}
}
