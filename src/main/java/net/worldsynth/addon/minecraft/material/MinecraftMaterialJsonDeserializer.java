/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.material;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;

import net.worldsynth.material.MaterialJsonDeserializer;

public class MinecraftMaterialJsonDeserializer extends MaterialJsonDeserializer<MinecraftMaterial, MinecraftMaterialState> {
	private static final long serialVersionUID = -7192999005961077454L;

	protected MinecraftMaterialJsonDeserializer() {
		super();
	}

	@Override
	public MinecraftMaterial deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		JsonNode node = jp.getCodec().readTree(jp);
		String idName = (String) ctxt.findInjectableValue("idName", null, null);
		File materialsFile = (File) ctxt.findInjectableValue("materialsFile", null, null);
		return new MinecraftMaterialBuilder(idName, node, materialsFile).createMaterial();
	}
}
