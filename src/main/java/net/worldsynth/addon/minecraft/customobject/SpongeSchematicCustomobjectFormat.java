/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.customobject;

import com.github.steveice10.opennbt.NBTIO;
import com.github.steveice10.opennbt.tag.builtin.*;
import net.worldsynth.addon.minecraft.material.MinecraftMaterial;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialProfile;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialProfile.FlattenedLookup;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialState;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilVersion;
import net.worldsynth.customobject.Block;
import net.worldsynth.customobject.CustomObject;
import net.worldsynth.customobject.CustomObjectFormat;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.Property;
import net.worldsynth.material.StateProperties;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

// Sponge schematic specification
// https://github.com/SpongePowered/Schematic-Specification
public class SpongeSchematicCustomobjectFormat extends CustomObjectFormat {
	
	@Override
	public CustomObject readObjectFromFile(File file) throws IOException {
		CompoundTag schematicCompound = NBTIO.readFile(file);
		
		int version = ((IntTag) schematicCompound.get(TAG_VERSION)).getValue();
		
		switch (version) {
		case 1:
			return parseObjectV1(schematicCompound);
		case 2:
			return parseObjectV2(schematicCompound);
		case 3:
			return parseObjectV3(schematicCompound);
		default:
			throw new UnsupportedOperationException("No implementation for sponge schematic version " + version);
		}
	}
	
	// V1
	protected static final String TAG_VERSION				= "Version";
	protected static final String TAG_METADATA				= "Metadata";
	protected static final String TAG_WIDTH					= "Width";
	protected static final String TAG_HEIGHT				= "Height";
	protected static final String TAG_LENGTH				= "Length";
	protected static final String TAG_OFFSET				= "Offset";
	protected static final String TAG_PALETTE_MAX			= "PaletteMax";
	protected static final String TAG_PALETTE				= "Palette";
	protected static final String TAG_BLOCK_DATA			= "BlockData";
	protected static final String TAG_TILE_ENTITIES			= "TileEntities";
	
	// Metadata tags
	protected static final String TAG_NAME					= "Name";
	protected static final String TAG_AUTHOR 				= "Author";
	protected static final String TAG_DATE					= "Date";
	protected static final String TAG_REQUIRED_MODS			= "RequiredMods";
	
	private CustomObject parseObjectV1(CompoundTag schematicCompound) {
		int dataVersion = AnvilVersion.VERSION_1_13_2.getDataVersion();
		
		int width = ((ShortTag) schematicCompound.get(TAG_WIDTH)).getValue() & 0xFFFF;
		int height = ((ShortTag) schematicCompound.get(TAG_HEIGHT)).getValue() & 0xFFFF;
		int length = ((ShortTag) schematicCompound.get(TAG_LENGTH)).getValue() & 0xFFFF;
		
		int[] offset = {0, 0, 0};
		if (schematicCompound.contains(TAG_OFFSET)) {
			offset = ((IntArrayTag) schematicCompound.get(TAG_OFFSET)).getValue();
		}
		
		CompoundTag paletteTag = schematicCompound.get(TAG_PALETTE);
		ByteArrayTag blockDataTag = schematicCompound.get(TAG_BLOCK_DATA);
		
		return makeCustomObject(width, height, length, offset, dataVersion, paletteTag, blockDataTag);
	}
	
	// V2
	protected static final String TAG_DATA_VERSION			= "DataVersion";
	protected static final String TAG_BLOCK_ENTITIES		= "BlockEntities";
	protected static final String TAG_ENTITIES				= "Entities";
	protected static final String TAG_BIOME_PALETTE_MAX		= "BiomePaletteMax";
	protected static final String TAG_BIOME_PALETTE			= "BiomePalette";
	protected static final String TAG_BIOME_DATA			= "BiomeData";
	
	private CustomObject parseObjectV2(CompoundTag schematicCompound) {
		int dataVersion = ((IntTag) schematicCompound.get(TAG_DATA_VERSION)).getValue();
		
		int width = ((ShortTag) schematicCompound.get(TAG_WIDTH)).getValue() & 0xFFFF;
		int height = ((ShortTag) schematicCompound.get(TAG_HEIGHT)).getValue() & 0xFFFF;
		int length = ((ShortTag) schematicCompound.get(TAG_LENGTH)).getValue() & 0xFFFF;
		
		int[] offset = {0, 0, 0};
		if (schematicCompound.contains(TAG_OFFSET)) {
			offset = ((IntArrayTag) schematicCompound.get(TAG_OFFSET)).getValue();
		}
		if (schematicCompound.contains(TAG_METADATA)) {
			// Look for special case with WorldEdit having different offsets for V2
			CompoundTag metadata = schematicCompound.get(TAG_METADATA);
			if (metadata.contains("WEOffsetX")) {
				offset[0] = ((IntTag) metadata.get("WEOffsetX")).getValue();
			}
			if (metadata.contains("WEOffsetY")) {
				offset[1] = ((IntTag) metadata.get("WEOffsetY")).getValue();
			}
			if (metadata.contains("WEOffsetZ")) {
				offset[2] = ((IntTag) metadata.get("WEOffsetZ")).getValue();
			}
		}
		
		CompoundTag paletteTag = schematicCompound.get(TAG_PALETTE);
		ByteArrayTag blockDataTag = schematicCompound.get(TAG_BLOCK_DATA);
		
		return makeCustomObject(width, height, length, offset, dataVersion, paletteTag, blockDataTag);
	}

	public void writeV2toFile(File file, CustomObject object, AnvilVersion anvilVersion) throws IOException {
		CompoundTag schematicCompound = createSchematicV2(object, anvilVersion);
		NBTIO.writeFile(schematicCompound, file);
	}

	private CompoundTag createSchematicV2(CustomObject object, AnvilVersion anvilVersion) {
		CompoundTag schematicCompound = new CompoundTag("");

		schematicCompound.put(new IntTag(TAG_VERSION, 2));
		schematicCompound.put(new IntTag(TAG_DATA_VERSION, anvilVersion.getDataVersion()));

		schematicCompound.put(new ShortTag(TAG_LENGTH, (short) object.length));
		schematicCompound.put(new ShortTag(TAG_HEIGHT, (short) object.height));
		schematicCompound.put(new ShortTag(TAG_WIDTH, (short) object.width));

		int[] offset = {0, 0, 0};
		schematicCompound.put(new IntArrayTag(TAG_OFFSET, offset));

		MaterialPalette mPalette = new MaterialPalette(anvilVersion);
		int[] blockData = new int[object.width * object.height * object.length];
		Arrays.fill(blockData, mPalette.getPaletteIndex(MinecraftMaterial.AIR.getDefaultState()));
		for (Block b: object.blocks) {
			int paletteIndex = mPalette.getPaletteIndex((MinecraftMaterialState) b.material);
			blockData[(b.x - object.xMin) + (b.z - object.zMin) * object.width + (b.y - object.yMin) * object.width * object.length] = paletteIndex;
		}

		schematicCompound.put(mPalette.toNbt());
		schematicCompound.put(new ByteArrayTag(TAG_BLOCK_DATA, encodeVarInt(blockData)));

		return schematicCompound;
	}
	
	// V3
	protected static final String TAG_BLOCKS				= "Blocks";
	protected static final String TAG_BIOMES				= "Biomes";
	protected static final String TAG_DATA					= "Data";
	
	private CustomObject parseObjectV3(CompoundTag schematicCompound) {
		int dataVersion = ((IntTag) schematicCompound.get(TAG_DATA_VERSION)).getValue();
		
		int width = ((ShortTag) schematicCompound.get(TAG_WIDTH)).getValue() & 0xFFFF;
		int height = ((ShortTag) schematicCompound.get(TAG_HEIGHT)).getValue() & 0xFFFF;
		int length = ((ShortTag) schematicCompound.get(TAG_LENGTH)).getValue() & 0xFFFF;
		
		int[] offset = {0, 0, 0};
		if (schematicCompound.contains(TAG_OFFSET)) {
			offset = ((IntArrayTag) schematicCompound.get(TAG_OFFSET)).getValue();
		}
		
		CompoundTag blockContainerTag = schematicCompound.get(TAG_BLOCKS);
		CompoundTag paletteTag = blockContainerTag.get(TAG_PALETTE);
		ByteArrayTag blockData = blockContainerTag.get(TAG_DATA);
		
		return makeCustomObject(width, height, length, offset, dataVersion, paletteTag, blockData);
	}

	@Override
	public String formatName() {
		return "Sponge Schematic";
	}

	@Override
	public String formatSuffix() {
		return "schem";
	}
	
	private CustomObject makeCustomObject(int width, int height, int length, int[] offset, int dataVersion, CompoundTag paletteTag, ByteArrayTag blocksTag) {
		MaterialPalette palette = new MaterialPalette(dataVersion, paletteTag);
		ArrayList<Integer> blocksData = decodeVarInt(blocksTag.getValue());
		
		ArrayList<Block> blocks = new ArrayList<>();
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				for (int z = 0; z < length; z++) {
					int index = x + z * width + y * width * length;
					MinecraftMaterialState material = palette.getMaterialState(blocksData.get(index));
					blocks.add(new Block(x+offset[0], y+offset[1], z+offset[2], material));
				}
			}
		}
		
		return new CustomObject(blocks.toArray(new Block[0]));
	}
	
	private ArrayList<Integer> decodeVarInt(byte[] varIntBytes) {
		ArrayList<Integer> ints = new ArrayList<>();

        int i = 0;
        int value = 0;
        int varint_length = 0;
        while (i < varIntBytes.length) {
            value = 0;
            varint_length = 0;

            while (true) {
                value |= (varIntBytes[i] & 127) << (varint_length++ * 7);
                if (varint_length > 5) {
                    throw new RuntimeException("VarInt too big (probably corrupted data)");
                }
                if ((varIntBytes[i] & 128) != 128) {
                    i++;
                    break;
                }
                i++;
            }

            ints.add(value);
        }

        return ints;
	}

	private byte[] encodeVarInt(int[] ints) {
		ArrayList<Byte> varIntBytes = new ArrayList<>();

		for (int val: ints) {
			while ((val & -128) != 0) {
				varIntBytes.add((byte) (val & 127 | 128));
				val >>>= 7;
			}
			varIntBytes.add((byte) val);
		}

		byte[] byteArray = new byte[varIntBytes.size()];
		for (int i = 0; i < byteArray.length; i++) {
			byteArray[i] = varIntBytes.get(i);
		}

		return byteArray;
	}

	private class MaterialPalette {
		private final AnvilVersion anvilVersion;
		private final HashMap<MinecraftMaterialState, Integer> paletteMap = new HashMap<>();
		private final HashMap<Integer, MinecraftMaterialState> paletteInverseMap = new HashMap<>();

		public MaterialPalette(AnvilVersion anvilVersion) {
			this.anvilVersion = anvilVersion;
		}

		private MaterialPalette(int dataVersion, CompoundTag paletteTag) {
			MinecraftMaterialProfile minecraftMaterialProfile = (MinecraftMaterialProfile) MaterialRegistry.getProfile("minecraft");
			anvilVersion = AnvilVersion.getVersionByDataVersion(dataVersion);
			FlattenedLookup flattenedLookup = minecraftMaterialProfile.getFlattenedLookup(anvilVersion.getMaterialVersionName());

			paletteTag.forEach(tag -> {
				int paletteIndex = (Integer) tag.getValue();
				MinecraftMaterialState materialState = lookupMaterial(tag.getName(), flattenedLookup);

				paletteMap.put(materialState, paletteIndex);
				paletteInverseMap.put(paletteIndex, materialState);
			});
		}

		private MinecraftMaterialState lookupMaterial(String materialKey, FlattenedLookup flattenedLookup) {
			String id = materialKey;
			ArrayList<Property> properties = new ArrayList<Property>();

			if (materialKey.contains("[")) {
				id = materialKey.substring(0, materialKey.indexOf("["));
				String[] propertyStrings = materialKey.substring(materialKey.indexOf("[")+1, materialKey.length()-1).split(",");
				for (String ps: propertyStrings) {
					String[] pair = ps.split("=");
					Property p = new Property(pair[0], pair[1]);
					properties.add(p);
				}
			}

			return flattenedLookup.getMaterialState(id, new StateProperties(properties));
		}

		public int getPaletteIndex(MinecraftMaterialState materialState) {
			if (!paletteMap.containsKey(materialState)) {
				int newIndex = paletteMap.size();
				paletteMap.put(materialState, newIndex);
				paletteInverseMap.put(newIndex, materialState);
			}
			return paletteMap.get(materialState);
		}

		public MinecraftMaterialState getMaterialState(int paletteIndex) {
			return paletteInverseMap.get(paletteIndex);
		}

		public int getPaletteSize() {
			return paletteMap.size();
		}

		public CompoundTag toNbt() {
			CompoundTag paletteCompound = new CompoundTag(TAG_PALETTE);
			paletteMap.forEach((material, index) -> {
				paletteCompound.put(new IntTag(material.getIdName(), index));
			});

			return paletteCompound;
		}
	}
}
