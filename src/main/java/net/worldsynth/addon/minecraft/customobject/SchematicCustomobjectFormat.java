/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.customobject;

import com.github.steveice10.opennbt.NBTIO;
import com.github.steveice10.opennbt.tag.builtin.*;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialBuilder;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialProfile;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialProfile.NumericLookup;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialState;
import net.worldsynth.addon.minecraft.material.anvildata.AnvilDataNumeric;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilVersion;
import net.worldsynth.customobject.Block;
import net.worldsynth.customobject.CustomObject;
import net.worldsynth.customobject.CustomObjectFormat;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.MaterialState;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class SchematicCustomobjectFormat extends CustomObjectFormat {
	
	protected static final String TAG_WIDTH                = "Width";
	protected static final String TAG_HEIGHT               = "Height";
	protected static final String TAG_LENGTH               = "Length";
	protected static final String TAG_MATEIRALS            = "Materials";
	protected static final String TAG_BLOCKS               = "Blocks";
	protected static final String TAG_ADD_BLOCKS           = "AddBlocks";
	@Deprecated
	/** Use TAG_ADD_BLOCKS */
	protected static final String TAG_ADD                  = "Add";
	protected static final String TAG_DATA                 = "Data";
	protected static final String TAG_ENTITIES             = "Entities";
	protected static final String TAG_TILE_ENTITIES        = "TileEntities";
	protected static final String TAG_ICON                 = "Icon";
	protected static final String TAG_SCHEMATICA_MAPPING   = "SchematicaMapping";
	protected static final String TAG_EXTENDED_METADATA    = "ExtendedMetadata";
	protected static final String TAG_WE_ORIGIN_X          = "WEOriginX";
	protected static final String TAG_WE_ORIGIN_Y          = "WEOriginY";
	protected static final String TAG_WE_ORIGIN_Z          = "WEOriginZ";
	protected static final String TAG_WE_OFFSET_X          = "WEOffsetX";
	protected static final String TAG_WE_OFFSET_Y          = "WEOffsetY";
	protected static final String TAG_WE_OFFSET_Z          = "WEOffsetZ";
	protected static final String TAG_ITEM_STACK_VERSION   = "itemStackVersion";
	protected static final String TAG_BLOCK_IDS            = "BlockIDs";
	protected static final String TAG_ITEM_IDS             = "ItemIDs";
	protected static final String TAG_TILE_TICKS           = "TileTicks";
	protected static final String TAG_BIOMES               = "Biomes";
	
	@Override
	public CustomObject readObjectFromFile(File file) throws IOException {
		CompoundTag schematicCompound = NBTIO.readFile(file);
		
		int width = (short) schematicCompound.get(TAG_WIDTH).getValue();
		int height = (short) schematicCompound.get(TAG_HEIGHT).getValue();
		int length = (short) schematicCompound.get(TAG_LENGTH).getValue();
		
		byte[] schematicBlocks = (byte[]) schematicCompound.get(TAG_BLOCKS).getValue();
		
		byte[] schematicBlocksAdd = null;
		if (schematicCompound.contains(TAG_ADD_BLOCKS)) {
			schematicBlocksAdd = (byte[]) schematicCompound.get(TAG_ADD_BLOCKS).getValue();
		}
		
		byte[] schematicData = null;
		if (schematicCompound.contains(TAG_DATA)) {
			schematicData = (byte[]) schematicCompound.get(TAG_DATA).getValue();
		}
		
		ArrayList<Block> blocks = new ArrayList<Block>();
		
		MinecraftMaterialProfile minecratMaterialProfile = (MinecraftMaterialProfile) MaterialRegistry.getProfile("minecraft");
		NumericLookup numericLookup = minecratMaterialProfile.getNumericLookup(AnvilVersion.VERSION_1_12_2.getMaterialVersionName());
		
		for (int y = 0; y < height; y++) {
			for (int z = 0; z < length; z++) {
				for (int x = 0; x < width; x++) {
					int index = (y * length + z) * width + x;
					
					int blockId = schematicBlocks[index] & 0xFF;
					
					if (schematicBlocksAdd != null) {
						byte blockAdd = schematicBlocksAdd[index/2];
						if (index % 2 == 0) {
							int idAdd = blockAdd & 0x0F;
							blockId = blockId | (idAdd << 8);
						}
						else {
							int idAdd = blockAdd & 0xF0;
							blockId = blockId | (idAdd << 4);
						}
					}
					
					byte blockData = 0;
					if (schematicData != null) {
						blockData = schematicData[index];
					}
					
					MinecraftMaterialState material = numericLookup.getMaterialState(blockId, blockData);
					if (material == null) {
						MinecraftMaterialBuilder materialBuilder = new MinecraftMaterialBuilder(blockId + ":" + blockData, "Unknown " + blockId + ":" + blockData);
						material = materialBuilder.new MinecraftMaterialStateBuilder(true).addNumericAnvilData(AnvilVersion.VERSION_1_12_2.getMaterialVersionName(), new AnvilDataNumeric(blockId, blockData, null, null, null)).createMaterialState();
						materialBuilder.createMaterial();
					}
					
					blocks.add(new Block(x, y, z, material));
				}
			}
		}
		
		return new CustomObject(blocks.toArray(new Block[0]));
	}

	public void writeObjectToFile(File file, CustomObject object) throws IOException {
		int width = object.width;
		int height = object.height;
		int length = object.length;
		
		int arrayLen = width * height * length;
		byte[] schematicBlocks = new byte[arrayLen];
		byte[] schematicData = new byte[arrayLen];

		int addArrayLen = arrayLen % 2 == 0 ? arrayLen / 2 : arrayLen / 2 + 1;
		byte[] schematicBlocksAdd = new byte[addArrayLen];
		
		boolean useAdd = false;
		
		for (Block block: object.blocks) {
			int bx = block.x - object.xMin;
			int by = block.y - object.yMin;
			int bz = block.z - object.zMin;
			
			int index = (by * length + bz) * width + bx;
			
			MaterialState<?, ?> ms = block.material;
			if (ms instanceof MinecraftMaterialState mms) {

				int id = mms.getNumericAnvilId("1.12.2");
				int meta = mms.getNumericAnvilMeta("1.12.2");
				
				schematicBlocks[index] = (byte)(id & 0xFF);
				schematicData[index] = (byte) meta;
				
				if (id >= 256) {
					useAdd = true;
					int addIndex = index/2;
					int addMod = index % 2;
					
					if (addMod == 0) {
						schematicBlocksAdd[addIndex] |= (byte) ((id >> 8) & 0x0F);
					}
					else {
						schematicBlocksAdd[addIndex] |= (byte) ((id >> 4) & 0xF0);
					}
				}
			}
		}
		
		// Create biomes array
		byte[] biomes = new byte[width * length];
		Arrays.fill(biomes, (byte) 1);
		
		CompoundTag schematicCompound = new CompoundTag("Schematic");
		schematicCompound.put(new ShortTag(TAG_WIDTH, (short) width));
		schematicCompound.put(new ShortTag(TAG_HEIGHT, (short) height));
		schematicCompound.put(new ShortTag(TAG_LENGTH, (short) length));
		
		schematicCompound.put(new ByteArrayTag(TAG_BLOCKS, schematicBlocks));
		schematicCompound.put(new ByteArrayTag(TAG_DATA, schematicData));
		schematicCompound.put(new ByteArrayTag(TAG_BIOMES, biomes));
		if (useAdd) {
			schematicCompound.put(new ByteArrayTag(TAG_ADD_BLOCKS, schematicBlocksAdd));
		}
		
		schematicCompound.put(new StringTag(TAG_MATEIRALS, "Alpha"));
		
		schematicCompound.put(new ListTag(TAG_ENTITIES));
		schematicCompound.put(new ListTag(TAG_TILE_ENTITIES));
		schematicCompound.put(new ListTag(TAG_TILE_TICKS));
		
		NBTIO.writeFile(schematicCompound, file);
	}

	@Override
	public String formatName() {
		return "Schematic";
	}

	@Override
	public String formatSuffix() {
		return "schematic";
	}

}
