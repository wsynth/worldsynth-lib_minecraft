/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.customobject;

import net.worldsynth.addon.minecraft.material.MinecraftMaterialBuilder;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialProfile;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialProfile.NumericLookup;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialState;
import net.worldsynth.addon.minecraft.material.anvildata.AnvilDataNumeric;
import net.worldsynth.customobject.Block;
import net.worldsynth.customobject.CustomObject;
import net.worldsynth.customobject.CustomObjectFormat;
import net.worldsynth.material.MaterialRegistry;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class Bo2CustomObjectFormat extends CustomObjectFormat {
	
//	public String version;
//	public String spawnOnBlockType;
//	public boolean spawnSunlight;
//	public boolean spawnDarkness;
//	public boolean spawnWater;
//	public boolean spawnLava;
//	public boolean underFill;
//	public boolean dig;
//	public boolean needsFoundation;
//	public int rarity;
//	public int collisionPercentage;
//	public int spawnElevationMin;
//	public int spawnElevationMax;
//	public boolean randomRotation;
//	public String groupId;
//	public boolean tree;
//	public boolean branch;
//	public boolean diggingBranch;
//	public int branchLimit;
//	public int groupFrequencyMin;
//	public int groupFrequencyMax;
//	public int groupSeperationMin;
//	public int groupSeperationMax;

	@Override
	public CustomObject readObjectFromFile(File file) throws IOException {
		//Create a byte buffer to read the file into
		byte[] bo2ConentBuffer = new byte[(int) file.length()];
		
		//Read the file into the byte buffer
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);
			fis.read(bo2ConentBuffer);
		} finally {
			fis.close();
		}
		
		//Convert the byte buffer to a string and remove the byte buffer from memory
		String bo2TextFormat = new String(bo2ConentBuffer);
		
		NumericLookup numericLookup = ((MinecraftMaterialProfile) MaterialRegistry.getProfile("minecraft")).getNumericLookup("1.12.2");
		ArrayList<Block> blocks = new ArrayList<Block>();
		
		boolean meta = false;
		boolean data = false;
		String[] separated = bo2TextFormat.split("\\r?\\n");
		
		for (String line: separated) {

			if (line.matches("\\[META\\]")) {
				meta = true;
				data = false;
			}
			else if (line.matches("\\[DATA\\]")) {
				meta = false;
				data = true;
			}
			
			//Perform this if in meta section of file
			if (meta) {
//				//fix regex expression
//				if (line.matches("version=.*")) {
//					version = line.substring(line.indexOf('='));
//				}
//				else if (line.matches("spawnOnBlockType=[0-9]+")) {
//					spawnOnBlockType = line.substring(line.indexOf('='));
//				}
//				else if (line.matches("spawnSunlight=True|False")) {
//					spawnSunlight = Boolean.parseBoolean(line.substring(line.indexOf('=')));
//				}
//				else if (line.matches("spawnDarkness=True|False")) {
//					spawnDarkness = Boolean.parseBoolean(line.substring(line.indexOf('=')));
//				}
//				else if (line.matches("spawnWater=True|False")) {
//					spawnWater = Boolean.parseBoolean(line.substring(line.indexOf('=')));
//				}
//				else if (line.matches("spawnLava=True|False")) {
//					spawnLava = Boolean.parseBoolean(line.substring(line.indexOf('=')));
//				}
//				else if (line.matches("underFill=True|False")) {
//					underFill = Boolean.parseBoolean(line.substring(line.indexOf('=')));
//				}
//				else if (line.matches("dig=True|False")) {
//					dig = Boolean.parseBoolean(line.substring(line.indexOf('=')));
//				}
//				else if (line.matches("needsFoundation=True|False")) {
//					needsFoundation = Boolean.parseBoolean(line.substring(line.indexOf('=')));
//				}
//				else if (line.matches("rarity=[0-9][0-9]?[0-9]?|1000")) {
//					rarity = Integer.parseInt(line.substring(line.indexOf('=')));
//				}
//				else if (line.matches("collisionPercentage=[0-9][0-9]?)|100")) {
//					collisionPercentage = Integer.parseInt(line.substring(line.indexOf('=')));
//				}
//				else if (line.matches("spawnElevationMin=[0-9][0-9]?|1([0-1][0-9]|2[0-8])")) {
//					spawnElevationMin = Integer.parseInt(line.substring(line.indexOf('=')));
//				}
//				else if (line.matches("spawnElevationMax=[0-9][0-9]?|1([0-1][0-9]|2[0-8])")) {
//					spawnElevationMax = Integer.parseInt(line.substring(line.indexOf('=')));
//				}
//				else if (line.matches("randomRotation=True|False")) {
//					randomRotation = Boolean.parseBoolean(line.substring(line.indexOf('=')));
//				}
//				else if (line.matches("groupId=.*")) {
//					groupId = line.substring(line.indexOf('='));
//				}
//				else if (line.matches("tree=True|False")) {
//					tree = Boolean.parseBoolean(line.substring(line.indexOf('=')));
//				}
//				else if (line.matches("branch=True|False")) {
//					branch = Boolean.parseBoolean(line.substring(line.indexOf('=')));
//				}
//				else if (line.matches("diggingBranch=True|False")) {
//					diggingBranch = Boolean.parseBoolean(line.substring(line.indexOf('=')));
//				}
//				else if (line.matches("branchLimit=[0-9]|1[0-6]")) {
//					branchLimit = Integer.parseInt(line.substring(line.indexOf('=')));
//				}
//				else if (line.matches("groupFrequencyMin=[0-9][0-9]?)|100")) {
//					groupFrequencyMin = Integer.parseInt(line.substring(line.indexOf('=')));
//				}
//				else if (line.matches("groupFrequencyMax=[0-9][0-9]?)|100")) {
//					groupFrequencyMax = Integer.parseInt(line.substring(line.indexOf('=')));
//				}
//				else if (line.matches("groupSeperationMin=[0-9]|1[0-6]")) {
//					groupSeperationMin = Integer.parseInt(line.substring(line.indexOf('=')));
//				}
//				else if (line.matches("groupSeperationMax=[0-9]|1[0-6]")) {
//					groupSeperationMax = Integer.parseInt(line.substring(line.indexOf('=')));
//				}
//				else if (line.matches("spawnInBiome=.*")) {
//					//Make read list of biomes or "All"
//				}
			}
			
			//Perform this if in data section of file
			else if (data) {
				if (line.matches("-?[0-9]+,-?[0-9]+,-?[0-9]+:[0-9]+\\.(([0-9]|1[0-6])(#[0-3]@([1-9][0-9]?|100))?)?")) {
					String[] s1 = line.split(":");
					String[] s2 = s1[0].split(",");
					int z = Integer.parseInt(s2[0]);
					int x = Integer.parseInt(s2[1]);
					int y = Integer.parseInt(s2[2]);
					String[] s3 = s1[1].split("\\.");
					int blockId = Integer.parseInt(s3[0]);
					byte blockMeta = 0;
					if (s3.length > 1) {
						String[] s4 = s3[1].split("#");
						blockMeta = Byte.parseByte(s4[0]);
						if (s4.length > 1) {
							//If containing branch options
						}
					}
					MinecraftMaterialState material = numericLookup.getMaterialState(blockId, blockMeta);
					if (material == null) {
						MinecraftMaterialBuilder materialBuilder = new MinecraftMaterialBuilder(blockId + ":" + blockMeta, "Unknown " + blockId + ":" + blockMeta);
						material = materialBuilder.new MinecraftMaterialStateBuilder(true).addNumericAnvilData("1.12.2", new AnvilDataNumeric(blockId, blockMeta, null, null, null)).createMaterialState();
						materialBuilder.createMaterial();
					}
					blocks.add(new Block(x, y, z, material));
				}
			}
			else {
				//What to do if there is not meta or data
			}
		}
		
		return new CustomObject(blocks.toArray(new Block[blocks.size()]));
	}

	@Override
	public String formatName() {
		return "BiomeObject2";
	}

	@Override
	public String formatSuffix() {
		return "bo2";
	}

}
