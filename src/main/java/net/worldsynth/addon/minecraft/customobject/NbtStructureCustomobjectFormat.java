/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.customobject;

import com.github.steveice10.opennbt.NBTIO;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.IntTag;
import com.github.steveice10.opennbt.tag.builtin.ListTag;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialState;
import net.worldsynth.addon.minecraft.material.MutatedMinecraftMaterialStateBuilder;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilVersion;
import net.worldsynth.addon.minecraft.minecraft.anvil.chunk.MaterialPalette;
import net.worldsynth.customobject.Block;
import net.worldsynth.customobject.CustomObject;
import net.worldsynth.customobject.CustomObjectFormat;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class NbtStructureCustomobjectFormat extends CustomObjectFormat {

	protected static final String TAG_DATA_VERSION			= "DataVersion";
	protected static final String TAG_SIZE					= "size";
	protected static final String TAG_PALETTE				= "palette";
	protected static final String TAG_BLOCKS				= "blocks";
	protected static final String TAG_STATE					= "state";
	protected static final String TAG_POS					= "pos";
	protected static final String TAG_NBT					= "bnt";
	protected static final String TAG_ENTITIES				= "entities";

	@Override
	public CustomObject readObjectFromFile(File file) throws IOException {
		CompoundTag structureCompound = NBTIO.readFile(file);

		int dataVersion = ((IntTag) structureCompound.get(TAG_DATA_VERSION)).getValue();

		ListTag sizeTag = structureCompound.get(TAG_SIZE);
		int width = ((IntTag) sizeTag.get(0)).getValue();
		int height = ((IntTag) sizeTag.get(1)).getValue();
		int length = ((IntTag) sizeTag.get(2)).getValue();

		ListTag paletteTag = structureCompound.get(TAG_PALETTE);
		ListTag blocksTag = structureCompound.get(TAG_BLOCKS);

		AnvilVersion anvilVersion = AnvilVersion.getVersionByDataVersion(dataVersion);
		MaterialPalette palette = new MaterialPalette(anvilVersion, paletteTag);

		ArrayList<Block> blocks = new ArrayList<>();
		blocksTag.forEach(tag -> {
			CompoundTag blockTag = (CompoundTag) tag;

			int state = ((IntTag) blockTag.get(TAG_STATE)).getValue();
			MinecraftMaterialState material = palette.getMaterialState(state);

			ListTag posTag = blockTag.get(TAG_POS);
			int x = ((IntTag) posTag.get(0)).getValue();
			int y = ((IntTag) posTag.get(1)).getValue();
			int z = ((IntTag) posTag.get(2)).getValue();

			if (blockTag.contains(TAG_NBT)) {
				MutatedMinecraftMaterialStateBuilder mutationBuilder = new MutatedMinecraftMaterialStateBuilder(material);
				mutationBuilder.setTileEntity(blockTag.get(TAG_NBT));
				material = mutationBuilder.createMaterialState();
			}

			blocks.add(new Block(x, y, z, material));
		});

		return new CustomObject(blocks.toArray(new Block[0]));
	}

	public void writeNbtStructureToFile(File file, CustomObject object, AnvilVersion anvilVersion) throws IOException {
		CompoundTag schematicCompound = new CompoundTag("");

		schematicCompound.put(new IntTag(TAG_DATA_VERSION, anvilVersion.getDataVersion()));

		ListTag sizeTag = new ListTag(TAG_SIZE);
		sizeTag.add(new IntTag("", object.width));
		sizeTag.add(new IntTag("", object.height));
		sizeTag.add(new IntTag("", object.length));
		schematicCompound.put(sizeTag);

		ListTag blocksTag = new ListTag(TAG_BLOCKS);
		MaterialPalette palette = new MaterialPalette(anvilVersion);
		for (Block b : object.getBlocks()) {
			CompoundTag blockCompound = new CompoundTag("");

			MinecraftMaterialState materialState = (MinecraftMaterialState) b.material;
			int paletteId = palette.getPaletteIndex(materialState);
			blockCompound.put(new IntTag(TAG_STATE, paletteId));

			ListTag posTag = new ListTag(TAG_POS);
			posTag.add(new IntTag("", b.x));
			posTag.add(new IntTag("", b.y));
			posTag.add(new IntTag("", b.z));
			blockCompound.put(posTag);

			CompoundTag tileEntity = materialState.getTileEntity(anvilVersion.getVersionName());
			if (tileEntity != null) {
				blockCompound.put(tileEntity);
			}

			blocksTag.add(blockCompound);
		}

		schematicCompound.put(blocksTag);
		schematicCompound.put(palette.toNbt(TAG_PALETTE));

		schematicCompound.put(new ListTag(TAG_ENTITIES));

		NBTIO.writeFile(schematicCompound, file);
	}

	@Override
	public String formatName() {
		return "NBT Structure";
	}

	@Override
	public String formatSuffix() {
		return "nbt";
	}
}
