/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.module.anvil;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import net.worldsynth.addon.minecraft.MinecraftModuleCategories;
import net.worldsynth.addon.minecraft.biome.MinecraftBiome;
import net.worldsynth.addon.minecraft.minecraft.Dimension;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilDimension;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilWorld;
import net.worldsynth.biome.Biome;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBiomemap;
import net.worldsynth.datatype.DatatypeBiomespace;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.FileParameter;

public class ModuleMinecraftWorldImport extends AbstractModule {
	
	FileParameter minecraftLevelSave = new FileParameter("savepath", "Minecraft save file directory", null, new File(findMinecraftDir(), "saves"), false, true, null);
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		AbstractParameter<?>[] p = {
				minecraftLevelSave
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		AnvilWorld world = new AnvilWorld(minecraftLevelSave.getValue());
		AnvilDimension dimension = world.getDimension(Dimension.OVERWORLD.getDimensionId());
		
		if (request.output == getOutput("World blockspace")) {
			DatatypeBlockspace blockspaceRequest = (DatatypeBlockspace) request.data;
			
			double x = blockspaceRequest.extent.getX();
			double y = blockspaceRequest.extent.getY();
			double z = blockspaceRequest.extent.getZ();
			double res = blockspaceRequest.resolution;
			int spw = blockspaceRequest.spacePointsWidth;
			int sph = blockspaceRequest.spacePointsHeight;
			int spl = blockspaceRequest.spacePointsLength;
			
			MaterialState<?, ?>[][][] blockspace = new MaterialState<?, ?>[spw][sph][spl];
			
			for (int u = 0; u < spw; u++) {
				for (int v = 0; v < sph; v++) {
					for (int w = 0; w < spl; w++) {
						blockspace[u][v][w] = dimension.getMaterial((int) (x+u*res), (int) (y+v*res), (int) (z+w*res));
					}
				}
			}
			
			try {
				dimension.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			blockspaceRequest.setBlockspace(blockspace);
			return blockspaceRequest;
		}
		else if (request.output == getOutput("World biomemap")) {
			DatatypeBiomemap biomemapRequest = (DatatypeBiomemap) request.data;
			
			double x = biomemapRequest.extent.getX();
			double z = biomemapRequest.extent.getZ();
			double res = biomemapRequest.resolution;
			int mpw = biomemapRequest.mapPointsWidth;
			int mpl = biomemapRequest.mapPointsLength;
			
			//Extract necessary data from the read chunks
			Biome[][] biomemap = new Biome[mpw][mpl];
			
			//Read in needed chunks
			for (int u = 0; u < mpw; u++) {
				for (int v = 0; v < mpl; v++) {
					MinecraftBiome biome = dimension.getBiome((int) (x+u*res), (int) (z+v*res));
					biomemap[u][v] = biome == null ? Biome.NULL : biome;
				}
			}
			
			try {
				dimension.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			biomemapRequest.setBiomemap(biomemap);
			return biomemapRequest;
		}
		else if (request.output == getOutput("World biomespace")) {
			DatatypeBiomespace biomespaceRequest = (DatatypeBiomespace) request.data;
			
			double x = biomespaceRequest.extent.getX();
			double y = biomespaceRequest.extent.getY();
			double z = biomespaceRequest.extent.getZ();
			double res = biomespaceRequest.resolution;
			int spw = biomespaceRequest.spacePointsWidth;
			int sph = biomespaceRequest.spacePointsHeight;
			int spl = biomespaceRequest.spacePointsLength;
			
			Biome[][][] biomespace = new Biome[spw][sph][spl];
			
			for (int u = 0; u < spw; u++) {
				for (int v = 0; v < sph; v++) {
					for (int w = 0; w < spl; w++) {
						biomespace[u][v][w] = dimension.getBiome((int) (x+u*res), (int) (y+v*res), (int) (z+w*res));
					}
				}
			}
			
			try {
				dimension.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			biomespaceRequest.setBiomespace(biomespace);
			return biomespaceRequest;
		}
		
		return null;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Minecraft world import";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return MinecraftModuleCategories.MINECRAFT;
	}

	@Override
	public ModuleInput[] registerInputs() {
			return null;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "World blockspace"),
				new ModuleOutput(new DatatypeBiomemap(), "World biomemap"),
				new ModuleOutput(new DatatypeBiomespace(), "World biomespace")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}
	
	public static File findMinecraftDir() {
        File candidate;
        String appData = System.getenv("APPDATA");
        if (appData != null) {
            candidate = new File(appData, ".minecraft");
            if (candidate.isDirectory()) {
                return candidate;
            }
        }
        candidate = new File(System.getProperty("user.home"), "Library/Application Support/minecraft");
        if (candidate.isDirectory()) {
            return candidate;
        }
        candidate = new File(System.getProperty("user.home"), ".minecraft");
        if (candidate.isDirectory()) {
            return candidate;
        }
        return null;
    }
}
