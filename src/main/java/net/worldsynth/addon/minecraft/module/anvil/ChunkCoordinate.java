/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.module.anvil;

public class ChunkCoordinate {
	long x;
	long z;
	
	public ChunkCoordinate(long x, long z) {
		this.x = x;
		this.z = z;
	}
}
