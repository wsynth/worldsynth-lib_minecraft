/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.module.customobject;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import net.worldsynth.WorldSynth;
import net.worldsynth.addon.minecraft.MinecraftModuleCategories;
import net.worldsynth.addon.minecraft.customobject.NbtStructureCustomobjectFormat;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilVersion;
import net.worldsynth.build.Build;
import net.worldsynth.customobject.Block;
import net.worldsynth.customobject.CustomObject;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.extent.Extent;
import net.worldsynth.module.*;
import net.worldsynth.parameter.*;
import net.worldsynth.standalone.ui.event.ModuleApplyParametersEvent;
import net.worldsynth.standalone.ui.parameters.BooleanParameterCheckbox;
import net.worldsynth.standalone.ui.parameters.EnumParameterDropdownSelector;
import net.worldsynth.standalone.ui.parameters.ExtentParameterDropdownSelector;
import net.worldsynth.standalone.ui.parameters.FileParameterField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ModuleMinecraftNbtStructureExport extends AbstractModule {
	private static final Logger logger = LogManager.getLogger(ModuleMinecraftNbtStructureExport.class);

	private final WorldExtentParameter exportExtent = new WorldExtentParameter("extent", "Export extent", null, this);
	private final BooleanParameter includeAir = new BooleanParameter("includeair", "Include air", null, false);

	private final EnumParameter<AnvilVersion> anvilVersion = new EnumParameter<>("anvilversion", "Anvil version", null, AnvilVersion.class, AnvilVersion.VERSION_1_13_2, getSelectabbleAnvilVersion());
	private static AnvilVersion[] getSelectabbleAnvilVersion() {
		ArrayList<AnvilVersion> selectableVersion = new ArrayList<>();

		for (AnvilVersion version: AnvilVersion.values()) {
			if (version.getDataVersion() >= AnvilVersion.VERSION_1_13.getDataVersion()) {
				selectableVersion.add(version);
			}
		}

		return selectableVersion.toArray(new AnvilVersion[0]);
	}

	private final FileParameter exportFile = new FileParameter("file", "Save file", null,
			new File(System.getProperty("user.home") + "/export.nbt"), true, false, new FileChooser.ExtensionFilter("NBT Structure", "*.nbt"));

	@Override
	public AbstractParameter<?>[] registerParameters() {
		return new AbstractParameter<?>[]{
				exportExtent,
				anvilVersion,
				exportFile,
				includeAir
		};
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<>();

		inputRequests.put("input", new ModuleInputRequest(getInput("Blockspace"), outputRequest.data));

		return inputRequests;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		return inputs.get("input");
	}

	@Override
	public String getModuleName() {
		return "NBT Structure exporter";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return MinecraftModuleCategories.MINECRAFT;
	}

	@Override
	public ModuleInput[] registerInputs() {
		return new ModuleInput[]{
				new ModuleInput(new DatatypeBlockspace(), "Blockspace")
		};
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		return new ModuleOutput[]{
				new ModuleOutput(new DatatypeBlockspace(), "", false)
		};
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		ExtentParameterDropdownSelector parameterExtent = (ExtentParameterDropdownSelector) this.exportExtent.getUi();
		BooleanParameterCheckbox parameterIncludeAir = (BooleanParameterCheckbox) this.includeAir.getUi();

		EnumParameterDropdownSelector<AnvilVersion> parameterAnvilVersion = (EnumParameterDropdownSelector<AnvilVersion>) this.anvilVersion.getUi();
		FileParameterField parameterExportFile = (FileParameterField) this.exportFile.getUi();

		Button exportButton = new Button("Export NBT");
		exportButton.setOnAction(e -> {
			if (parameterExtent.getValue() == null) {
				return;
			}

			DatatypeBlockspace blockspace = getBlockspace(parameterExtent.getValue().asExtent());
			writeStructureToFile(parameterExportFile.getValue(), blockspace, parameterAnvilVersion.getValue(), parameterIncludeAir.getValue());
		});

		pane.setHgap(4.0);
		pane.setVgap(5.0);
//		pane.setGridLinesVisible(true);

		// Extent parameters
		pane.add(parameterExtent.getLabel(), 0, 0);
		pane.add(parameterExtent.getDropdown(), 1, 0);
		// Anvil version parameter
		pane.add(parameterAnvilVersion.getLabel(), 2, 0);
		pane.add(parameterAnvilVersion.getDropdown(), 3, 0);
		// Export file parameter
		pane.add(parameterExportFile.getLabel(), 0, 1);
		pane.add(parameterExportFile.getField(), 1, 1, 3, 1);
		pane.add(parameterExportFile.getDialogButton(), 4, 1);
		// Include air parameter
		pane.add(parameterIncludeAir.getLabel(), 0, 2);
		pane.add(parameterIncludeAir.getCheckBox(), 1, 2);
		// Export button
		pane.add(exportButton, 1, 3);

		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			parameterExportFile.applyUiValue();
			parameterAnvilVersion.applyUiValue();
			parameterExtent.applyUiValue();
			parameterIncludeAir.applyUiValue();
		};

		return applyHandler;
	}

	private DatatypeBlockspace getBlockspace(Extent extent) {
		ModuleInputRequest request = new ModuleInputRequest(getInput("Blockspace"), new DatatypeBlockspace(extent, 1.0));
		Build build = new Build(WorldSynth.getBuildCache(), e -> {
			logger.info(e.getStatus().toString());
		});

		return (DatatypeBlockspace) buildInputData(request, build);
	}

	private void writeStructureToFile(File file, DatatypeBlockspace blockspace, AnvilVersion anvilVersion, boolean includeAir) {
		int spw = blockspace.spacePointsWidth;
		int sph = blockspace.spacePointsHeight;
		int spl = blockspace.spacePointsLength;

		// Convert blockspace to custom object
		ArrayList<Block> blocks = new ArrayList<>();
		for (int u = 0; u < spw; u++) {
			for (int v = 0; v < sph; v++) {
				for (int w = 0; w < spl; w++) {
					if (!includeAir && blockspace.getLocalMaterial(u, v, w).isAir()) continue;

					blocks.add(new Block(u, v, w, blockspace.getLocalMaterial(u, v, w)));
				}
			}
		}

		CustomObject exportObject = new CustomObject(blocks.toArray(new Block[0]));

		// Write to file
		try {
			new NbtStructureCustomobjectFormat().writeNbtStructureToFile(file, exportObject, anvilVersion);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
