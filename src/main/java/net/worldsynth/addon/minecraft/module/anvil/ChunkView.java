/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.module.anvil;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import net.worldsynth.editor.ui.navcanvas.Coordinate;
import net.worldsynth.editor.ui.navcanvas.NavigationalCanvas;
import net.worldsynth.editor.ui.navcanvas.Pixel;

import java.util.ArrayList;

public class ChunkView extends Pane implements NavigationalCanvas {
	private final Canvas canvas;
	
	private double centerCoordX = 0.0;
	private double centerCoordY = 0.0;
	private double zoom = 1.0;
	
	private ArrayList<ChunkCoordinate> writtenChunks = new ArrayList<ChunkCoordinate>();
	private ArrayList<ExportBuildRegion> buildRegions = new ArrayList<ExportBuildRegion>();
	
	private double lastMouseX = 0;
	private double lastMouseY = 0;
	public ChunkView(double width, double height, ExportBuildRegion buildRegion, ExportTask exportTask) {
		canvas = new Canvas(500, 500);
		getChildren().add(canvas);
		
		buildRegions.add(buildRegion);
		
		setOnMouseMoved(e -> {
			lastMouseX = e.getX();
			lastMouseY = e.getY();
		});
		
		setOnMouseDragged(e -> {
			double diffX = lastMouseX - e.getX();
			double diffY = lastMouseY - e.getY();
			
			centerCoordX += diffX / zoom;
			centerCoordY += diffY / zoom;
			
			lastMouseX = e.getX();
			lastMouseY = e.getY();
			
			paint();
		});
		
		addEventHandler(ScrollEvent.SCROLL, e -> {
			double maxZoom = 1.0;
			double minZoom = 1.0/8.0;
			
			double lastZoom = zoom;
			zoom += e.getDeltaY() / e.getMultiplierY() * zoom / 10;
			if (zoom < minZoom) zoom = minZoom;
			else if (zoom > maxZoom) zoom = maxZoom;
			
			if (lastZoom != zoom) {
				paint();
			}
		});
		
		exportTask.messageProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				writtenChunks = exportTask.getWrittenChunksList();
				paint();
			}
		});
	}
	
	@Override
    protected void layoutChildren() {
        super.layoutChildren();
        final double x = snappedLeftInset();
        final double y = snappedTopInset();
        // Java 9 - snapSize is depricated used snapSizeX() and snapSizeY() accordingly
        final double w = snapSize(getWidth()) - x - snappedRightInset();
        final double h = snapSize(getHeight()) - y - snappedBottomInset();
        canvas.setLayoutX(x);
        canvas.setLayoutY(y);
        canvas.setWidth(w);
        canvas.setHeight(h);
        
        paint();
    }
	
	@Override
	public double getCenterCoordinateX() {
		return centerCoordX;
	}

	@Override
	public double getCenterCoordinateY() {
		return centerCoordY;
	}

	@Override
	public double getZoom() {
		return zoom;
	}
	
	public void registerBuiltChunk(int x, int z) {
		writtenChunks.add(new ChunkCoordinate(x, z));
		paint();
	}
	
	private void paint() {
		GraphicsContext g = canvas.getGraphicsContext2D();
		g.setFill(Color.web("#303030"));
		g.fillRect(0, 0, getWidth(), getHeight());
		
		//Draw build regions
		g.setFill(Color.RED);
		for (ExportBuildRegion cbr: buildRegions) {
			Pixel p = new Pixel(new Coordinate(cbr.getStartChunkX()*16, cbr.getStartChunkZ()*16), this);
			g.fillRect(p.x, p.y, (int) (Math.ceil((cbr.getChunksWidth())*16.0*zoom)), (int) (Math.ceil((cbr.getChunksLenght())*16.0*zoom)));
		}
		
		//Draw built chunks
		g.setFill(Color.GREEN);
		for (ChunkCoordinate c: writtenChunks) {
			Pixel p = new Pixel(new Coordinate(c.x*16, c.z*16), this);
			g.fillRect(p.x, p.y, (int) (Math.ceil(16.0*zoom)), (int) (Math.ceil(16.0*zoom)));
		}
		
		paintGrid(g);
	}
	
	private void paintGrid(GraphicsContext g) {
		//Draw the grid
		g.setStroke(Color.web("#363636", Math.max(0.0, Math.min(1.0, zoom*2-0.5))));
		g.setLineWidth(1.0);
		float gridIncrement = 16;
		for (float cx = gridIncrement; (new Pixel(new Coordinate(cx, 0), this)).x < getWidth(); cx += gridIncrement) {
			double x = (new Pixel(new Coordinate(cx, 0), this)).x;
			g.strokeLine(x, 0, x, getHeight());
		}
		for (float cx = -gridIncrement; (new Pixel(new Coordinate(cx, 0), this)).x > 0; cx -= gridIncrement) {
			double x = (new Pixel(new Coordinate(cx, 0), this)).x;
			g.strokeLine(x, 0, x, getHeight());
		}
		for (float cy = gridIncrement; (new Pixel(new Coordinate(0, cy), this)).y < getHeight(); cy += gridIncrement) {
			double y = (new Pixel(new Coordinate(0, cy), this)).y;
			g.strokeLine(0, y, getWidth(), y);
		}
		for (float cy = -gridIncrement; (new Pixel(new Coordinate(0, cy), this)).y > 0; cy -= gridIncrement) {
			double y = (new Pixel(new Coordinate(0, cy), this)).y;
			g.strokeLine(0, y, getWidth(), y);
		}
		
		//Draw center cross;
		g.setStroke(Color.web("#808080", 0.5));
		g.setLineWidth(2.0);
		Pixel centerCoordinatePixel = new Pixel(new Coordinate(0, 0), this);
		g.strokeLine(centerCoordinatePixel.x, 0, centerCoordinatePixel.x, getHeight());
		g.strokeLine(0, centerCoordinatePixel.y, getWidth(), centerCoordinatePixel.y);
		
	}
}
