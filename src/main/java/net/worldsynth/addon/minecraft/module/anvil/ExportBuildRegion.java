/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.module.anvil;

public class ExportBuildRegion {
	private final int startChunkX;
	private final int startChunkZ;
	private final int endChunkX;
	private final int endChunkZ;
	private final int minSectionY;
	private final int maxSectionY;
	
	private final int chunkGroupingX;
	private final int chunkGroupingZ;
	
	/**
	 * Constructs a chunk region containing the chunks containing the blocks in the specified area
	 * @param x block coordinate
	 * @param z block coordinate
	 * @param width in blocks
	 * @param lenght in blocks
	 * @param chunkGroupingX
	 * @param chunkGroupingZ
	 */
	public ExportBuildRegion(long x, long z, long width, long lenght, int chunkGroupingX, int chunkGroupingZ, int minSectionY, int maxSextionY) {
		startChunkX = (int) Math.floor((double)x/16.0);
		startChunkZ = (int) Math.floor((double)z/16.0);
		endChunkX = (int) Math.ceil((double)(x+width)/16.0)-1;
		endChunkZ = (int) Math.ceil((double)(z+lenght)/16.0)-1;
		this.chunkGroupingX = chunkGroupingX;
		this.chunkGroupingZ = chunkGroupingZ;
		
		this.minSectionY = minSectionY;
		this.maxSectionY = maxSextionY;
	}
	
	public int getStartChunkX() {
		return startChunkX;
	}
	
	public int getStartChunkZ() {
		return startChunkZ;
	}
	
	public int getEndChunkX() {
		return endChunkX;
	}
	
	public int getEndChunkZ() {
		return endChunkZ;
	}
	
	public int getMinSectionY() {
		return minSectionY;
	}
	
	public int getMaxSectionY() {
		return maxSectionY;
	}
	
	public int getChunksWidth() {
		return endChunkX - startChunkX + 1;
	}
	
	public int getChunksLenght() {
		return endChunkZ - startChunkZ + 1;
	}
	
	public int getSectionsHeight() {
		return maxSectionY - minSectionY + 1;
	}
	
	public int getChunkCount() {
		return getChunksWidth() * getChunksLenght();
	}
	
	public int getChunkGroupingX() {
		return chunkGroupingX;
	}
	
	public int getChunkGroupingZ() {
		return chunkGroupingZ;
	}
}
