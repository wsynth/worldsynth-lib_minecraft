/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.module.anvil;

import javafx.concurrent.Task;
import javafx.scene.layout.Pane;
import net.worldsynth.WorldSynth;
import net.worldsynth.addon.minecraft.biome.MinecraftBiome;
import net.worldsynth.addon.minecraft.material.MinecraftMaterial;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialState;
import net.worldsynth.addon.minecraft.minecraft.Difficulty;
import net.worldsynth.addon.minecraft.minecraft.Dimension;
import net.worldsynth.addon.minecraft.minecraft.GameMode;
import net.worldsynth.addon.minecraft.minecraft.Generator;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilDimension;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilVersion;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilWorld;
import net.worldsynth.addon.minecraft.minecraft.anvil.IChunkStatus;
import net.worldsynth.addon.minecraft.minecraft.anvil.chunk.AnvilChunkCreationUtil;
import net.worldsynth.addon.minecraft.minecraft.anvil.chunk.SectionedAnvilChunk;
import net.worldsynth.biome.Biome;
import net.worldsynth.build.Build;
import net.worldsynth.build.BuildCache;
import net.worldsynth.datatype.*;
import net.worldsynth.extent.Extent;
import net.worldsynth.material.MaterialState;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.ArrayList;

public class ExportTask extends Task<Boolean> {
	private static final Logger logger = LogManager.getLogger(ExportTask.class);
	
	private final ModuleMinecraftWorldExport exportModule;
	private final ModuleInput blockspaceInput;
	private final ModuleInput biomeInput;
	
	private final ExportBuildRegion exportBuildRegion;
	
	private final File minecraftSaveDirectory;
	private final String levelName;
	private final AnvilVersion anvilVersion;
	private final Generator generator;
	private final Dimension dimension;
	private final IChunkStatus chunkStatus;
	private final GameMode gameMode;
	private final Difficulty difficulty;
	private final long seed;
	
	private volatile ArrayList<ChunkCoordinate> writtenChunks = new ArrayList<ChunkCoordinate>();
	
	public ExportTask(
			ModuleMinecraftWorldExport exportModule,
			ModuleInput blockspaceInput,
			ModuleInput biomeInput,
			ExportBuildRegion exportBuildRegion,
			File minecraftSaveDirectory,
			String levelName,
			AnvilVersion anvilVersion,
			Generator generator,
			long seed,
			GameMode gameMode,
			Difficulty difficulty,
			Dimension dimension,
			IChunkStatus chunkStatus,
			Pane relativePositionComponent
			) {
		this.exportModule = exportModule;
		this.blockspaceInput = blockspaceInput;
		this.biomeInput = biomeInput;
		
		this.exportBuildRegion = exportBuildRegion;
		
		this.minecraftSaveDirectory = minecraftSaveDirectory;
		this.levelName = levelName;
		this.anvilVersion = anvilVersion;
		
		this.generator = generator;
		this.gameMode = gameMode;
		this.difficulty = difficulty;
		this.seed = seed;
		
		this.dimension = dimension;
		this.chunkStatus = chunkStatus;
		
		setOnFailed(e -> {
			logger.error("Minecraft world export failed", getException());
		});
	}
	
	@Override
	protected Boolean call() throws Exception {
		logger.debug("Initalizing anvil exporter");
		AnvilWorld world = new AnvilWorld(new File(minecraftSaveDirectory, levelName));
		
//		// Temporary level file if export is aborted
		world.createLevelFile(anvilVersion, levelName, generator, seed, gameMode, difficulty, 0, 256, 0);
		
		AnvilDimension dimension = world.getDimension(this.dimension.getDimensionId());
		
		int currentBuiltChunkCount = 0;
		
		int xGroupsCount = (int) Math.ceil((double) exportBuildRegion.getChunksWidth() / (double) exportBuildRegion.getChunkGroupingX());
		int zGroupsCount = (int) Math.ceil((double) exportBuildRegion.getChunksLenght() / (double) exportBuildRegion.getChunkGroupingZ());
		
		double buildStartY = exportBuildRegion.getMinSectionY() * 16;
		double buildHeight = exportBuildRegion.getSectionsHeight() * 16;
		int integerSpaceHeight = exportBuildRegion.getSectionsHeight() * 16;
		
		for (int cgx = 0; cgx < xGroupsCount; cgx++) {
			for (int cgz = 0; cgz < zGroupsCount; cgz++) {
				if (isCancelled()) {
					return null;
				}
				logger.debug("Starting export of chunk group (" + cgx + ", " + cgz + ")");
				
				int groupStartChunkX = cgx*exportBuildRegion.getChunkGroupingX() + exportBuildRegion.getStartChunkX();
				int groupStartChunkZ = cgz*exportBuildRegion.getChunkGroupingZ() + exportBuildRegion.getStartChunkZ();
				int groupEndChunkX = cgx*exportBuildRegion.getChunkGroupingX() + exportBuildRegion.getStartChunkX() + exportBuildRegion.getChunkGroupingX();
				int groupEndChunkZ = cgz*exportBuildRegion.getChunkGroupingZ() + exportBuildRegion.getStartChunkZ() + exportBuildRegion.getChunkGroupingZ();
				
				if (groupEndChunkX > exportBuildRegion.getEndChunkX()+1) {
					groupEndChunkX = exportBuildRegion.getEndChunkX()+1;
				}
				if (groupEndChunkZ > exportBuildRegion.getEndChunkZ()+1) {
					groupEndChunkZ = exportBuildRegion.getEndChunkZ()+1;
				}
				
				double buildStartX = groupStartChunkX * 16;
				double buildStartZ = groupStartChunkZ * 16;
				double buildWidth = (groupEndChunkX - groupStartChunkX) * 16;
				double buildLenght = (groupEndChunkZ - groupStartChunkZ) * 16;
				
				logger.debug("Build chunk group (" + cgx + ", " + cgz + ")");
				Extent groupBuildExtent = new Extent(buildStartX, buildStartY, buildStartZ, buildWidth, buildHeight, buildLenght);
				
				// Build blockspace data
				ModuleInputRequest request = new ModuleInputRequest(blockspaceInput, new DatatypeBlockspace(groupBuildExtent, 1.0));

				BuildCache cache = WorldSynth.getBuildCache();
				Build build = new Build(cache, null);
				DatatypeBlockspace blockspaceData = (DatatypeBlockspace) exportModule.buildInputData(request, build);
				if (blockspaceData == null) {
					throw new NullPointerException("Blockspace data was null");
				}
				
				// Build biomes data
				if (anvilVersion.compareTo(AnvilVersion.VERSION_1_15) >= 0) {
					request = new ModuleInputRequest(biomeInput, new DatatypeMultitype(
							new DatatypeBiomespace(groupBuildExtent, 1.0),
							new DatatypeBiomemap(groupBuildExtent, 1.0))
							);
				}
				else {
					request = new ModuleInputRequest(biomeInput, new DatatypeBiomemap(groupBuildExtent, 1.0));
				}
				AbstractDatatype biomedata = exportModule.buildInputData(request, build);
				if (biomedata == null) {
					throw new NullPointerException("Biome data was null");
				}
				
				logger.debug("Done building chunk group (" + cgx + ", " + cgz + ")");
				
				//Write the built chunk group to file
				for (int u = 0; u < exportBuildRegion.getChunkGroupingX(); u++) {
					for (int v = 0; v < exportBuildRegion.getChunkGroupingZ(); v++) {
						int writeChunkX = groupStartChunkX+u;
						int writeChunkZ = groupStartChunkZ+v;
						if (writeChunkX > exportBuildRegion.getEndChunkX() || writeChunkZ > exportBuildRegion.getEndChunkZ()) {
							continue;
						}
						logger.debug("Starting export of chunk (" + writeChunkX + ", " + writeChunkZ + ")");
						
						MinecraftMaterialState[][][] blockspace;
						blockspace = extractSubBlockspace(blockspaceData.getBlockspace(), u*16, 0, v*16, 16, integerSpaceHeight, 16);
						
						
						logger.debug("Convert chunk (" + writeChunkX + ", " + writeChunkZ + ")");
						SectionedAnvilChunk<?> chunk;
						int minSectionY = exportBuildRegion.getMinSectionY();
						if (biomedata instanceof DatatypeBiomespace) {
							MinecraftBiome[][][] biomespace = extractSubBiomespace(((DatatypeBiomespace) biomedata).getBiomespace(), u*16, 0, v*16, 16, integerSpaceHeight, 16);
							chunk = AnvilChunkCreationUtil.createAnvilChunk(writeChunkX, writeChunkZ, minSectionY, anvilVersion, chunkStatus, blockspace, biomespace);
						}
						else {
							MinecraftBiome[][] biomemap = extractSubBiomemap(((DatatypeBiomemap) biomedata).getBiomemap(), u*16, v*16, 16, 16);
							chunk = AnvilChunkCreationUtil.createAnvilChunk(writeChunkX, writeChunkZ, minSectionY, anvilVersion, chunkStatus, blockspace, biomemap);
						}
						
						logger.debug("Write chunk (" + writeChunkX + ", " + writeChunkZ + ")");
						dimension.addChunk(chunk, true);
						dimension.save();
						
						currentBuiltChunkCount++;
						writtenChunks.add(new ChunkCoordinate(writeChunkX, writeChunkZ));
						//FIXME Fix possible/probable race problem
						logger.info("Wrote chunk: " + writeChunkX + "," + writeChunkZ);
						updateMessage("Wrote chunk: " + writeChunkX + "," + writeChunkZ);
					}
				}
			}
		}
		
		// Find surface at (0, 0)
		int y;
		for(y = 255; y > 0; y--) {
			if (!dimension.getMaterial(0, y, 0).isAir()) {
				break;
			}
		}
		
		dimension.close();
		
		world.createLevelFile(anvilVersion, levelName, generator, seed, gameMode, difficulty, 0, y, 0);
		
		return true;
	}
	
	private MinecraftMaterialState[][][] extractSubBlockspace(MaterialState<?, ?>[][][] blockspace, int x, int y, int z, int width, int height, int lenght) {
		MinecraftMaterialState[][][] extractedBlockstates = new MinecraftMaterialState[width][height][lenght];
		
		for (int u = 0; u < width; u++) {
			for (int v = 0; v < height; v++) {
				for (int w = 0; w < lenght; w++) {
					try {
						extractedBlockstates[u][v][w] = (MinecraftMaterialState) blockspace[x+u][y+v][z+w];
					} catch (ClassCastException e) {
						//TODO Indicate to the log when trying to export a material that is not a minecraft material
						extractedBlockstates[u][v][w] = MinecraftMaterial.AIR.getDefaultState();
					}
				}
			}
		}
		
		return extractedBlockstates;
	}
	
	private MinecraftBiome[][] extractSubBiomemap(Biome[][] biomemap, int x, int z, int width, int lenght) {
		MinecraftBiome[][] extractedBiomes = new MinecraftBiome[width][lenght];
		
		for (int u = 0; u < width; u++) {
			for (int w = 0; w < lenght; w++) {
				try {
					extractedBiomes[u][w] = (MinecraftBiome) biomemap[x+u][z+w];
				} catch (ClassCastException e) {
					//TODO Indicate to the log when trying to export a biome that is not a minecraft biome
					extractedBiomes[u][w] = MinecraftBiome.UNKNOWN;
				}
			}
		}
		
		return extractedBiomes;
	}
	
	private MinecraftBiome[][][] extractSubBiomespace(Biome[][][] biomespace, int x, int y, int z, int width, int height, int lenght) {
		MinecraftBiome[][][] extractedBiomes = new MinecraftBiome[width][height][lenght];
		
		for (int u = 0; u < width; u++) {
			for (int v = 0; v < height; v++) {
				for (int w = 0; w < lenght; w++) {
					try {
						extractedBiomes[u][v][w] = (MinecraftBiome) biomespace[x+u][y+v][z+w];
					} catch (ClassCastException e) {
						//TODO Indicate to the log when trying to export a biome that is not a minecraft biome
						extractedBiomes[u][v][w] = MinecraftBiome.UNKNOWN;
					}
				}
			}
		}
		
		return extractedBiomes;
	}
	
	private MinecraftBiome[][][] extrudeBiomespace(MinecraftBiome[][] biomemap, int height) {
		int width = biomemap.length;
		int length = biomemap[0].length;
		
		MinecraftBiome[][][] biomespace = new MinecraftBiome[width][height][length];
		for (int u = 0; u < width; u++) {
			for (int w = 0; w < length; w++) {
				MinecraftBiome b = biomemap[u][w];
				for (int v = 0; v < height; v++) {
					biomespace[u][v][w] = b;
				}
			}
		}
		
		return biomespace;
	}
	
	public ArrayList<ChunkCoordinate> getWrittenChunksList() {
		return (ArrayList<ChunkCoordinate>) writtenChunks.clone();
	}
}
