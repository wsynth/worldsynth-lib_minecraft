/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.module.anvil;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

public class ExportProgressPane extends GridPane {
	
	ProgressBar exportProgressBar;
	Label exportProgressLabel;
	ChunkView chunkView;
	
	public ExportProgressPane(ExportBuildRegion buildRegion, ExportTask exportTask, ToggleButton pinButton) {
		chunkView = new ChunkView(500, 500, buildRegion, exportTask);
		GridPane.setHgrow(chunkView, Priority.ALWAYS);
		GridPane.setVgrow(chunkView, Priority.ALWAYS);
		add(chunkView, 0, 1);
		
		exportProgressBar = new ProgressBar(0.0D);
		exportProgressBar.setPrefWidth(300);
		exportProgressLabel = new Label("  0%  |  0 / " + buildRegion.getChunkCount() + " chunks written");
		
		HBox progressBox = new HBox(exportProgressBar, exportProgressLabel);
		progressBox.setAlignment(Pos.CENTER);
		add(progressBox, 0, 0);
		
		GridPane.setHalignment(pinButton, HPos.LEFT);
		add(pinButton, 0, 2);
		
		Button abortButton = new Button("Abort world export");
		abortButton.setPrefWidth(200);
		abortButton.setOnAction(e -> {
			exportTask.cancel();
		});
		GridPane.setHalignment(abortButton, HPos.CENTER);
		add(abortButton, 0, 2);
		
		exportTask.messageProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				int writtenChnkCount = exportTask.getWrittenChunksList().size();
				double exportProgress = (double) writtenChnkCount / (double) buildRegion.getChunkCount();
				DecimalFormat df = new DecimalFormat("#");
				df.setRoundingMode(RoundingMode.HALF_UP);
				String exsportProgressString = df.format(exportProgress * 100.0D);
				
				exportProgressBar.setProgress(exportProgress);
				exportProgressLabel.setText("  " + exsportProgressString + "%  |  " + writtenChnkCount + " / " + buildRegion.getChunkCount() + " chunks written");
			}
		});
	}
}