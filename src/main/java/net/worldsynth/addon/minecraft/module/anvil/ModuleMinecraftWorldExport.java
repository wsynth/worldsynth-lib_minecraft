/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.module.anvil;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

import net.worldsynth.addon.minecraft.minecraft.anvil.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.WindowEvent;
import net.worldsynth.addon.minecraft.MinecraftModuleCategories;
import net.worldsynth.addon.minecraft.minecraft.Difficulty;
import net.worldsynth.addon.minecraft.minecraft.Dimension;
import net.worldsynth.addon.minecraft.minecraft.GameMode;
import net.worldsynth.addon.minecraft.minecraft.Generator;
import net.worldsynth.addon.minecraft.minecraft.WorldType;
import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeBiomemap;
import net.worldsynth.datatype.DatatypeBiomespace;
import net.worldsynth.datatype.DatatypeBlockspace;
import net.worldsynth.datatype.DatatypeMultitype;
import net.worldsynth.extent.WorldExtent;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.BooleanParameter;
import net.worldsynth.parameter.EnumParameter;
import net.worldsynth.parameter.FileParameter;
import net.worldsynth.parameter.IntegerTupleParameter;
import net.worldsynth.parameter.LongParameter;
import net.worldsynth.parameter.StringParameter;
import net.worldsynth.parameter.WorldExtentParameter;
import net.worldsynth.standalone.ui.event.ModuleApplyParametersEvent;
import net.worldsynth.standalone.ui.parameters.BooleanParameterCheckbox;
import net.worldsynth.standalone.ui.parameters.EnumParameterDropdownSelector;
import net.worldsynth.standalone.ui.parameters.ExtentParameterDropdownSelector;
import net.worldsynth.standalone.ui.parameters.FileParameterField;
import net.worldsynth.standalone.ui.parameters.LongParameterField;
import net.worldsynth.standalone.ui.parameters.NumberTupleParameterField;
import net.worldsynth.standalone.ui.parameters.StringParameterField;
import net.worldsynth.standalone.ui.stage.UiWindowUtil;

public class ModuleMinecraftWorldExport extends AbstractModule {
	private static final Logger logger = LogManager.getLogger(ModuleMinecraftWorldExport.class);
	
	private WorldExtentParameter exportExtent = new WorldExtentParameter("extent", "Export extent", null, this);
	private BooleanParameter exportExtentHeight = new BooleanParameter("useextentheight", "Use extent height", null, false);
	private IntegerTupleParameter chunkGrouping = new IntegerTupleParameter("grouping", "Chunk grouping", null, 1, Integer.MAX_VALUE, 16, 16);
	
	private FileParameter saveDirectory = new FileParameter("savedirectory", "Minecraft save directory", null, new File(findMinecraftDir(), "saves"), true, true, null);
	private StringParameter saveName = new StringParameter("savename", "Level name", null, "New WorldSynth export");
	
	//TODO Add selectable versions
//	AnvilVersion[] selectableAnvilVersions = {AnvilVersion.VERSION_1_12_2, AnvilVersion.VERSION_1_13_2};
	private EnumParameter<AnvilVersion> anvilVersion = new EnumParameter<AnvilVersion>("anvilversion", "Anvil version", null, AnvilVersion.class, AnvilVersion.VERSION_1_13_2);
	
	private EnumParameter<WorldType> worldType = new EnumParameter<WorldType>("worldtype", "World type", null, WorldType.class, WorldType.DEFAULT);
	private StringParameter customWorldTypeName = new StringParameter("customworldtypename", "Custom world type name", null, "Custom name here");
	private LongParameter seed = new LongParameter("seed", "Seed", null, 0, Long.MIN_VALUE, Long.MAX_VALUE);
	private EnumParameter<GameMode> gameMode = new EnumParameter<GameMode>("gamemode", "Game mode", null, GameMode.class, GameMode.CREATIVE);
	private EnumParameter<Difficulty> difficulty = new EnumParameter<Difficulty>("difficulty", "Difficulty", null, Difficulty.class, Difficulty.NORMAL);
	
	private EnumParameter<Dimension> dimension = new EnumParameter<Dimension>("dimension", "Dimension", null, Dimension.class, Dimension.OVERWORLD);
	private EnumParameter<ChunkDecoration112> chunkDecoration = new EnumParameter<ChunkDecoration112>("decoration112", "Chunk status", null, ChunkDecoration112.class, ChunkDecoration112.DECORATED);
	private ChunkStatus113[] selectableStatuses113 = {ChunkStatus113.BASE, ChunkStatus113.CARVED, ChunkStatus113.LIQUID_CARVED, ChunkStatus113.DECORATED};
	private EnumParameter<ChunkStatus113> chunkStatus113 = new EnumParameter<ChunkStatus113>("chunkstatus113", "Chunk status", null, ChunkStatus113.class, ChunkStatus113.DECORATED, selectableStatuses113);
//	private ChunkStatus114[] selectableStatuses114 = {ChunkStatus114.FULL};
	private EnumParameter<ChunkStatus114> chunkStatus114 = new EnumParameter<ChunkStatus114>("chunkstatus114", "Chunk status", null, ChunkStatus114.class, ChunkStatus114.FULL);
	private EnumParameter<ChunkStatus120> chunkStatus120 = new EnumParameter<ChunkStatus120>("chunkstatus120", "Chunk status", null, ChunkStatus120.class, ChunkStatus120.FULL);
	
	
	private static File findMinecraftDir() {
        File candidate;
        String appData = System.getenv("APPDATA");
        if (appData != null) {
            candidate = new File(appData, ".minecraft");
            if (candidate.isDirectory()) {
                return candidate;
            }
        }
        candidate = new File(System.getProperty("user.home"), "Library/Application Support/minecraft");
        if (candidate.isDirectory()) {
            return candidate;
        }
        candidate = new File(System.getProperty("user.home"), ".minecraft");
        if (candidate.isDirectory()) {
            return candidate;
        }
        return null;
    }
	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		
		seed.setValue(new Random().nextLong());
		
		AbstractParameter<?>[] p = {
				exportExtent,
				exportExtentHeight,
				chunkGrouping,
				saveDirectory,
				saveName,
				anvilVersion,
				worldType,
				customWorldTypeName,
				seed,
				gameMode,
				difficulty,
				dimension,
				chunkDecoration,
				chunkStatus113,
				chunkStatus114,
				chunkStatus120
				};
		return p;
	}
	
	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		return inputs.get("input");
	}

	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("input", new ModuleInputRequest(getInput("Blockspace input"), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public String getModuleName() {
		return "Minecraft world export";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return MinecraftModuleCategories.MINECRAFT;
	}

	@Override
	public ModuleInput[] registerInputs() {
			ModuleInput[] i = {
					new ModuleInput(new DatatypeBlockspace(), "Blockspace input"),
					new ModuleInput(new DatatypeMultitype(new DatatypeBiomemap(), new DatatypeBiomespace()), "Biome input")
					};
			return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeBlockspace(), "Blockspace trough", false)
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return false;
	}

	@Override
	public EventHandler<ModuleApplyParametersEvent> moduleUI(GridPane pane) {
		ExtentParameterDropdownSelector parameterExtent = (ExtentParameterDropdownSelector) this.exportExtent.getUi();
		BooleanParameterCheckbox parameterExtentHeight = (BooleanParameterCheckbox) this.exportExtentHeight.getUi();
		NumberTupleParameterField<Integer> parameterGrouping = (NumberTupleParameterField<Integer>) this.chunkGrouping.getUi();
		EnumParameterDropdownSelector<Dimension> parameterDimension = (EnumParameterDropdownSelector<Dimension>) this.dimension.getUi();
		EnumParameterDropdownSelector<ChunkDecoration112> parameterChunkDecoration112 = (EnumParameterDropdownSelector<ChunkDecoration112>) this.chunkDecoration.getUi();
		EnumParameterDropdownSelector<ChunkStatus113> parameterChunkStatus113 = (EnumParameterDropdownSelector<ChunkStatus113>) this.chunkStatus113.getUi();
		EnumParameterDropdownSelector<ChunkStatus114> parameterChunkStatus114 = (EnumParameterDropdownSelector<ChunkStatus114>) this.chunkStatus114.getUi();
		EnumParameterDropdownSelector<ChunkStatus120> parameterChunkStatus120 = (EnumParameterDropdownSelector<ChunkStatus120>) this.chunkStatus120.getUi();
		EnumParameterDropdownSelector<WorldType> parameterWorldType = (EnumParameterDropdownSelector<WorldType>) this.worldType.getUi();
		
		EnumParameterDropdownSelector<AnvilVersion> parameterAnvilVersion = (EnumParameterDropdownSelector<AnvilVersion>) this.anvilVersion.getUi();
		EnumParameterDropdownSelector<GameMode> parameterGameMode = (EnumParameterDropdownSelector<GameMode>) this.gameMode.getUi();
		EnumParameterDropdownSelector<Difficulty> parameterDifficulty = (EnumParameterDropdownSelector<Difficulty>) this.difficulty.getUi();
		LongParameterField parameterSeed = (LongParameterField) this.seed.getUi();
		
		StringParameterField parameterCustomWorldTypeName = (StringParameterField) this.customWorldTypeName.getUi();
		
		FileParameterField parameterSaveDirectory = (FileParameterField) this.saveDirectory.getUi();
		StringParameterField parameterLevelName = (StringParameterField) this.saveName.getUi();
		
		ClassLoader addonClassLoader = getClass().getClassLoader();
		Image warningIcon = new Image(addonClassLoader.getResourceAsStream("warning.png"));
		Label exportWarningLabel = new Label("World type \"" + parameterWorldType.getValue().name() + "\" uses the Superflat generator. Minecraft will not decorate generated chunks.", new ImageView(warningIcon));
		updateExportWarning(exportWarningLabel, parameterAnvilVersion.getValue(), parameterWorldType.getValue(), parameterChunkDecoration112.getValue(), parameterChunkStatus113.getValue(), parameterChunkStatus114.getValue(), parameterChunkStatus120.getValue());
		
//		parameterExtentHeight.setDisable(parameterAnvilVersion.getValue().compareTo(AnvilVersion.VERSION_1_18) < 0);
		parameterAnvilVersion.addChengeEventHandler(e -> {
//			parameterExtentHeight.setDisable(parameterAnvilVersion.getValue().compareTo(AnvilVersion.VERSION_1_18) < 0);
			
			updateExportWarning(exportWarningLabel, parameterAnvilVersion.getValue(), parameterWorldType.getValue(), parameterChunkDecoration112.getValue(), parameterChunkStatus113.getValue(), parameterChunkStatus114.getValue(), parameterChunkStatus120.getValue());
		});
		parameterExtentHeight.setDisable(true);
		
		parameterCustomWorldTypeName.setDisable(parameterWorldType.getValue() != WorldType.CUSTOM);
		parameterWorldType.addChengeEventHandler(e -> {
			parameterCustomWorldTypeName.setDisable(parameterWorldType.getValue() != WorldType.CUSTOM);
			
			updateExportWarning(exportWarningLabel, parameterAnvilVersion.getValue(), parameterWorldType.getValue(), parameterChunkDecoration112.getValue(), parameterChunkStatus113.getValue(), parameterChunkStatus114.getValue(), parameterChunkStatus120.getValue());
		});
		
		parameterChunkDecoration112.addChengeEventHandler(e -> {
			updateExportWarning(exportWarningLabel, parameterAnvilVersion.getValue(), parameterWorldType.getValue(), parameterChunkDecoration112.getValue(), parameterChunkStatus113.getValue(), parameterChunkStatus114.getValue(), parameterChunkStatus120.getValue());
		});
		
		parameterChunkStatus113.addChengeEventHandler(e -> {
			updateExportWarning(exportWarningLabel, parameterAnvilVersion.getValue(), parameterWorldType.getValue(), parameterChunkDecoration112.getValue(), parameterChunkStatus113.getValue(), parameterChunkStatus114.getValue(), parameterChunkStatus120.getValue());
		});
		
		parameterChunkStatus114.addChengeEventHandler(e -> {
			updateExportWarning(exportWarningLabel, parameterAnvilVersion.getValue(), parameterWorldType.getValue(), parameterChunkDecoration112.getValue(), parameterChunkStatus113.getValue(), parameterChunkStatus114.getValue(), parameterChunkStatus120.getValue());
		});

		parameterChunkStatus120.addChengeEventHandler(e -> {
			updateExportWarning(exportWarningLabel, parameterAnvilVersion.getValue(), parameterWorldType.getValue(), parameterChunkDecoration112.getValue(), parameterChunkStatus113.getValue(), parameterChunkStatus114.getValue(), parameterChunkStatus120.getValue());
		});
		
		
		Button exportButton = new Button("Export world");
		exportButton.setOnAction(e -> {

			// Select chunk status based on anvil version
			AnvilVersion anvilVersion = parameterAnvilVersion.getValue();
			IChunkStatus chunkStatus = parameterChunkDecoration112.getValue();
			chunkStatus = anvilVersion.compareTo(AnvilVersion.VERSION_1_13) >= 0 ? parameterChunkStatus113.getValue() : chunkStatus;
			chunkStatus = anvilVersion.compareTo(AnvilVersion.VERSION_1_14) >= 0 ? parameterChunkStatus114.getValue() : chunkStatus;
			chunkStatus = anvilVersion.compareTo(AnvilVersion.VERSION_1_20) >= 0 ? parameterChunkStatus120.getValue() : chunkStatus;
			
			exportWorldFile(
					parameterExtent.getValue(),
					parameterExtentHeight.getValue(),
					parameterGrouping.getValue().getValue(0),
					parameterGrouping.getValue().getValue(1),
					parameterSaveDirectory.getValue(),
					parameterLevelName.getValue(),
					parameterAnvilVersion.getValue(),
					parameterDimension.getValue(),
					chunkStatus,
					parameterWorldType.getValue(),
					parameterCustomWorldTypeName.getValue(),
					parameterSeed.getValue(),
					parameterGameMode.getValue(),
					parameterDifficulty.getValue(),
					pane);
		});
		
		pane.setHgap(4.0);
		pane.setVgap(5.0);
//		pane.setGridLinesVisible(true);
		
		// Parameters row 1
		// Parameters column 1
		// Extent parameters
		pane.add(parameterExtent.getLabel(), 0, 0);
		pane.add(parameterExtent.getDropdown(), 1, 0);
		pane.add(parameterExtentHeight.getLabel(), 2, 0);
		pane.add(parameterExtentHeight.getCheckBox(), 3, 0);
		// Chunk grouping parameter
		pane.add(parameterGrouping.getLabel(), 0, 1);
		pane.add(parameterGrouping.getFields(), 1, 1, 3, 1);
		// Dimension parameter
		pane.add(parameterDimension.getLabel(), 0, 2);
		pane.add(parameterDimension.getDropdown(), 1, 2, 3, 1);
		// Chunk status parameters
		if (parameterAnvilVersion.getValue().compareTo(AnvilVersion.VERSION_1_20) >= 0) {
			pane.add(parameterChunkStatus120.getLabel(), 0, 3);
			pane.add(parameterChunkStatus120.getDropdown(), 1, 3, 3, 1);
		}
		else if (parameterAnvilVersion.getValue().compareTo(AnvilVersion.VERSION_1_14) >= 0) {
			pane.add(parameterChunkStatus114.getLabel(), 0, 3);
			pane.add(parameterChunkStatus114.getDropdown(), 1, 3, 3, 1);
		}
		else if (parameterAnvilVersion.getValue().compareTo(AnvilVersion.VERSION_1_13) >= 0) {
			pane.add(parameterChunkStatus113.getLabel(), 0, 3);
			pane.add(parameterChunkStatus113.getDropdown(), 1, 3, 3, 1);
		}
		else {
			pane.add(parameterChunkDecoration112.getLabel(), 0, 3);
			pane.add(parameterChunkDecoration112.getDropdown(), 1, 3, 3, 1);
		}
		// World type parameter
		pane.add(parameterWorldType.getLabel(), 0, 4);
		pane.add(parameterWorldType.getDropdown(), 1, 4, 3, 1);
		// Parameters row 1
		// Parameters column 2
		// Anvil version parameter
		pane.add(parameterAnvilVersion.getLabel(), 4, 0);
		pane.add(parameterAnvilVersion.getDropdown(), 5, 0);
		// Game mode parameter
		pane.add(parameterGameMode.getLabel(), 4, 1);
		pane.add(parameterGameMode.getDropdown(), 5, 1);
		// Difficulty parameter
		pane.add(parameterDifficulty.getLabel(), 4, 2);
		pane.add(parameterDifficulty.getDropdown(), 5, 2);
		// Seed parameter
		pane.add(parameterSeed.getLabel(), 4, 3);
		pane.add(parameterSeed.getField(), 5, 3);
		// Custom world type name parameter
		pane.add(parameterCustomWorldTypeName.getLabel(), 4, 4);
		pane.add(parameterCustomWorldTypeName.getField(), 5, 4);
		// Parameters row 2
		// Save directory parameter
		pane.add(parameterSaveDirectory.getLabel(), 0, 5);
		pane.add(parameterSaveDirectory.getField(), 1, 5, 5, 1);
		pane.add(parameterSaveDirectory.getDialogButton(), 6, 5);
		// Level name parameter
		pane.add(parameterLevelName.getLabel(), 0, 6);
		pane.add(parameterLevelName.getField(), 1, 6, 5, 1);
		// Export button and warnings row
		// Export button
		pane.add(exportButton, 1, 7);
		// Export warnings
		pane.add(exportWarningLabel, 1, 8, 5, 1);
		
		// Update chunk status parameter on anvil version change
		parameterAnvilVersion.addChengeEventHandler(e -> {
			pane.getChildren().remove(parameterChunkStatus120.getLabel());
			pane.getChildren().remove(parameterChunkStatus120.getDropdown());
			pane.getChildren().remove(parameterChunkStatus114.getLabel());
			pane.getChildren().remove(parameterChunkStatus114.getDropdown());
			pane.getChildren().remove(parameterChunkStatus113.getLabel());
			pane.getChildren().remove(parameterChunkStatus113.getDropdown());
			pane.getChildren().remove(parameterChunkDecoration112.getLabel());
			pane.getChildren().remove(parameterChunkDecoration112.getDropdown());
			
			// Chunk status parameters
			if (parameterAnvilVersion.getValue().compareTo(AnvilVersion.VERSION_1_20) >= 0) {
				pane.add(parameterChunkStatus120.getLabel(), 0, 3);
				pane.add(parameterChunkStatus120.getDropdown(), 1, 3, 3, 1);
			}
			else if (parameterAnvilVersion.getValue().compareTo(AnvilVersion.VERSION_1_14) >= 0) {
				pane.add(parameterChunkStatus114.getLabel(), 0, 3);
				pane.add(parameterChunkStatus114.getDropdown(), 1, 3, 3, 1);
			}
			else if (parameterAnvilVersion.getValue().compareTo(AnvilVersion.VERSION_1_13) >= 0) {
				pane.add(parameterChunkStatus113.getLabel(), 0, 3);
				pane.add(parameterChunkStatus113.getDropdown(), 1, 3, 3, 1);
			}
			else {
				pane.add(parameterChunkDecoration112.getLabel(), 0, 3);
				pane.add(parameterChunkDecoration112.getDropdown(), 1, 3, 3, 1);
			}
		});
		
		EventHandler<ModuleApplyParametersEvent> applyHandler = e -> {
			parameterSaveDirectory.applyUiValue();
			parameterLevelName.applyUiValue();
			parameterAnvilVersion.applyUiValue();
			
			parameterExtent.applyUiValue();
			parameterGrouping.applyUiValue();

			parameterWorldType.applyUiValue();
			parameterCustomWorldTypeName.applyUiValue();
			parameterSeed.applyUiValue();
			parameterGameMode.applyUiValue();
			parameterDifficulty.applyUiValue();

			parameterDimension.applyUiValue();
			parameterChunkDecoration112.applyUiValue();
			parameterChunkStatus113.applyUiValue();
			parameterChunkStatus114.applyUiValue();
		};
		
		return applyHandler;
	}
	
	private void updateExportWarning(Label warningLabel, AnvilVersion anvilVersion, WorldType worldType, ChunkDecoration112 chunkDecoration112, ChunkStatus113 chunkStatus113, ChunkStatus114 chunkStatus114, ChunkStatus120 chunkStatus120) {
		String warningText = "";
		
		if (anvilVersion == AnvilVersion.VERSION_NA) {
			warningText += (warningText.isEmpty() ? "" : "\n")
					+ "You have selected anvil version \"" + anvilVersion + "\". This won't work, why is it even there?";
		}
		else if (anvilVersion.compareTo(AnvilVersion.VERSION_1_20) >= 0) {
			if (chunkStatus120 != ChunkStatus120.FULL) {
				warningText += (warningText.isEmpty() ? "" : "\n")
						+ "The chunk status is not set to FULL, this may not work as expected. You are on your own now!";
			}

			if (chunkStatus120.compareTo(ChunkStatus120.FEATURES) < 0) {
				if (worldType.getName() == "flat") {
					warningText += (warningText.isEmpty() ? "" : "\n")
							+ "World type \"" + worldType.name() + "\" uses the Superflat generator. Minecraft will not decorate the generated chunks.";
				}
			}
		}
		else if (anvilVersion.compareTo(AnvilVersion.VERSION_1_18) >= 0) {
			if (chunkStatus114 != ChunkStatus114.FULL) {
				warningText += (warningText.isEmpty() ? "" : "\n")
				+ "The chunk status is not set to FULL, this may not work as expected. You are on your own now!";
			}
			
			if (chunkStatus114.compareTo(ChunkStatus114.FEATURES) < 0) {
				if (worldType.getName() == "flat") {
					warningText += (warningText.isEmpty() ? "" : "\n")
					+ "World type \"" + worldType.name() + "\" uses the Superflat generator. Minecraft will not decorate the generated chunks.";
				}
			}
		}
		else if (anvilVersion.compareTo(AnvilVersion.VERSION_1_17) >= 0) {
			if (chunkStatus114 != ChunkStatus114.FULL) {
				warningText += (warningText.isEmpty() ? "" : "\n")
				+ "The chunk status is not set to FULL, this may not work as expected. You are on your own now!";
			}
			
			if (chunkStatus114.compareTo(ChunkStatus114.FEATURES) < 0) {
				if (worldType.getName() == "flat") {
					warningText += (warningText.isEmpty() ? "" : "\n")
					+ "World type \"" + worldType.name() + "\" uses the Superflat generator. Minecraft will not decorate the generated chunks.";
				}
				else {
					warningText += (warningText.isEmpty() ? "" : "\n")
					+ "Minecraft 1.17 versions will not decorate chunks acording to exported biomes!";
				}
			}
		}
		else if (anvilVersion.compareTo(AnvilVersion.VERSION_1_16) >= 0) {
			if (chunkStatus114 != ChunkStatus114.FULL) {
				warningText += (warningText.isEmpty() ? "" : "\n")
				+ "The chunk status is not set to FULL, this may not work as expected. You are on your own now!";
			}
			
			if (chunkStatus114.compareTo(ChunkStatus114.FEATURES) < 0) {
				if (worldType.getName() == "flat") {
					warningText += (warningText.isEmpty() ? "" : "\n")
					+ "World type \"" + worldType.name() + "\" uses the Superflat generator. Minecraft will not decorate the generated chunks.";
				}
				else {
					warningText += (warningText.isEmpty() ? "" : "\n")
					+ "Minecraft 1.16 versions will not decorate chunks acording to exported biomes!";
				}
			}
		}
		else if (anvilVersion.compareTo(AnvilVersion.VERSION_1_15) >= 0) {
			if (chunkStatus114 != ChunkStatus114.FULL) {
				warningText += (warningText.isEmpty() ? "" : "\n")
				+ "The chunk status is not set to FULL, this may not work as expected. You are on your own now!";
			}
			
			if (chunkStatus114.compareTo(ChunkStatus114.FEATURES) < 0) {
				if (worldType.getName() == "flat") {
					warningText += (warningText.isEmpty() ? "" : "\n")
					+ "World type \"" + worldType.name() + "\" uses the Superflat generator. Minecraft will not decorate the generated chunks.";
				}
			}
		}
		else if (anvilVersion.compareTo(AnvilVersion.VERSION_1_14) >= 0) {
			if (chunkStatus114 != ChunkStatus114.FULL) {
				warningText += (warningText.isEmpty() ? "" : "\n")
				+ "The chunk status is not set to FULL, this may not work as expected. You are on your own now!";
			}
			
			if (chunkStatus114.compareTo(ChunkStatus114.FEATURES) < 0) {
				if (worldType.getName() == "flat") {
					warningText += (warningText.isEmpty() ? "" : "\n")
					+ "World type \"" + worldType.name() + "\" uses the Superflat generator. Minecraft will not decorate the generated chunks.";
				}
				else {
					warningText += (warningText.isEmpty() ? "" : "\n")
					+ "Minecraft 1.14 versions will not decorate chunks acording to exported biomes!";
				}
			}
		}
		else if (anvilVersion.compareTo(AnvilVersion.VERSION_1_13) >= 0) {
			if (chunkStatus113 != ChunkStatus113.DECORATED) {
				warningText += (warningText.isEmpty() ? "" : "\n")
				+ "The chunk status is not set to DECORATED, this may not work as expected. You are on your own now!";
			}
			
			if (chunkStatus113.compareTo(ChunkStatus113.DECORATED) < 0) {
				if (worldType.getName() == "flat") {
					warningText += (warningText.isEmpty() ? "" : "\n")
					+ "World type \"" + worldType.name() + "\" uses the Superflat generator. Minecraft will not decorate the generated chunks.";
				}
			}
		}
		else {
			if (chunkDecoration112 == ChunkDecoration112.UNDECORATED) {
				if (worldType.getName() == "flat") {
					warningText += (warningText.isEmpty() ? "" : "\n")
					+ "World type \"" + worldType.name() + "\" uses the Superflat generator. Minecraft will not decorate the generated chunks.";
				}
			}
		}
		
		
		if (!warningText.isEmpty()) {
			warningLabel.setText(warningText);
			warningLabel.setVisible(true);
		}
		else {
			warningLabel.setVisible(false);
		}
	}
	
	private void exportWorldFile(
			WorldExtent extent,
			boolean useExtentHeight,
			int xGrouping,
			int zGrouping,
			File minecraftSaveDirectory,
			String levelName,
			AnvilVersion anvilVersion,
			Dimension dimension,
			IChunkStatus chunkStatus,
			WorldType worldType,
			String customWorldTypeName,
			long seed,
			GameMode gameMode,
			Difficulty difficulty,
			Pane parentPane
			) {
		logger.info("Initializing minecraft save export");
		
		File worldSaveDirectory = new File(minecraftSaveDirectory, levelName);
		
		logger.debug("Checking if minecraft save exists");
		if (worldSaveDirectory.exists()) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Export world exists");
			alert.setHeaderText("WARNING: This world save already exists, do you want to overwrite it?");
			alert.setContentText("Are you sure you want to overwrite this world?\nThe old world will be lost forever! (A long time!)");
			
			ButtonType buttonTypeAbort = new ButtonType("Abort");
			ButtonType buttonTypeOverwrite = new ButtonType("Overwrite");
			
			alert.getButtonTypes().setAll(buttonTypeAbort, buttonTypeOverwrite);
			Optional<ButtonType> result = alert.showAndWait();
			
			if (result.get() != buttonTypeOverwrite) return;
		}
		
		logger.debug("Constructiong minecraft export buld region");
		int minSectionY = anvilVersion.compareTo(AnvilVersion.VERSION_1_18) >= 0 ? -4 : 0;
		int maxSectionY = anvilVersion.compareTo(AnvilVersion.VERSION_1_18) >= 0 ? 19 : 15;
		ExportBuildRegion exportBuildRegion = new ExportBuildRegion((int) extent.getX(), (int) extent.getZ(), (int) extent.getWidth(), (int) extent.getLength(), xGrouping, zGrouping, minSectionY, maxSectionY);
		
		logger.debug("Initializing minecraft save export task");
		
		ExportTask task = new ExportTask(
				ModuleMinecraftWorldExport.this,
				getInput(0), //Blockspace
				getInput(1), //Biome
				exportBuildRegion,
				minecraftSaveDirectory,
				levelName,
				anvilVersion,
				new Generator(worldType, customWorldTypeName),
				seed,
				gameMode,
				difficulty,
				dimension,
				chunkStatus,
				parentPane);
		
		EventHandler<WindowEvent> exportprogressWindowCloseEvent = EventHandler -> {
			logger.debug("Canceling minecraft save export");
			task.cancel();
			logger.info("Canceled minecraft save export");
		};
		
		logger.debug("Opening minecraft save export progress viewer");
		ToggleButton pinButton = UiWindowUtil.constructPinButton();
		UiWindowUtil.openPinnableUtilityWindow(new ExportProgressPane(exportBuildRegion, task, pinButton), "Minecraft worldExport", true, exportprogressWindowCloseEvent, pinButton.selectedProperty());
		logger.info("Start minecraft save export task");
		new Thread(task, "Minecraft world exporter task thread").start();
	}
}
