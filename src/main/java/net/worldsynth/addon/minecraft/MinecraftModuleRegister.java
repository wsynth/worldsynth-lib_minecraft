/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft;

import net.worldsynth.addon.minecraft.module.anvil.ModuleMinecraftWorldExport;
import net.worldsynth.addon.minecraft.module.anvil.ModuleMinecraftWorldImport;
import net.worldsynth.addon.minecraft.module.customobject.ModuleMinecraftNbtStructureExport;
import net.worldsynth.addon.minecraft.module.customobject.ModuleMinecraftSchematicExport;
import net.worldsynth.addon.minecraft.module.customobject.ModuleMinecraftSpongeSchematicExport;
import net.worldsynth.module.AbstractModuleRegister;
import net.worldsynth.module.ClassNotModuleExeption;

public class MinecraftModuleRegister extends AbstractModuleRegister {
	
	public MinecraftModuleRegister() {
		super();
		
		try {
			registerModule(ModuleMinecraftWorldExport.class, "\\Minecraft");
			registerModule(ModuleMinecraftWorldImport.class, "\\Minecraft");

			registerModule(ModuleMinecraftSchematicExport.class, "\\Minecraft\\Schematic");
			registerModule(ModuleMinecraftSpongeSchematicExport.class, "\\Minecraft\\Schematic");
			registerModule(ModuleMinecraftNbtStructureExport.class, "\\Minecraft\\Schematic");
		} catch (ClassNotModuleExeption e) {
			throw new RuntimeException(e);
		}
	}
}
