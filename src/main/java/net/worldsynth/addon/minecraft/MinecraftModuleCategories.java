/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft;

import net.worldsynth.module.IModuleCategory;

import javafx.scene.paint.Color;

public enum MinecraftModuleCategories implements IModuleCategory {
	MINECRAFT{
		@Override
		public Color classColor() {return Color.rgb(250, 250, 250);}
	};

	
}
