/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.chunk;

import java.util.ArrayList;
import java.util.List;

import com.github.steveice10.opennbt.tag.builtin.ByteTag;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.IntTag;
import com.github.steveice10.opennbt.tag.builtin.ListTag;
import com.github.steveice10.opennbt.tag.builtin.LongArrayTag;
import com.github.steveice10.opennbt.tag.builtin.LongTag;
import com.github.steveice10.opennbt.tag.builtin.ShortTag;
import com.github.steveice10.opennbt.tag.builtin.StringTag;

import net.worldsynth.addon.minecraft.biome.MinecraftBiome;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialState;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilVersion;
import net.worldsynth.addon.minecraft.minecraft.anvil.ChunkStatus114;
import net.worldsynth.addon.minecraft.minecraft.anvil.ChunkStatus120;
import net.worldsynth.addon.minecraft.minecraft.anvil.util.WastefulBitArrayUtil;

public class ChunkWriter118 extends AnvilChunkWriter<AnvilChunk118> {
	
	protected final CompoundTag chunkCompund;
	
	public ChunkWriter118(AnvilChunk118 chunk) {
		super(chunk);
		AnvilVersion anvilVersion = chunk.getAnvilVersion();
		
		chunkCompund = new CompoundTag(ChunkLoader118.TAG_CHUNK);
		chunkCompund.put(new IntTag(ChunkLoader118.TAG_DATAVERSION, anvilVersion.getDataVersion()));
		
		chunkCompund.put(new IntTag(ChunkLoader118.TAG_XPOS, chunk.getChunkX()));
		chunkCompund.put(new IntTag(ChunkLoader118.TAG_ZPOS, chunk.getChunkZ()));
		
		chunkCompund.put(new IntTag(ChunkLoader118.TAG_YPOS, chunk.minSectionY()));
		
		chunkCompund.put(new LongTag(ChunkLoader118.TAG_INHABITEDTIME, chunk.getInhabitedTime()));
		chunkCompund.put(new LongTag(ChunkLoader118.TAG_LASTUPDATE, chunk.getLastUpdate()));
		
		chunkCompund.put(new ListTag(ChunkLoader118.TAG_ENTITIES, chunk.getEntitiesList()));
		
		chunkCompund.put(createTileEntitiesListTag(ChunkLoader118.TAG_BLOCKENTITIES));
		chunkCompund.put(createTileTicksListTag(ChunkLoader118.TAG_BLOCKTICKS));
		chunkCompund.put(createLiquidTicksListTag(ChunkLoader118.TAG_FLUIDTICKS));
		
		chunkCompund.put(new StringTag(ChunkLoader118.TAG_STATUS, chunk.getChunkStatus().getName()));

		List<List<Short>> chunkLightsLists = null;
		if (chunk.getChunkStatus() instanceof ChunkStatus114) {
			ChunkStatus114 chunkStatus = (ChunkStatus114) chunk.getChunkStatus();
			chunkLightsLists = chunkStatus.compareTo(ChunkStatus114.FULL) >= 0 ? null : new ArrayList<>();
		}
		else if (chunk.getChunkStatus() instanceof ChunkStatus120) {
			ChunkStatus120 chunkStatus = (ChunkStatus120) chunk.getChunkStatus();
			chunkLightsLists = chunkStatus.compareTo(ChunkStatus120.FULL) >= 0 ? null : new ArrayList<>();
		}

		chunkCompund.put(chunkSectionsList(chunkLightsLists));
		
		if (chunkLightsLists != null) {
			ListTag chunkLightsListsTag = new ListTag(ChunkLoader118.TAG_LIGHTS);
			for (List<Short> sectionLightsList: chunkLightsLists) {
				ListTag sectionLightsListTag = new ListTag("");
				for (short s: sectionLightsList) {
					sectionLightsListTag.add(new ShortTag("", s));
				}
				chunkLightsListsTag.add(sectionLightsListTag);
			}
			chunkCompund.put(chunkLightsListsTag);
		}
		
		
	}
	
	@Override
	public CompoundTag getChunkCompund() {
		return chunkCompund;
	}
	
	protected ListTag chunkSectionsList(List<List<Short>> chunkLightsLists) {
		ListTag sectionsListTag = new ListTag(ChunkLoader118.TAG_SECTIONS);
		
		// Add all the other sections
		for (AnvilChunkSection118 section: chunk.getChunkSections()) {
			List<Short> sectionLightsList = chunkLightsLists == null ? null : new ArrayList<Short>();
			sectionsListTag.add(chunkSectionToNbt(section, sectionLightsList));
			
			if (sectionLightsList != null) {
				chunkLightsLists.add(sectionLightsList);
			}
		}

		return sectionsListTag;
	}
	
	protected CompoundTag chunkSectionToNbt(AnvilChunkSection118 chunkSection, List<Short> sectionLightsList) {
		CompoundTag sectionCompoundTag = new CompoundTag(ChunkLoader118.TAG_SECTION);
		
		sectionCompoundTag.put(new ByteTag(ChunkLoader118.TAG_Y, (byte) chunkSection.getSectionY()));
//		sectionCompoundTag.put(new ByteArrayTag(ChunkLoader118.TAG_BLOCKLIGHT, chunkSection.getBlockLightsArray()));
//		sectionCompoundTag.put(new ByteArrayTag(ChunkLoader118.TAG_SKYLIGHT, chunkSection.getSkyLightsArray()));
		
		// Put block_states
		CompoundTag blockStatesCompoundTag = new CompoundTag(ChunkLoader118.TAG_BLOCKSTATES);
		
		MinecraftMaterialState[] materials = chunkSection.getMaterialsArray();
		MaterialPalette materialPalette = new MaterialPalette(anvilVersion);
		int[] blockStateIndexes = new int[4096];
		
		for (int i = 0; i < 4096; i++) {
			blockStateIndexes[i] = materialPalette.getPaletteIndex(materials[i]);
		}
		
		blockStatesCompoundTag.put(materialPalette.toNbt(ChunkLoader118.TAG_PALETTE));
		if (materialPalette.getPaletteSize() > 1) {
			int bitsPerBlockstateEntry = Math.max((int) Math.ceil(Math.log(materialPalette.getPaletteSize()) / Math.log(2)), 4);
			blockStatesCompoundTag.put(new LongArrayTag(ChunkLoader118.TAG_BLOCKSTATES_DATA, WastefulBitArrayUtil.toLongBitArray(bitsPerBlockstateEntry, blockStateIndexes)));
		}
		
		sectionCompoundTag.put(blockStatesCompoundTag);
		
		// Fill in list of light sources if needed
		if (sectionLightsList != null) {
			boolean hasLightSources = false;
			for (int i = 0; i < materialPalette.getPaletteSize(); i++) {
				if (materialPalette.getMaterialState(i).getLightLevel() > 0) {
					hasLightSources = true;
					break;
				}
			}
			
			if (hasLightSources) {
				for (short i = 0; i < 4096; i++) {
					if (chunkSection.getMaterialsArray()[i].getLightLevel() > 0) {
						// Block array index is YZX packed, light index is ZYX packed, translate that shit.
						int x = i & 0xF;
						int y = i >>> 8 & 0xF;
						int z = i >>> 4 & 0xF;
						short zyxPacked = (short) (z << 8 | y << 4 | x);
						sectionLightsList.add(zyxPacked);
					}
				}
			}
		}
		
		// Put biomes
		CompoundTag biomesCompoundTag = new CompoundTag(ChunkLoader118.TAG_BIOMES);
		MinecraftBiome[] biomes = chunkSection.getBiomesArray();
		BiomePalette biomePalette = new BiomePalette(anvilVersion);
		int[] biomeIndexes = new int[64];
		
		for (int i = 0; i < 64; i++) {
			biomeIndexes[i] = biomePalette.getPaletteIndex(biomes[i]);
		}
		
		biomesCompoundTag.put(biomePalette.toNbt(ChunkLoader118.TAG_PALETTE));
		if (biomePalette.getPaletteSize() > 1) {
			int bitsPerBiomeEntry = (int) Math.ceil(Math.log(biomePalette.getPaletteSize()) / Math.log(2));
			biomesCompoundTag.put(new LongArrayTag(ChunkLoader118.TAG_BIOMES_DATA, WastefulBitArrayUtil.toLongBitArray(bitsPerBiomeEntry, biomeIndexes)));
		}
		
		sectionCompoundTag.put(biomesCompoundTag);
		
		return sectionCompoundTag;
	}
}
