/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.chunk;

import com.github.steveice10.opennbt.tag.builtin.CompoundTag;

public interface AnvilChunkLoader<T extends SectionedAnvilChunk<?>> {
	
	public CompoundTag chunkToNbt(T chunk);
	public SectionedAnvilChunk<?> chunkFromNbt(CompoundTag chunkTag);
}
