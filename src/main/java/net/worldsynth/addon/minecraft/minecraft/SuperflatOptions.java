/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft;

import java.util.ArrayList;

import com.github.steveice10.opennbt.tag.builtin.ByteTag;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.IntTag;
import com.github.steveice10.opennbt.tag.builtin.ListTag;
import com.github.steveice10.opennbt.tag.builtin.StringTag;

import net.worldsynth.addon.minecraft.biome.MinecraftBiome;
import net.worldsynth.addon.minecraft.material.MinecraftMaterial;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialState;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilVersion;
import net.worldsynth.biome.BiomeRegistry;
import net.worldsynth.material.MaterialRegistry;

public class SuperflatOptions {
	
	public static final SuperflatOptions VOID = new SuperflatOptions("minecraft:the_void",
			new SuperflatLayer("minecraft:air", 1));
	
	public static final SuperflatOptions FLAT = new SuperflatOptions("minecraft:plains",
			new SuperflatLayer("minecraft:bedrock", 1),
			new SuperflatLayer("minecraft:dirt", 2),
			new SuperflatLayer("minecraft:grass_block", 1));
	
	
	private MinecraftBiome biome;
	private ArrayList<SuperflatLayer> layers = new ArrayList<SuperflatLayer>();
	
	public SuperflatOptions(String biome, SuperflatLayer ... layers) {
		this.biome = (MinecraftBiome) BiomeRegistry.getBiome(biome, "minecraft");
		for (SuperflatLayer layer: layers) {
			this.layers.add(layer);
		}
	}
	
	public String optionsString112(AnvilVersion version) {
		//Version number
		StringBuilder sb = new StringBuilder("3;");
		
		//Blocks
		boolean firstMaterial = true;
		for (SuperflatLayer layer: layers) {
			if (firstMaterial) {
				firstMaterial = false;
			}
			else {
				sb.append(",");
			}
			
			if (layer.getLayerHeight() > 1) {
				sb.append(layer.getLayerHeight());
				sb.append("*");
			}
			sb.append(layer.getLayerMaterial().getNumericAnvilId(version.getMaterialVersionName()));
			
			byte meta = layer.getLayerMaterial().getNumericAnvilMeta(version.getMaterialVersionName());
			if (meta != 0) {
				sb.append(":");
				sb.append(meta);
			}
		}
		
		//Biome
		sb.append(";");
		sb.append(biome.getNumericAnvilId(version.getMaterialVersionName()));
		sb.append(";");
		
		return sb.toString();
	}
	
	public CompoundTag generatorOptionsTag113(AnvilVersion version) {
		CompoundTag generatorOptionsCmpound = new CompoundTag("generatorOptions");
		
		CompoundTag structuresCompound = new CompoundTag("structures");
		generatorOptionsCmpound.put(structuresCompound);
		structuresCompound.put(new CompoundTag("decoration"));
		
		ListTag layersList = new ListTag("layers");
		generatorOptionsCmpound.put(layersList);
		for (SuperflatLayer layer: layers) {
			CompoundTag layerCompound = new CompoundTag("");
			layerCompound.put(new StringTag("block", layer.getLayerMaterial().getFlattenedAnvilId(version.getMaterialVersionName())));
			layerCompound.put(new IntTag("height", layer.getLayerHeight()));
			layersList.add(layerCompound);
		}
		
		generatorOptionsCmpound.put(new StringTag("biome", biome.getIdName()));
		
		return generatorOptionsCmpound;
	}
	
	public CompoundTag generatorCompoundTag116(AnvilVersion version, long seed) {
		CompoundTag generatorCompound = new CompoundTag("generator");
		generatorCompound.put(new StringTag("type", "minecraft:flat"));
		
		CompoundTag settingsCompound = new CompoundTag("settings");
		generatorCompound.put(settingsCompound);
		
		CompoundTag structuresCompound = new CompoundTag("structures");
		settingsCompound.put(structuresCompound);
		structuresCompound.put(new CompoundTag("structures"));
		
		ListTag layersList = new ListTag("layers");
		settingsCompound.put(layersList);
		for (SuperflatLayer layer: layers) {
			CompoundTag layerCompound = new CompoundTag("");
			
			String block = layer.getLayerMaterial().getFlattenedAnvilId(version.getVersionName());
			layerCompound.put(new StringTag("block", block));
			layerCompound.put(new IntTag("height", layer.getLayerHeight()));
			
			layersList.add(layerCompound);
		}
		
		settingsCompound.put(new StringTag("biome", biome.getIdName()));
		settingsCompound.put(new ByteTag("features", (byte) 0));
		settingsCompound.put(new ByteTag("lakes", (byte) 0));
		
		return generatorCompound;
	}
	
	public ArrayList<SuperflatLayer> getLayers() {
		return layers;
	}
	
	public MinecraftBiome getBiome() {
		return biome;
	}
	
	public static class SuperflatLayer {
		private MinecraftMaterialState layerMaterial;
		private int layerHeight;
		
		public SuperflatLayer(String layerMaterial, int layerHeight) {
			this.layerMaterial = ((MinecraftMaterial) MaterialRegistry.getMaterial(layerMaterial, "minecraft")).getDefaultState();
			this.layerHeight = layerHeight;
		}
		
		public MinecraftMaterialState getLayerMaterial() {
			return layerMaterial;
		}
		
		public int getLayerHeight() {
			return layerHeight;
		}
	}
}