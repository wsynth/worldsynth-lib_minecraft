/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

public class CachingChunkHandler<T extends IChunk> {
	
	private final IChunkStorage<T> chunkStorage;
	
	private final HashMap<Coordinate, T> chunkCache = new HashMap<>();
	private final HashSet<Coordinate> noChunkCache = new HashSet<>();
	
	public CachingChunkHandler(IChunkStorage<T> chunkStorage) {
		this.chunkStorage = chunkStorage;
	}
	
	public T getChunk(int chunkX, int chunkZ) {
		Coordinate chunkCoord = new Coordinate(chunkX, chunkZ);
		if (noChunkCache.contains(chunkCoord)) return null;
		
		T chunk = chunkCache.get(chunkCoord);
		if (chunk == null) {
			try {
				chunk = chunkStorage.readChunk(chunkX, chunkZ);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			if (chunk != null) {
				chunkCache.put(chunkCoord, chunk);
			}
			else {
				noChunkCache.add(chunkCoord);
			}
		}
		return chunk;
	}
	
	public void addChunk(T chunk, boolean replace) {
		int chunkX = chunk.getChunkX();
		int chunkZ = chunk.getChunkZ();
		Coordinate chunkCoord = new Coordinate(chunkX, chunkZ);
		
		if (replace) {
			noChunkCache.remove(chunkCoord);
			chunkCache.put(chunkCoord, chunk);
		}
		else if (!chunkExists(chunkX, chunkZ)) {
			noChunkCache.remove(chunkCoord);
			chunkCache.put(chunkCoord, chunk);
		}
	}
	
	public boolean chunkExists(int chunkX, int chunkZ) {
		Coordinate chunkCoord = new Coordinate(chunkX, chunkZ);
		
		if (noChunkCache.contains(chunkCoord)) return false;
		else if (chunkCache.containsKey(new Coordinate(chunkX, chunkZ))) return true;
		
		boolean exists = false;
		try {
			exists = chunkStorage.chunkExists(chunkX, chunkZ);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (!exists) {
			noChunkCache.add(chunkCoord);
		}
		
		return exists;
	}
	
	public void save() throws IOException {
		for (T chunk: chunkCache.values()) {
			chunkStorage.writeChunk(chunk);
		}
		chunkCache.clear();
	}
	
	public void close() throws Exception {
		chunkStorage.close();
	}
}
