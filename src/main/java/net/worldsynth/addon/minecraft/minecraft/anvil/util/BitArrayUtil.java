/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.util;

public class BitArrayUtil {
	
	public static long[] toLongBitArray(int bitsPerEntry, int[] inputArray) {
		int bitArrayLength = (int) Math.ceil(((double) inputArray.length * (double) bitsPerEntry) / 64.0);
		int maxEntryValue = (int) Math.pow(2, bitsPerEntry) - 1;
		
		long[] bitArray = new long[bitArrayLength];
		for (int entryIndex = 0; entryIndex < inputArray.length; entryIndex++) {
			int bitArrayIndex = (bitsPerEntry * entryIndex) / 64;
			int bitArraySubIndex = (bitsPerEntry * entryIndex) % 64;
			
			bitArray[bitArrayIndex] = bitArray[bitArrayIndex] | (((long) (inputArray[entryIndex] & maxEntryValue)) << bitArraySubIndex);
			if (bitArrayIndex + 1 < bitArrayLength && bitArraySubIndex + bitsPerEntry > 64) {
				bitArray[bitArrayIndex + 1] = bitArray[bitArrayIndex + 1] | (((long) (inputArray[entryIndex] & maxEntryValue)) >>> (64 - bitArraySubIndex));
			}
		}
		
		return bitArray;
	}
	
	public static int[] intArrayFromLongBitArray(int bitsPerEntry, int entries, long[] bitArray) {
		if (bitsPerEntry * entries > Long.SIZE * bitArray.length) {
			throw new IllegalArgumentException("Expected entries is more than possible entries");
		}
		
		int[] outputArray = new int[entries];
		int maxEntryValue = (int) Math.pow(2, bitsPerEntry) - 1;
		
		for (int entryIndex = 0; entryIndex < entries; entryIndex++) {
			int bitArrayIndex = (bitsPerEntry * entryIndex) / 64;
			int bitArraySubIndex = (bitsPerEntry * entryIndex) % 64;
			
			outputArray[entryIndex] = (int) (bitArray[bitArrayIndex] >>> bitArraySubIndex) & maxEntryValue;
			if (bitArrayIndex + 1 < entries && bitArraySubIndex + bitsPerEntry > 64) {
				outputArray[entryIndex] = (int) (outputArray[entryIndex] | (bitArray[bitArrayIndex + 1] << (64 - bitArraySubIndex))) & maxEntryValue;
			}
		}
		
		return outputArray;
	}
}
