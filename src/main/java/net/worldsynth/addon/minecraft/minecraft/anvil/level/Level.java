/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.level;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import com.github.steveice10.opennbt.NBTIO;
import com.github.steveice10.opennbt.SNBTIO;
import com.github.steveice10.opennbt.tag.builtin.ByteTag;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.IntTag;
import com.github.steveice10.opennbt.tag.builtin.LongTag;
import com.github.steveice10.opennbt.tag.builtin.StringTag;

import net.worldsynth.addon.minecraft.minecraft.Difficulty;
import net.worldsynth.addon.minecraft.minecraft.GameMode;
import net.worldsynth.addon.minecraft.minecraft.Generator;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilVersion;

public abstract class Level {

	protected static final String TAG_LEVEL                      = "Level";
	protected static final String TAG_DATA                       = "Data";
	
	protected static final String TAG_LEVEL_NAME                 = "LevelName";

	// Version compound
	protected static final String TAG_VERSION_COMPOUND           = "Version";
	protected static final String TAG_VERSION_ID                 = "Id";
	protected static final String TAG_VERSION_NAME               = "Name";
	protected static final String TAG_VERSION_SNAPSHOT           = "Snapshot";

	// Data version tag
	protected static final String TAG_DATA_VERSION               = "DataVersion";
	protected static final String TAG_NBT_VERSION                = "version";

	// Time tags
	protected static final String TAG_LAST_PLAYED                = "LastPlayed";
	
	// Difficulty tags
	protected static final String TAG_DIFFICULTY                 = "Difficulty";
	protected static final String TAG_DIFFICULTY_LOCKED          = "DifficultyLocked";
	protected static final String TAG_HARDCORE                   = "hardcore";
	protected static final String TAG_GAME_TYPE                  = "GameType";
	
	// Generator tags
	protected static final String TAG_GENERATOR_NAME             = "generatorName";
	protected static final String TAG_GENERATOR_OPTIONS          = "generatorOptions";
	protected static final String TAG_GENERATOR_VERSION          = "generatorVersion";
	protected static final String TAG_MAP_FEATURES               = "MapFeatures";
	protected static final String TAG_RANDOM_SEED                = "RandomSeed";
	
	// Initialized tag
	protected static final String TAG_INITIALIZED                = "initialized";
	
	protected static final String TAG_ALLOW_COMMANDS             = "allowCommands";
	
	// Spawn tags
	protected static final String TAG_SPAWN_X                    = "SpawnX";
	protected static final String TAG_SPAWN_Y                    = "SpawnY";
	protected static final String TAG_SPAWN_Z                    = "SpawnZ";
	
	
	// Level compound tag
	protected final CompoundTag dataCompound;
	
	
	public Level(String levelName, AnvilVersion anvilVersion, Generator generator, long seed, GameMode gameMode, Difficulty difficulty, int spawnX, int spawnY, int spawnZ) throws IOException {
		InputStream is = getClass().getClassLoader().getResourceAsStream(defaultLevelDataResource());
		CompoundTag defaultLevelDat = (CompoundTag) SNBTIO.readTag(is);
		dataCompound = defaultLevelDat.get(TAG_DATA);
		
		dataCompound.put(new IntTag(TAG_NBT_VERSION, 19133));
		dataCompound.put(new IntTag(TAG_DATA_VERSION, anvilVersion.getDataVersion()));
		
		CompoundTag minecraftVersionCompoundTag = new CompoundTag(TAG_VERSION_COMPOUND);
		minecraftVersionCompoundTag.put(new IntTag(TAG_VERSION_ID, anvilVersion.getDataVersion()));
		minecraftVersionCompoundTag.put(new StringTag(TAG_VERSION_NAME, anvilVersion.getVersionName()));
		minecraftVersionCompoundTag.put(new ByteTag(TAG_VERSION_SNAPSHOT, isSnapshot()));
		dataCompound.put(minecraftVersionCompoundTag);
		
		dataCompound.put(new StringTag(TAG_LEVEL_NAME, levelName));
		
		putGeneratorTags(dataCompound, anvilVersion, generator, seed);
		
		dataCompound.put(new LongTag(TAG_LAST_PLAYED, System.currentTimeMillis()));

		dataCompound.put(new IntTag(TAG_GAME_TYPE, gameMode.getGameModeId()));
		dataCompound.put(new ByteTag(TAG_DIFFICULTY, difficulty.getDifficultyId()));
		dataCompound.put(new ByteTag(TAG_DIFFICULTY_LOCKED, (byte) 0));
		dataCompound.put(new ByteTag(TAG_HARDCORE, difficulty.isHardcore()));
		dataCompound.put(new ByteTag(TAG_ALLOW_COMMANDS, (byte) 1));
		
		dataCompound.put(new IntTag(TAG_SPAWN_X, spawnX));
		dataCompound.put(new IntTag(TAG_SPAWN_Y, spawnY));
		dataCompound.put(new IntTag(TAG_SPAWN_Z, spawnZ));
		
		dataCompound.put(new ByteTag(TAG_INITIALIZED, (byte) 1));
	}

	public Level(CompoundTag levelTag) {
		dataCompound = levelTag.get(TAG_DATA);
	}

	public void save(File levelFile) throws IOException {
		CompoundTag root = new CompoundTag("");
		root.put(dataCompound);
		NBTIO.writeFile(root, levelFile);
	}
	
	protected abstract String defaultLevelDataResource();

	protected abstract void putGeneratorTags(CompoundTag dataCompound, AnvilVersion anvilVersion, Generator generator, long seed);
	
	protected byte isSnapshot() {
		return 0;
	}
}
