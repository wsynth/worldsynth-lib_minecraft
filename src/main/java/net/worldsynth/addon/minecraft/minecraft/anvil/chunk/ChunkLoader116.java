/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.chunk;

import com.github.steveice10.opennbt.tag.builtin.CompoundTag;

public class ChunkLoader116 implements AnvilChunkLoader<AnvilChunk115> {

	// Chunk tags
	public static final String TAG_CHUNK = "Chunk";
	public static final String TAG_DATAVERSION = "DataVersion";
	public static final String TAG_LEVEL = "Level";

	public static final String TAG_STATUS = "Status";

	public static final String TAG_XPOS = "xPos";
	public static final String TAG_ZPOS = "zPos";

	public static final String TAG_LASTUPDATE = "LastUpdate";
	public static final String TAG_INHABITEDTIME = "InhabitedTime";

	public static final String TAG_SECTIONS = "Sections";
	public static final String TAG_BIOMES = "Biomes";
	public static final String TAG_ENTITIES = "Entities";
	public static final String TAG_TILEENTITIES = "TileEntities";
	public static final String TAG_TILETICKS = "TileTicks";
	public static final String TAG_LIQUIDTICKS = "LiquidTicks";

	public static final String TAG_LIGHTS = "Lights";
	public static final String TAG_POSTPROCESSING = "PostProcessing";

//	public static final String TAG_HEIGHTMAPS = "Heightmaps";
//	public static final String TAG_MOTIONBLOCKING = "MOTION_BLOCKING";
//	public static final String TAG_MOTIONBLOCKINGNOLEAVES = "MOTION_BLOCKING_NO_LEAVES";
//	public static final String TAG_OCEANFLOOR = "OCEAN_FLOOR";
//	public static final String TAG_WORLDSURFACE = "WORLD_SURFACE";

	// Chunk section tags
	public static final String TAG_SECTION = "Section";
	public static final String TAG_Y = "Y";
	public static final String TAG_BLOCKLIGHT = "BlockLight";
	public static final String TAG_SKYLIGHT = "SkyLight";

	public static final String TAG_BLOCKSTATES = "BlockStates";
	public static final String TAG_PALETTE = "Palette";

	@Override
	public CompoundTag chunkToNbt(AnvilChunk115 chunk) {
		return new ChunkWriter116(chunk).getChunkCompund();
	}

	@Override
	public SectionedAnvilChunk<?> chunkFromNbt(CompoundTag chunkTag) {
		return new ChunkReader116(chunkTag).getChunk();
	}
}
