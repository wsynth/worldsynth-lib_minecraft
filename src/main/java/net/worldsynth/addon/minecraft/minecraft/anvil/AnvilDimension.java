/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil;

import java.io.File;
import java.io.IOException;

import net.worldsynth.addon.minecraft.biome.MinecraftBiome;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialState;
import net.worldsynth.addon.minecraft.minecraft.CachingChunkHandler;
import net.worldsynth.addon.minecraft.minecraft.IChunk;
import net.worldsynth.addon.minecraft.minecraft.IDimension;
import net.worldsynth.addon.minecraft.minecraft.anvil.chunk.AnvilChunkStorage;
import net.worldsynth.addon.minecraft.minecraft.anvil.chunk.SectionedAnvilChunk;

public class AnvilDimension implements IDimension {
	
	private final CachingChunkHandler<SectionedAnvilChunk<?>> chunkHandler;
	
	public AnvilDimension(File dimensionDir) {
		AnvilChunkStorage chunkStorage = new AnvilChunkStorage(new File(dimensionDir, "region"));
		chunkHandler = new CachingChunkHandler<SectionedAnvilChunk<?>>(chunkStorage);
	}

	/*=============================================================
	 * 
	 * Implement IDimension
	 * 
	 *=============================================================*/
	
	@Override
	public MinecraftMaterialState getMaterial(int x, int y, int z) {
		return getChunk(x >> 4, z >> 4).getMaterial(x & 15, y, z & 15);
	}

	@Override
	public void setMaterial(int x, int y, int z, MinecraftMaterialState material) {
		getChunk(x >> 4, z >> 4).setMaterial(x & 15, y, z & 15, material);
	}

	@Override
	public MinecraftBiome getBiome(int x, int z) {
		return getChunk(x >> 4, z >> 4).getBiome(x & 15, z & 15);
	}

	@Override
	public void setBiome(int x, int z, MinecraftBiome biome) {
		getChunk(x >> 4, z >> 4).setBiome(x & 15, z & 15, biome);
	}

	@Override
	public MinecraftBiome getBiome(int x, int y, int z) {
		return getChunk(x >> 4, z >> 4).getBiome(x & 15, y, z & 15);
	}

	@Override
	public void setBiome(int x, int y, int z, MinecraftBiome biome) {
		getChunk(x >> 4, z >> 4).setBiome(x & 15, y, z & 15, biome);
	}

	@Override
	public byte getBlockLight(int x, int y, int z) {
		return getChunk(x >> 4, z >> 4).getBlockLight(x & 15, y, z & 15);
	}

	@Override
	public void setBlockLight(int x, int y, int z, byte blockLight) {
		getChunk(x >> 4, z >> 4).setBlockLight(x & 15, y, z & 15, blockLight);
	}

	@Override
	public byte getSkyLight(int x, int y, int z) {
		return getChunk(x >> 4, z >> 4).getSkyLight(x & 15, y, z & 15);
	}

	@Override
	public void setSkyLight(int x, int y, int z, byte skyLight) {
		getChunk(x >> 4, z >> 4).setSkyLight(x & 15, y, z & 15, skyLight);
	}
	
	/*=============================================================
	 * 
	 * Chunk handling
	 * 
	 *=============================================================*/
	
	private final IChunk nonchunk = new IChunk() {
		public int getChunkX() {
			throw new IllegalStateException("Can not get coordinates of nonexistent chunk");
		}
		public int getChunkZ() {
			throw new IllegalStateException("Can not get coordinates of nonexistent chunk");
		}
		
		@Override
		public int getMinY() {
			return 0;
		}
		@Override
		public int getMaxY() {
			return 0;
		}
		
		@Override
		public MinecraftMaterialState getMaterial(int x, int y, int z) { return null; }
		@Override
		public void setMaterial(int x, int y, int z, MinecraftMaterialState material) {}
		
		@Override
		public byte getBlockLight(int x, int y, int z) { return 0; }
		@Override
		public void setBlockLight(int x, int y, int z, byte blockLight) {}
		
		@Override
		public byte getSkyLight(int x, int y, int z) { return 0; }
		@Override
		public void setSkyLight(int x, int y, int z, byte skyLight) {}
		
		@Override
		public MinecraftBiome getBiome(int x, int z) { return null; }
		@Override
		public void setBiome(int x, int z, MinecraftBiome biome) {}
		
		@Override
		public MinecraftBiome getBiome(int x, int y, int z) { return null; }
		@Override
		public void setBiome(int x, int y, int z, MinecraftBiome biome) {}
	};
	
	public IChunk getChunk(int chunkX, int chunkZ) {
		IChunk chunk = chunkHandler.getChunk(chunkX, chunkZ);
		return chunk == null ? nonchunk : chunk;
	}
	
	public void addChunk(SectionedAnvilChunk<?> chunk, boolean replace) {
		chunkHandler.addChunk(chunk, replace);
	}
	
	public void save() throws IOException {
		chunkHandler.save();
	}
	
	public void close() throws Exception {
		chunkHandler.close();
	}
}
