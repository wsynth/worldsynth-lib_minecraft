/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.level;

import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import net.worldsynth.addon.minecraft.minecraft.Difficulty;
import net.worldsynth.addon.minecraft.minecraft.GameMode;
import net.worldsynth.addon.minecraft.minecraft.Generator;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilVersion;

import java.io.IOException;

public class Level120 extends Level116 {

	public Level120(String levelName, AnvilVersion anvilVersion, Generator generator, long seed, GameMode gameMode, Difficulty difficulty, int spawnX, int spawnY, int spawnZ) throws IOException {
		super(levelName, anvilVersion, generator, seed, gameMode, difficulty, spawnX, spawnY, spawnZ);
	}

	public Level120(CompoundTag levelTag) {
		super(levelTag);
	}
	
	@Override
	protected String defaultLevelDataResource() {
		return "defaultlevel120.snbt";
	}
}
