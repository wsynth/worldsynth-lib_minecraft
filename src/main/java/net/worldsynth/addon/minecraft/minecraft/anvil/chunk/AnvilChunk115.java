/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.chunk;

import net.worldsynth.addon.minecraft.biome.MinecraftBiome;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilVersion;

public class AnvilChunk115 extends AnvilChunk113 {
	
	public AnvilChunk115(int chunkX, int chunkZ, AnvilVersion anvilVersion) {
		super(chunkX, chunkZ, anvilVersion);
		
		biomes = new MinecraftBiome[1024];
	}

	@Override
	public MinecraftBiome getBiome(int x, int z) {
		// Biomes in chunk are 3D
		// Pick biome from the top layer
		return getBiome(x, getMaxY(), z);
	}
	
	@Override
	public void setBiome(int x, int z, MinecraftBiome biome) {
		// Biomes in chunk are 3D
		// Extrude biome in y
		for (int y = getMinY(); y < getMaxY(); y+=4) {
			setBiome(x, y, z, biome);
		}
	}
	
	@Override
	public MinecraftBiome getBiome(int x, int y, int z) {
		if (y < getMinY()) {
			// Extrude bottom biome if below min y coordinates
			y = getMinY();
		}
		else if (y >= getMaxY()) {
			// Extrude top biome if above max y coordinates
			y = getMaxY();
		}
		return biomes[(y & ~3) << 2 | z & ~3 | x >> 2];
	}
	
	@Override
	public void setBiome(int x, int y, int z, MinecraftBiome biome) {
		if (y < getMinY() || y >= getMaxY()) return;
		biomes[(y & ~3) << 2 | z & ~3 | x >> 2] = biome;
	}
	
	MinecraftBiome[] getBiomesArray() {
		return biomes;
	}
	
	void setBiomesArray(MinecraftBiome[] biomes) {
		if (biomes.length != 1024) {
			throw new IllegalArgumentException("Biomes array must have lenght 1024");
		}
		this.biomes = biomes;
	}
}
