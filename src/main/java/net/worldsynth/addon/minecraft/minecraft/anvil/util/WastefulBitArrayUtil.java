/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.util;

public class WastefulBitArrayUtil {
	
	public static long[] toLongBitArray(int bitsPerEntry, int[] inputArray) {
		int entriesPerLong = Long.SIZE / bitsPerEntry;
		int longArrayLength = (int) Math.ceil((double) inputArray.length / (double) entriesPerLong);
		int maxEntryValue = (int) Math.pow(2, bitsPerEntry) - 1;
		
		long[] bitArray = new long[longArrayLength];
		for (int entryIndex = 0; entryIndex < inputArray.length; entryIndex++) {
			int bitArrayIndex = entryIndex / entriesPerLong;
			int bitArraySubIndex = (entryIndex - bitArrayIndex * entriesPerLong) * bitsPerEntry;
			
			bitArray[bitArrayIndex] = bitArray[bitArrayIndex] | (((long) (inputArray[entryIndex] & maxEntryValue)) << bitArraySubIndex);
		}
		
		return bitArray;
	}
	
	public static int[] intArrayFromLongBitArray(int bitsPerEntry, int entries, long[] bitArray) {
		int entriesPerLong = Long.SIZE / bitsPerEntry;
		int requiredLongArrayLength = (int) Math.ceil((double) entries / (double) entriesPerLong);
		if (requiredLongArrayLength > bitArray.length) {
			throw new IllegalArgumentException("Expected entries is more than possible entries");
		}
		
		int[] outputArray = new int[entries];
		int maxEntryValue = (int) Math.pow(2, bitsPerEntry) - 1;
		
		for (int entryIndex = 0; entryIndex < entries; entryIndex++) {
			int bitArrayIndex = entryIndex / entriesPerLong;
			int bitArraySubIndex = (entryIndex - bitArrayIndex * entriesPerLong) * bitsPerEntry;
			
			outputArray[entryIndex] = (int) (bitArray[bitArrayIndex] >>> bitArraySubIndex) & maxEntryValue;
		}
		
		return outputArray;
	}
}
