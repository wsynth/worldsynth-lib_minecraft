/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft;

public enum Dimension {
	OVERWORLD(0),
	NETHER(-1),
	END(1);
	
	private final int dimensionId;
	
	private Dimension(int dimensionId) {
		this.dimensionId = dimensionId;
	}
	
	public int getDimensionId() {
		return dimensionId;
	}
}
