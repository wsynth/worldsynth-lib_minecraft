/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft;

public enum Difficulty {
	PEACEFUL(0),
	EASY(1),
	NORMAL(2),
	HARD(3),
	HARDCORE(3);
	
	private final byte difficultyId;
	
	private Difficulty(int difficultyId) {
		this.difficultyId = (byte) difficultyId;
	}
	
	public byte getDifficultyId() {
		return difficultyId;
	}
	
	public byte isHardcore() {
		return (byte) (this == HARDCORE ? 1 : 0);
	}
}
