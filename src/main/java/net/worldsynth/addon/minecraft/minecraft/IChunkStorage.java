/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft;

import java.io.IOException;

/**
 * Storage interface for saving chunks according to a save format. The type of
 * chunk the implementation may be different for different save formats.
 *
 * @param <T> The type of chunk this storage should handle
 */
public interface IChunkStorage<T extends IChunk> extends AutoCloseable {
	
	/**
	 * Checks if the given chunk exists in the chunk storage
	 * 
	 * @param chunkX The x coordinate of the chunk
	 * @param chunkZ The z coordinate of the chunk
	 * @return {@code true} if the chunk exists in storage
	 * @throws IOException
	 */
	public boolean chunkExists(int chunkX, int chunkZ) throws IOException;
	
	/**
	 * Attempts to read the given chunk from the chunk storage
	 * 
	 * @param chunkX The x coordinate of the chunk
	 * @param chunkZ The z coordinate of the chunk
	 * @return The chunk if it was found, otherwise returns {@code null}
	 * @throws IOException
	 */
	public T readChunk(int chunkX, int chunkZ) throws IOException;
	
	/**
	 * Attempts to write the given chunk to the chunk storage
	 * 
	 * @param chunk The chunk to write to storage
	 * @throws IOException
	 */
	public void writeChunk(T chunk) throws IOException;
}
