/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.chunk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.ListTag;
import com.github.steveice10.opennbt.tag.builtin.StringTag;
import com.github.steveice10.opennbt.tag.builtin.Tag;

import net.worldsynth.addon.minecraft.material.MinecraftMaterial;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialBuilder;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialProfile;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialProfile.FlattenedLookup;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialState;
import net.worldsynth.addon.minecraft.material.anvildata.AnvilDataFlattened;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilVersion;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.Property;
import net.worldsynth.material.StateProperties;

public class MaterialPalette {
	
	private AnvilVersion anvilVersion;
	
	private ArrayList<MinecraftMaterialState> paletteList = new ArrayList<MinecraftMaterialState>();
	private HashMap<MinecraftMaterialState, Integer> paletteMap = new HashMap<MinecraftMaterialState, Integer>();
	
	public static final String TAG_NAME = "Name";
	public static final String TAG_PROPERTIES = "Properties";
	
	public MaterialPalette(AnvilVersion anvilVersion) {
		this.anvilVersion = anvilVersion;
		getPaletteIndex(MinecraftMaterial.AIR.getDefaultState());
	}
	
	public MaterialPalette(AnvilVersion anvilVersion, ListTag paletteListTag) {
		this.anvilVersion = anvilVersion;
		FlattenedLookup flattenedLookup = ((MinecraftMaterialProfile) MaterialRegistry.getProfile("minecraft")).getFlattenedLookup(anvilVersion.getVersionName());
		for (Tag e: paletteListTag.getValue()) {
			CompoundTag compoundEntry = (CompoundTag) e;
			String flattenedId = ((StringTag) compoundEntry.get(TAG_NAME)).getValue();
			final ArrayList<Property> properties = new ArrayList<Property>();
			if (compoundEntry.contains(TAG_PROPERTIES)) {
				((CompoundTag) compoundEntry.get(TAG_PROPERTIES)).getValue().forEach((String t, Tag u) -> {
					String pname = t;
					String pval = ((StringTag) u).getValue();
					properties.add(new Property(pname, pval));
				});
				
			}
			getPaletteIndex(getMinecraftMaterialState(flattenedLookup, flattenedId, new StateProperties(properties)));
		}
	}
	
	private MinecraftMaterialState getMinecraftMaterialState(FlattenedLookup flattenedLookup, String flattenedId, StateProperties properties) {
		MinecraftMaterialState materialState = flattenedLookup.getMaterialState(flattenedId, properties);
		
		if (materialState == null) {
			MinecraftMaterialBuilder materialBuilder = new MinecraftMaterialBuilder(flattenedId, "Unknown " + flattenedId);
			materialState = materialBuilder.new MinecraftMaterialStateBuilder(true).addFlattenedAnvilData(anvilVersion.getVersionName(), new AnvilDataFlattened(flattenedId, properties, null, null, null)).properties(properties).createMaterialState();
			
			Map<String, List<String>> props = new HashMap<>();
			for (Property prop: properties.asList()) {
				ArrayList<String> values = new ArrayList<>();
				values.add(prop.value());
				props.put(prop.name(), values);
			}
			materialBuilder.properties(props);
			
			materialBuilder.createMaterial();
		}
		
		return materialState;
	}
	
	public int getPaletteIndex(MinecraftMaterialState materialState) {
		if (!paletteMap.containsKey(materialState)) {
			paletteMap.put(materialState, paletteList.size());
			paletteList.add(materialState);
			CompoundTag materialPaletteEntry = new CompoundTag("");
			materialPaletteEntry.put(new StringTag(TAG_NAME, materialState.getFlattenedAnvilId(anvilVersion.getVersionName())));
		}
		return paletteMap.get(materialState);
	}
	
	public MinecraftMaterialState getMaterialState(int paletteIndex) {
		return paletteList.get(paletteIndex);
	}
	
	public int getPaletteSize() {
		return paletteList.size();
	}
	
	public ListTag toNbt(String tagName) {
		ListTag paletteListTag = new ListTag(tagName);
		
		for (MinecraftMaterialState m: paletteList) {
			CompoundTag paletteEntry = new CompoundTag("");
			paletteEntry.put(new StringTag(TAG_NAME, m.getFlattenedAnvilId(anvilVersion.getVersionName())));
			
			if (m.getProperties() != null && !m.getProperties().isEmpty()) {
				CompoundTag properties = new CompoundTag(TAG_PROPERTIES);
				for (Property p: m.getProperties().asList()) {
					properties.put(new StringTag(p.name(), p.value()));
				}
				paletteEntry.put(properties);
			}
			
			paletteListTag.add(paletteEntry);
		}
		
		return paletteListTag;
	}
}
