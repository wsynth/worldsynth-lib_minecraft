/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.chunk;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;

import com.github.steveice10.opennbt.tag.builtin.ByteArrayTag;
import com.github.steveice10.opennbt.tag.builtin.ByteTag;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.IntArrayTag;
import com.github.steveice10.opennbt.tag.builtin.IntTag;
import com.github.steveice10.opennbt.tag.builtin.ListTag;
import com.github.steveice10.opennbt.tag.builtin.LongArrayTag;
import com.github.steveice10.opennbt.tag.builtin.LongTag;
import com.github.steveice10.opennbt.tag.builtin.ShortTag;
import com.github.steveice10.opennbt.tag.builtin.StringTag;

import net.worldsynth.addon.minecraft.biome.MinecraftBiome;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialState;
import net.worldsynth.addon.minecraft.minecraft.anvil.ChunkStatus113;
import net.worldsynth.addon.minecraft.minecraft.anvil.util.BitArrayUtil;

public class ChunkWriter113 extends AnvilChunkWriter<AnvilChunk113> {

	protected final CompoundTag chunkCompund;
	
	public ChunkWriter113(AnvilChunk113 chunk) {
		super(chunk);
		
		chunkCompund = new CompoundTag(ChunkLoader113.TAG_CHUNK);
		chunkCompund.put(new IntTag(ChunkLoader113.TAG_DATAVERSION, anvilVersion.getDataVersion()));
		
		chunkCompund.put(levelCompound());
	}
	
	@Override
	public CompoundTag getChunkCompund() {
		return chunkCompund;
	}
	
	protected CompoundTag levelCompound() {
		CompoundTag levelCompound = new CompoundTag(ChunkLoader113.TAG_LEVEL);
		levelCompound.put(new IntTag(ChunkLoader113.TAG_XPOS, chunk.getChunkX()));
		levelCompound.put(new IntTag(ChunkLoader113.TAG_ZPOS, chunk.getChunkZ()));
		
		levelCompound.put(new LongTag(ChunkLoader113.TAG_INHABITEDTIME, chunk.getInhabitedTime()));
		levelCompound.put(new LongTag(ChunkLoader113.TAG_LASTUPDATE, chunk.getLastUpdate()));
		
		levelCompound.put(new ListTag(ChunkLoader113.TAG_ENTITIES, chunk.getEntitiesList()));
		
		levelCompound.put(createTileEntitiesListTag(ChunkLoader113.TAG_TILEENTITIES));
		levelCompound.put(createTileTicksListTag(ChunkLoader113.TAG_TILETICKS));
		levelCompound.put(createLiquidTicksListTag(ChunkLoader113.TAG_LIQUIDTICKS));

		levelCompound.put(new StringTag(ChunkLoader113.TAG_STATUS, chunk.getChunkStatus().getName()));
		
		ChunkStatus113 chunkStatus = (ChunkStatus113) chunk.getChunkStatus();
		List<List<Short>> chunkLightsLists = chunkStatus.compareTo(ChunkStatus113.FULLCHUNK) >= 0 ? null : new ArrayList<List<Short>>();
		levelCompound.put(chunkSectionsList(chunkLightsLists));
		
		if (chunkLightsLists != null) {
			ListTag chunkLightsListsTag = new ListTag(ChunkLoader113.TAG_LIGHTS);
			for (List<Short> sectionLightsList: chunkLightsLists) {
				ListTag sectionLightsListTag = new ListTag("");
				for (short s: sectionLightsList) {
					sectionLightsListTag.add(new ShortTag("", s));
				}
				chunkLightsListsTag.add(sectionLightsListTag);
			}
			levelCompound.put(chunkLightsListsTag);
		}
		
		levelCompound.put(biomesArray());
//		levelCompound.put(heightmaps());
		
		return levelCompound;
	}

	protected ListTag chunkSectionsList(List<List<Short>> chunkLightsLists) {
		ListTag sectionsListTag = new ListTag(ChunkLoader113.TAG_SECTIONS);
		for (AnvilChunkSection112 section: chunk.getChunkSections()) {
			List<Short> sectionLightsList = chunkLightsLists == null ? null : new ArrayList<Short>();
			sectionsListTag.add(chunkSectionToNbt(section, sectionLightsList));
			
			if (sectionLightsList != null) {
				chunkLightsLists.add(sectionLightsList);
			}
		}
		
		return sectionsListTag;
	}
	
	protected CompoundTag chunkSectionToNbt(AnvilChunkSection112 chunkSection, List<Short> sectionLightsList) {
		CompoundTag sectionCompoundTag = new CompoundTag(ChunkLoader113.TAG_SECTION);
		
		sectionCompoundTag.put(new ByteTag(ChunkLoader113.TAG_Y, (byte) chunkSection.getSectionY()));
		sectionCompoundTag.put(new ByteArrayTag(ChunkLoader113.TAG_BLOCKLIGHT, chunkSection.getBlockLightsArray()));
		sectionCompoundTag.put(new ByteArrayTag(ChunkLoader113.TAG_SKYLIGHT, chunkSection.getSkyLightsArray()));
		
		MinecraftMaterialState[] materials = chunkSection.getMaterialsArray();
		MaterialPalette palette = new MaterialPalette(anvilVersion);
		int[] blockStates = new int[4096];
		
		for (int i = 0; i < 4096; i++) {
			blockStates[i] = palette.getPaletteIndex(materials[i]);
		}
		int bitsPerBlockstateEntry = Math.max((int) Math.ceil(Math.log(palette.getPaletteSize()) / Math.log(2)), 4);
		sectionCompoundTag.put(new LongArrayTag(ChunkLoader113.TAG_BLOCKSTATES, BitArrayUtil.toLongBitArray(bitsPerBlockstateEntry, blockStates)));
		
		sectionCompoundTag.put(palette.toNbt(ChunkLoader113.TAG_PALETTE));
		
		// Fill in list of light sources if needed
		if (sectionLightsList != null) {
			boolean hasLightSources = false;
			for (int i = 0; i < palette.getPaletteSize(); i++) {
				if (palette.getMaterialState(i).getLightLevel() > 0) {
					hasLightSources = true;
					break;
				}
			}
			
			if (hasLightSources) {
				for (short i = 0; i < 4096; i++) {
					if (chunkSection.getMaterialsArray()[i].getLightLevel() > 0) {
						// Block array index is YZX packed, light index is ZYX packed, translate that shit.
						int x = i & 0xF;
						int y = i >>> 8 & 0xF;
						int z = i >>> 4 & 0xF;
						short zyxPacked = (short) (z << 8 | y << 4 | x);
						sectionLightsList.add(zyxPacked);
					}
				}
			}
		}
		
		return sectionCompoundTag;
	}

	protected IntArrayTag biomesArray() {
		MinecraftBiome[] biomes = chunk.getBiomesArray();
		int[] biomesArray = new int[biomes.length];
		for (int i = 0; i < biomes.length; i++) {
			biomesArray[i] = cachedGetNumericBiomeId(biomes[i]);
		}
		
		return new IntArrayTag(ChunkLoader113.TAG_BIOMES, biomesArray);
	}
	
	private IdentityHashMap<MinecraftBiome, Integer> biomeIdCache = new IdentityHashMap<>();
	protected final int cachedGetNumericBiomeId(MinecraftBiome biome) {
		if (!biomeIdCache.containsKey(biome)) {
			biomeIdCache.put(biome, biome.getNumericAnvilId(anvilVersion.getVersionName()));
		}
		return biomeIdCache.get(biome);
	}

//	protected CompoundTag heightmaps() {
//		CompoundTag heightmapsTag = new CompoundTag(ChunkLoader113.TAG_HEIGHTMAPS);
//		heightmapsTag.put(new LongArrayTag(ChunkLoader113.TAG_WORLDSURFACE, BitArrayUtil.toLongBitArray(9, createHeightmap(chunk, PREDICATE_WORLD_SURFACE))));
//		
//		return heightmapsTag;
//	}
}
