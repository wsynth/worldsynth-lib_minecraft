/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.chunk;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;

import com.github.steveice10.opennbt.NBTIO;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;

import net.worldsynth.addon.minecraft.minecraft.Coordinate;
import net.worldsynth.addon.minecraft.minecraft.IChunkStorage;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilVersion;
import net.worldsynth.addon.minecraft.minecraft.anvil.RegionFile;

public class AnvilChunkStorage implements IChunkStorage<SectionedAnvilChunk<?>> {
	
	public static final String TAG_DATAVERSION = "DataVersion";
	
	private final File regionDir;
	private final HashMap<Coordinate, RegionFile> regions = new HashMap<>();
	
	public AnvilChunkStorage(File regionDir) {
		this.regionDir = regionDir;
		if (!regionDir.exists()) {
			regionDir.mkdirs();
		}
	}

	/**
	 * Gets the specified region file within the region directory. If the region
	 * file does not exist, the region file will either be made, or the method
	 * returns {@code null} depending on the {@code createIfNotExists} parameter.
	 * 
	 * @param regionX
	 * @param regionZ
	 * @param createIfNotExists Non existing region file will be created if this is {@code true}
	 * @return The region file, or {@code null} if the region file doesn't exist and wasn't created
	 * @throws IOException
	 */
	private RegionFile getRegion(int regionX, int regionZ, boolean createIfNotExists) throws IOException {
		Coordinate regionCoord = new Coordinate(regionX, regionZ);
		RegionFile region;
		if ((region = regions.get(regionCoord)) != null) {
			return region;
		}
		
		File regFile = new File(regionDir, "r." + regionX + "." + regionZ + RegionFile.ANVIL_EXTENSION);
		if (regFile.exists()) {
			region = new RegionFile(regionDir, regionX, regionZ);
			regions.put(regionCoord, region);
		}
		else if (createIfNotExists) {
			regFile.createNewFile();
			region = new RegionFile(regionDir, regionX, regionZ);
			regions.put(regionCoord, region);
		}
		
		return region;
	}

	@Override
	public boolean chunkExists(int chunkX, int chunkZ) throws IOException {
		RegionFile regionFile = getRegion(chunkX >> 5, chunkZ >> 5, false);
		if (regionFile == null) {
			return false;
		}
		
		return regionFile.hasChunk(chunkX & 31, chunkZ & 31);
	}

	@Override
	public SectionedAnvilChunk<?> readChunk(int chunkX, int chunkZ) throws IOException {
		RegionFile regionFile = getRegion(chunkX >> 5, chunkZ >> 5, false);
		if (regionFile == null) return null;
		
		InputStream chunkDataInputStream = regionFile.getChunkDataInputStream(chunkX & 31, chunkZ & 31);
		if (chunkDataInputStream == null) return null;
		CompoundTag chunkCompoundTag = (CompoundTag) NBTIO.readTag(chunkDataInputStream);
		chunkDataInputStream.close();
		
		int dataVersion = (int) chunkCompoundTag.get(TAG_DATAVERSION).getValue();
		AnvilChunkLoader<?> chunkLoader = AnvilVersion.getVersionByDataVersion(dataVersion).getNewChunkLoader();
		if (chunkLoader == null) return null;
		
		return chunkLoader.chunkFromNbt(chunkCompoundTag);
	}

	@Override
	public void writeChunk(SectionedAnvilChunk<?> chunk) throws IOException {
		int chunkX = chunk.getChunkX();
		int chunkZ = chunk.getChunkZ();
		
		@SuppressWarnings("unchecked")
		CompoundTag chunkData = chunk.getAnvilVersion().getNewChunkLoader().chunkToNbt(chunk);
		
		RegionFile regionFile = getRegion(chunkX >> 5, chunkZ >> 5, true);
		OutputStream chunkDataOutputStream = regionFile.getChunkDataOutputStream(chunkX & 31, chunkZ & 31);
		NBTIO.writeTag(chunkDataOutputStream, chunkData);
		chunkDataOutputStream.close();
	}

	@Override
	public void close() throws Exception {
		for (RegionFile r: regions.values()) {
			try {
				r.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		regions.clear();
	}
}
