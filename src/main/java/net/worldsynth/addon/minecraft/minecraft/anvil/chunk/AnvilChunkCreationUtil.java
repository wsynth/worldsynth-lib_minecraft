/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.chunk;

import net.worldsynth.addon.minecraft.biome.MinecraftBiome;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialState;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilVersion;
import net.worldsynth.addon.minecraft.minecraft.anvil.IChunkStatus;

/**
 * Utility class for constructing anvil chunks from data generated in WorldSynth
 */
public class AnvilChunkCreationUtil {
	
	public static SectionedAnvilChunk<?> createAnvilChunk(int chunkX, int chunkZ, int minSectionY, AnvilVersion anvilVersion, IChunkStatus chunkStatus, MinecraftMaterialState[][][] materials, MinecraftBiome[][] biomes) {
		SectionedAnvilChunk<?> chunk;
		
		if (anvilVersion.compareTo(AnvilVersion.VERSION_1_13) < 0) {
			chunk = new AnvilChunk112(chunkX, chunkZ, anvilVersion);
		}
		else if (anvilVersion.compareTo(AnvilVersion.VERSION_1_15) < 0) {
			chunk = new AnvilChunk113(chunkX, chunkZ, anvilVersion);
		}
		else if (anvilVersion.compareTo(AnvilVersion.VERSION_1_18) < 0) {
			chunk = new AnvilChunk115(chunkX, chunkZ, anvilVersion);
		}
		else {
			chunk = new AnvilChunk118(chunkX, chunkZ, anvilVersion);
		}
		
		chunk.setChunkStatus(chunkStatus);
		setMaterials(chunk, materials);
		setBiomes(chunk, biomes);
		
		return chunk;
	}
	
	public static SectionedAnvilChunk<?> createAnvilChunk(int chunkX, int chunkZ, int minSectionY, AnvilVersion anvilVersion, IChunkStatus chunkStatus, MinecraftMaterialState[][][] materials, MinecraftBiome[][][] biomes) {
		SectionedAnvilChunk<?> chunk;
		
		if (anvilVersion.compareTo(AnvilVersion.VERSION_1_13) < 0) {
			chunk = new AnvilChunk112(chunkX, chunkZ, anvilVersion);
		}
		else if (anvilVersion.compareTo(AnvilVersion.VERSION_1_15) < 0) {
			chunk = new AnvilChunk113(chunkX, chunkZ, anvilVersion);
		}
		else if (anvilVersion.compareTo(AnvilVersion.VERSION_1_18) < 0) {
			chunk = new AnvilChunk115(chunkX, chunkZ, anvilVersion);
		}
		else {
			chunk = new AnvilChunk118(chunkX, chunkZ, anvilVersion);
		}
		
		chunk.setChunkStatus(chunkStatus);
		setMaterials(chunk, materials);
		setBiomes(chunk, biomes);
		
		return chunk;
	}
	
	private static void setMaterials(SectionedAnvilChunk<?> chunk, MinecraftMaterialState[][][] materials) {
		int minY = chunk.getMinY();
		for (int y = chunk.getMinY(); y <= chunk.getMaxY(); y++) {
			for (int x = 0; x < 16; x++) {
				for (int z = 0; z < 16; z++) {
					chunk.setMaterial(x, y, z, materials[x][y-minY][z]);
				}
			}
		}
	}
	
	private static void setBiomes(SectionedAnvilChunk<?> chunk, MinecraftBiome[][] biomes) {
		for (int x = 0; x < 16; x++) {
			for (int z = 0; z < 16; z++) {
				chunk.setBiome(x, z, biomes[x][z]);
			}
		}
	}
	
	private static void setBiomes(SectionedAnvilChunk<?> chunk, MinecraftBiome[][][] biomes) {
		int minY = chunk.getMinY();
		for (int y = chunk.getMinY(); y <= chunk.getMaxY(); y+=4) {
			for (int x = 0; x < 16; x+=4) {
				for (int z = 0; z < 16; z+=4) {
					chunk.setBiome(x, y, z, biomes[x][y-minY][z]);
				}
			}
		}
	}
}
