/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil;

public enum ChunkStatus120 implements IChunkStatus {
	EMPTY("minecraft:empty"),
	STRUCTURE_STARTS("minecraft:structure_starts"),
	STRUCTURE_REFERENCES("minecraft:structure_references"),
	BIOMES("minecraft:biomes"),
	NOISE("minecraft:noise"),
	SURFACE("minecraft:surface"),
	CARVERS("minecraft:carvers"),
	FEATURES("minecraft:features"),
	INITIALIZE_LIGHT("minecraft:initialize_light"),
	LIGHT("minecraft:light"),
	SPAWN("minecraft:spawn"),
	FULL("minecraft:full");

	private final String name;

	private ChunkStatus120(String name) {
		this.name = name;
	}
	
	@Override
	public String getName() {
		return name;
	}
}
