/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil;

public enum ChunkDecoration112 implements IChunkStatus {
	UNDECORATED("undecorated", false),
	DECORATED("decorated", true);
	
	private final String name;
	private final boolean decorated;

	private ChunkDecoration112(String name, boolean decorated) {
		this.name = name;
		this.decorated = decorated;
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	public boolean getDecorated() {
		return decorated;
	}
}
