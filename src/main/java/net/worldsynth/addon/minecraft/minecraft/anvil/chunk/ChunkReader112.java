/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.chunk;

import java.util.ArrayList;

import com.github.steveice10.opennbt.tag.builtin.ByteArrayTag;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.ListTag;
import com.github.steveice10.opennbt.tag.builtin.Tag;

import net.worldsynth.addon.minecraft.biome.MinecraftBiome;
import net.worldsynth.addon.minecraft.biome.MinecraftBiomeProfile;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialBuilder;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialProfile;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialProfile.NumericLookup;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialState;
import net.worldsynth.addon.minecraft.material.anvildata.AnvilDataNumeric;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilVersion;
import net.worldsynth.biome.BiomeRegistry;
import net.worldsynth.material.MaterialRegistry;

public class ChunkReader112 extends AnvilChunkReader<AnvilChunk112> {

	protected final AnvilChunk112 chunk;
	protected final AnvilVersion anvilVersion;
	
	public ChunkReader112(CompoundTag chunkTag) {
		int dataVersion = (int) chunkTag.get(ChunkLoader112.TAG_DATAVERSION).getValue();
		anvilVersion = AnvilVersion.getVersionByDataVersion(dataVersion);
		
		CompoundTag levelCompound = (CompoundTag) chunkTag.get(ChunkLoader112.TAG_LEVEL);
		int posX = (int) levelCompound.get(ChunkLoader112.TAG_XPOS).getValue();
		int posZ = (int) levelCompound.get(ChunkLoader112.TAG_ZPOS).getValue();
		
		chunk = new AnvilChunk112(posX, posZ, anvilVersion);
		
		// Read in chunk materials and biomes
		readMaterialsAndBiomes(levelCompound);
		
		// Read in entities
		ListTag entities = levelCompound.get(ChunkLoader112.TAG_ENTITIES);
		if (entities != null) {
			chunk.setEntitiesList(entities.getValue());
		}
		else {
			chunk.setEntitiesList(new ArrayList<Tag>());
		}
		
		// Read in other chunk information
		chunk.setLastUpdate((long) levelCompound.get(ChunkLoader112.TAG_LASTUPDATE).getValue());
		chunk.setInhabitedTime((long) levelCompound.get(ChunkLoader112.TAG_INHABITEDTIME).getValue());
	}
	
	@Override
	public AnvilChunk112 getChunk() {
		return chunk;
	}
	
	protected void readMaterialsAndBiomes(CompoundTag levelCompound) {
		// Read in chunk sections
		ListTag chunkSections = levelCompound.get(ChunkLoader112.TAG_SECTIONS);
		if (chunkSections.size() > 0) {
			chunk.setChunkSections(getChunkSections(chunkSections));
			
			// Read tile entities and mutate chunk materials
			ListTag tileEntities = levelCompound.get(ChunkLoader112.TAG_TILEENTITIES);
			if (tileEntities != null) {
				mutateMaterialTileEntities(tileEntities);
			}
			
			// Read tile ticks and mutate chunk materials
			ListTag tileTicks = levelCompound.get(ChunkLoader112.TAG_TILETICKS);
			if (tileTicks != null) {
				mutateMaterialsTileTicks(tileTicks);
			}
		}
		else {
			// There are no chunk sections, do not read materials
			chunk.setChunkSections(new AnvilChunkSection112[0]);
		}
		
		// Read in biomes
		Tag biomes = levelCompound.get(ChunkLoader112.TAG_BIOMES);
		if (biomes != null) {
			chunk.setBiomesArray(getBiomes(biomes));
		}
		else {
			// There is no biome data, do not read biomes
			chunk.setBiomesArray(new MinecraftBiome[256]);
		}
	}

	protected AnvilChunkSection112[] getChunkSections(Tag sectionsTag) {
		ListTag sectionsListTag = (ListTag) sectionsTag;
		int sectionsCount = sectionsListTag.size();
		AnvilChunkSection112[] chunkSections = new AnvilChunkSection112[sectionsCount];
		for (int i = 0; i < sectionsCount; i++) {
			chunkSections[i] = chunkSectionFromNbt((CompoundTag) sectionsListTag.get(i), anvilVersion);
		}
		return chunkSections;
	}
	
	protected AnvilChunkSection112 chunkSectionFromNbt(CompoundTag chunkSectionTag, AnvilVersion anvilVersion) {
		byte sectionY = (byte) chunkSectionTag.get(ChunkLoader112.TAG_Y).getValue();
		AnvilChunkSection112 chunkSection = new AnvilChunkSection112(sectionY);
		
		 boolean useAdd = chunkSectionTag.contains(ChunkLoader112.TAG_ADD);
		
		byte[] serializedBlocks = ((ByteArrayTag) chunkSectionTag.get(ChunkLoader112.TAG_BLOCKS)).getValue();
		byte[] serializedAdd = new byte[2048];
		if (useAdd) {
			serializedAdd = ((ByteArrayTag) chunkSectionTag.get(ChunkLoader112.TAG_ADD)).getValue();
		}
		byte[] serializedData = ((ByteArrayTag) chunkSectionTag.get(ChunkLoader112.TAG_DATA)).getValue();
		byte[] serializedBlockLight = ((ByteArrayTag) chunkSectionTag.get(ChunkLoader112.TAG_BLOCKLIGHT)).getValue();
		byte[] serializedSkyLight = ((ByteArrayTag) chunkSectionTag.get(ChunkLoader112.TAG_SKYLIGHT)).getValue();
		
		MinecraftMaterialProfile minecratMaterialProfile = (MinecraftMaterialProfile) MaterialRegistry.getProfile("minecraft");
		NumericLookup numericLookup = minecratMaterialProfile.getNumericLookup(anvilVersion.getVersionName());
		
		//Convert
		for (int x = 0; x < 16; x++) {
			for (int y = 0; y < 16; y++) {
				for (int z = 0; z < 16; z++) {
					int index = y*256 + z*16 + x;
					int dataIndex = index / 2;
					int dataSubIndex = index % 2;
					
					//Block material
					byte block = serializedBlocks[index];
					byte add = serializedAdd[dataIndex];
					byte data = serializedData[dataIndex];
					
					int blockId = block & 0xFF;
					byte blockData = (byte) (data & 0x0F);
					
					if (useAdd) {
						if (dataSubIndex == 0) {
							int idAdd = add & 0x0F;
							blockId = blockId | (idAdd << 8);
						}
						else {
							int idAdd = add & 0xF0;
							blockId = blockId | (idAdd << 4);
						}
					}
					
					if (dataSubIndex != 0) {
						data = (byte) ((data & 0xF0) >> 4);
						blockData = data;
					}
					
					MinecraftMaterialState material = numericLookup.getMaterialState(blockId, blockData);
					if (material == null) {
						MinecraftMaterialBuilder materialBuilder = new MinecraftMaterialBuilder(blockId + ":" + blockData, "Unknown " + blockId + ":" + blockData);
						material = materialBuilder.new MinecraftMaterialStateBuilder(true).addNumericAnvilData(anvilVersion.getVersionName(), new AnvilDataNumeric(blockId, blockData, null, null, null)).createMaterialState();
						materialBuilder.createMaterial();
					}
					chunkSection.setMaterial(x, y, z, material);
					
					//Blocklight and skylight
					byte bLight = serializedBlockLight[dataIndex];
					byte sLight = serializedSkyLight[dataIndex];
					
					if (dataSubIndex == 0) {
						bLight = (byte) (bLight & 0x0F);
						sLight = (byte) (sLight & 0x0F);
						chunkSection.setBlockLight(x, y, z, bLight);
						chunkSection.setSkyLight(x, y, z, sLight);
					}
					else {
						bLight = (byte) ((bLight & 0xF0) >> 4);
						sLight = (byte) ((sLight & 0xF0) >> 4);
						chunkSection.setBlockLight(x, y, z, bLight);
						chunkSection.setSkyLight(x, y, z, sLight);
					}
				}
			}
		}
		
		return chunkSection;
	}

	protected MinecraftBiome[] getBiomes(Tag biomesTag) {
		byte[] biomes = (byte[]) biomesTag.getValue();
		MinecraftBiomeProfile mcBiomeProfile = (MinecraftBiomeProfile) BiomeRegistry.getProfile("minecraft");
		MinecraftBiome[] biomesArray = new MinecraftBiome[256];
		for (int i = 0; i < 256; i++) {
			biomesArray[i] = mcBiomeProfile.getNumericLookup(anvilVersion.getVersionName()).getBiome(biomes[i] & 0xFF);
		}
		return biomesArray;
	}
}
