/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.chunk;

import net.worldsynth.addon.minecraft.biome.MinecraftBiome;
import net.worldsynth.addon.minecraft.material.MinecraftMaterial;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialState;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilVersion;

public class AnvilChunk118 extends SectionedAnvilChunk<AnvilChunkSection118> {
	
	private static final MinecraftMaterialState AIR = MinecraftMaterial.AIR.getDefaultState();
	private static final MinecraftMaterialState BEDROCK = MinecraftMaterial.BEDROCK.getDefaultState();
	
	public AnvilChunk118(int chunkX, int chunkZ, AnvilVersion anvilVersion) {
		super(chunkX, chunkZ, anvilVersion);
	}
	
	@Override
	public int minSectionY() {
		return -4;
	}
	
	@Override
	public
	int maxSectionY() {
		return 19;
	}
	
	@Override
	protected AnvilChunkSection118 constructSection(byte sectionY) {
		return new AnvilChunkSection118(sectionY);
	}
	
	@Override
	public MinecraftMaterialState getMaterial(int x, int y, int z) {
		int sectionY = y >> 4;
		if (sectionY < minSectionY()) {
			return BEDROCK;
		}
		else if (sectionY > maxSectionY()) {
			return AIR;
		}
		else if (sectionExists(sectionY)) {
			return getSection(sectionY).getMaterial(x, y & 15, z);
		}
		return AIR;
	}

	@Override
	public void setMaterial(int x, int y, int z, MinecraftMaterialState material) {
		int sectionY = y >> 4;
		if (sectionY < minSectionY() || sectionY > maxSectionY()) return;
		getSection(sectionY).setMaterial(x, y & 15, z, material);
	}

	@Override
	public MinecraftBiome getBiome(int x, int z) {
		// Biomes in chunk are 3D
		// Pick biome from the top layer
		return getBiome(x, getMaxY(), z);
	}
	
	@Override
	public void setBiome(int x, int z, MinecraftBiome biome) {
		// Biomes in chunk are 3D
		// Extrude biome in y
		for (int y = getMinY(); y < getMaxY(); y+=4) {
			setBiome(x, y, z, biome);
		}
	}
	
	@Override
	public MinecraftBiome getBiome(int x, int y, int z) {
		int sectionY = y >> 4;
		if (sectionY < minSectionY() || sectionY > maxSectionY()) return null;
		return getSection(sectionY).getBiome(x, y & 15, z);
	}
	
	@Override
	public void setBiome(int x, int y, int z, MinecraftBiome biome) {
		int chunkSection = y >> 4;
		if (chunkSection < minSectionY() || chunkSection > maxSectionY()) return;
		getSection(chunkSection).setBiome(x, y & 15, z, biome);
	}
	
	@Override
	public byte getBlockLight(int x, int y, int z) {
		int chunkSection = (y >> 4) - minSectionY();
		if (chunkSection < 0) {
			return 0;
		}
		else if (chunkSection < sectionsCount()) {
			return getSectionByIndex(chunkSection).getBlockLight(x, y & 15, z);
		}
		return 15;
	}
	
	@Override
	public void setBlockLight(int x, int y, int z, byte blockLight) {
		int chunkSection = (y >> 4) - minSectionY();
		if (chunkSection < 0 || chunkSection >= sectionsCount()) return;
		getSectionByIndex(chunkSection).setBlockLight(x, y & 15, z, blockLight);
	}

	@Override
	public byte getSkyLight(int x, int y, int z) {
		int chunkSection = (y >> 4) - minSectionY();
		if (chunkSection < 0) {
			return 0;
		}
		else if (chunkSection < sectionsCount()) {
			return getSectionByIndex(chunkSection).getSkyLight(x, y & 15, z);
		}
		return 15;
	}

	@Override
	public void setSkyLight(int x, int y, int z, byte skyLight) {
		int chunkSection = (y >> 4) - minSectionY();
		if (chunkSection < 0 || chunkSection >= sectionsCount()) return;
		getSectionByIndex(chunkSection).setSkyLight(x, y & 15, z, skyLight);
	}
}
