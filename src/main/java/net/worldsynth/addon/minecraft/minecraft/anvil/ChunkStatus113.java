/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil;

public enum ChunkStatus113 implements IChunkStatus {
	EMPTY("empty"),
	BASE("base"),
	CARVED("carved"),
	LIQUID_CARVED("liquid_carved"),
	DECORATED("decorated"),
	LIGHTED("lighted"),
	MOBS_SPAWNED("mobs_spawned"),
	FINALIZED("finalized"),
	FULLCHUNK("fullchunk"),
	POSTPROCESSED("postprocessed");

	private final String name;

	private ChunkStatus113(String name) {
		this.name = name;
	}
	
	@Override
	public String getName() {
		return name;
	}
}
