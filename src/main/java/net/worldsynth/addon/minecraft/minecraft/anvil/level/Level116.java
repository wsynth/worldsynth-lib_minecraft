/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.level;

import java.io.IOException;

import com.github.steveice10.opennbt.tag.builtin.ByteTag;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.LongTag;
import com.github.steveice10.opennbt.tag.builtin.StringTag;
import com.github.steveice10.opennbt.tag.builtin.Tag;

import net.worldsynth.addon.minecraft.minecraft.Difficulty;
import net.worldsynth.addon.minecraft.minecraft.GameMode;
import net.worldsynth.addon.minecraft.minecraft.Generator;
import net.worldsynth.addon.minecraft.minecraft.WorldType;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilVersion;

public class Level116 extends Level {
	
	protected static final String TAG_WORLD_GEN_SETTINGS_COMPOUND   = "WorldGenSettings";
	protected static final String TAG_DIMENSIONS_COMPOUND           = "dimensions";
	protected static final String TAG_GENERATOR_COMPOUND            = "generator";
	protected static final String TAG_BIOME_SOURCE_COMPOUND         = "biome_source";
	
	protected static final String TAG_SEED                          = "seed";
	protected static final String TAG_TYPE                          = "type";
	protected static final String TAG_LARGE_BIOMES                  = "large_biomes";
	
	
	public Level116(String levelName, AnvilVersion anvilVersion, Generator generator, long seed, GameMode gameMode, Difficulty difficulty, int spawnX, int spawnY, int spawnZ) throws IOException {
		super(levelName, anvilVersion, generator, seed, gameMode, difficulty, spawnX, spawnY, spawnZ);
	}
	
	public Level116(CompoundTag levelTag) {
		super(levelTag);
	}
	
	@Override
	protected String defaultLevelDataResource() {
		return "defaultlevel116.snbt";
	}

	@Override
	protected void putGeneratorTags(CompoundTag dataCompound, AnvilVersion anvilVersion, Generator generator, long seed) {
		CompoundTag worldGenSettingsCompound = dataCompound.get(TAG_WORLD_GEN_SETTINGS_COMPOUND);
		worldGenSettingsCompound.put(new LongTag(TAG_SEED, seed));
		
		CompoundTag dimensionsCompound = worldGenSettingsCompound.get(TAG_DIMENSIONS_COMPOUND);
		for (Tag dimensionTag: dimensionsCompound.values()) {
			CompoundTag dimensionCompound = (CompoundTag) dimensionTag;
			CompoundTag generatorCompound = dimensionCompound.get(TAG_GENERATOR_COMPOUND);
			CompoundTag biomeSourceCompound = generatorCompound.get(TAG_BIOME_SOURCE_COMPOUND);
			String dimensionType = (String) dimensionCompound.get(TAG_TYPE).getValue();
			
			if (dimensionType.equals("minecraft:overworld")) {
				if (generator.getWorldType() == WorldType.LARGEBIOMES) {
					biomeSourceCompound.put(new ByteTag(TAG_LARGE_BIOMES, (byte) 1));
				}
				else if (generator.getWorldType() == WorldType.AMPLIFIED) {
					generatorCompound.put(new StringTag("settings", "minecraft:amplified"));
				}
				else if (generator.getWorldType().getName() == "flat") {
					dimensionCompound.put(generator.getSuperflatOptions().generatorCompoundTag116(anvilVersion, seed));
					continue;
				}
			}
			
			generatorCompound.put(new LongTag(TAG_SEED, seed));
			biomeSourceCompound.put(new LongTag(TAG_SEED, seed));
		}
	}
}
