/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.chunk;

import java.util.IdentityHashMap;

import com.github.steveice10.opennbt.tag.builtin.ByteArrayTag;
import com.github.steveice10.opennbt.tag.builtin.ByteTag;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.IntArrayTag;
import com.github.steveice10.opennbt.tag.builtin.IntTag;
import com.github.steveice10.opennbt.tag.builtin.ListTag;
import com.github.steveice10.opennbt.tag.builtin.LongTag;

import net.worldsynth.addon.minecraft.biome.MinecraftBiome;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialState;
import net.worldsynth.addon.minecraft.material.anvildata.AnvilDataNumeric;
import net.worldsynth.addon.minecraft.minecraft.anvil.ChunkDecoration112;

public class ChunkWriter112 extends AnvilChunkWriter<AnvilChunk112> {
	
	protected final CompoundTag chunkCompund;
	
	public ChunkWriter112(AnvilChunk112 chunk) {
		super(chunk);
		
		chunkCompund = new CompoundTag(ChunkLoader112.TAG_CHUNK);
		chunkCompund.put(new IntTag(ChunkLoader112.TAG_DATAVERSION, anvilVersion.getDataVersion()));
		
		chunkCompund.put(levelCompound());
	}
	
	@Override
	public CompoundTag getChunkCompund() {
		return chunkCompund;
	}
	
	protected CompoundTag levelCompound() {
		CompoundTag levelCompound = new CompoundTag(ChunkLoader112.TAG_LEVEL);
		levelCompound.put(new IntTag(ChunkLoader112.TAG_XPOS, chunk.getChunkX()));
		levelCompound.put(new IntTag(ChunkLoader112.TAG_ZPOS, chunk.getChunkZ()));
		
		levelCompound.put(new LongTag(ChunkLoader112.TAG_INHABITEDTIME, chunk.getInhabitedTime()));
		levelCompound.put(new LongTag(ChunkLoader112.TAG_LASTUPDATE, chunk.getLastUpdate()));
		
		levelCompound.put(new ListTag(ChunkLoader112.TAG_ENTITIES, chunk.getEntitiesList()));
		
		levelCompound.put(createTileEntitiesListTag(ChunkLoader112.TAG_TILEENTITIES));
		ListTag tileTicks = createTileTicksListTag(ChunkLoader112.TAG_TILETICKS);
		if (tileTicks.size() > 0) {
			levelCompound.put(tileTicks);
		}
		
		levelCompound.put(new ByteTag(ChunkLoader112.TAG_LIGHTPOPULATED, chunk.isLightPopulated()));
		levelCompound.put(new ByteTag(ChunkLoader112.TAG_TERRAINPOPULATED, chunk.getChunkStatus() == ChunkDecoration112.DECORATED ? (byte) 1 : (byte) 0));
		
		levelCompound.put(chunkSectionsList());
		levelCompound.put(biomesArray());
		levelCompound.put(heightmap());
		
		return levelCompound;
	}

	protected ListTag chunkSectionsList() {
		ListTag sectionsListTag = new ListTag(ChunkLoader112.TAG_SECTIONS);
		for (AnvilChunkSection112 section: chunk.getChunkSections()) {
			sectionsListTag.add(chunkSectionToNbt(section));
		}
		
		return sectionsListTag;
	}
	
	protected CompoundTag chunkSectionToNbt(AnvilChunkSection112 chunkSection) {
		CompoundTag sectionCompoundTag = new CompoundTag(ChunkLoader112.TAG_SECTION);
		
		sectionCompoundTag.put(new ByteTag(ChunkLoader112.TAG_Y, (byte) chunkSection.getSectionY()));
		sectionCompoundTag.put(new ByteArrayTag(ChunkLoader112.TAG_BLOCKLIGHT, chunkSection.getBlockLightsArray()));
		sectionCompoundTag.put(new ByteArrayTag(ChunkLoader112.TAG_SKYLIGHT, chunkSection.getSkyLightsArray()));
		
		MinecraftMaterialState[] materials = chunkSection.getMaterialsArray();
		byte[] blocksArray = new byte[4096];
		byte[] addArray = new byte[2048];
		byte[] dataArray = new byte[2048];
		
		boolean useAdd = false;
		for (int i = 0; i < 2048; i++) {
			int numericAnvilId1 = cachedGetNumericMaterialData(materials[i*2]).getId();
			int numericAnvilId2 = cachedGetNumericMaterialData(materials[i*2+1]).getId();
			if (numericAnvilId1 >= 256 || numericAnvilId2 >= 256) {
				useAdd = true;
			}
			else {
				if (numericAnvilId1 < 0) numericAnvilId1 = 0;
				if (numericAnvilId2 < 0) numericAnvilId2 = 0;
			}
			
			blocksArray[i*2] = (byte)(numericAnvilId1 & 0xFF);
			blocksArray[i*2+1] = (byte)(numericAnvilId2 & 0xFF);
			
			addArray[i] = (byte) (((numericAnvilId1 >> 8) & 0x0F) | ((numericAnvilId2 >> 4) & 0xF0));
			dataArray[i] = (byte) ((cachedGetNumericMaterialData(materials[i*2]).getMeta() & 0x0F) | ((cachedGetNumericMaterialData(materials[i*2+1]).getMeta() & 0x0F) << 4));
		}
		
		sectionCompoundTag.put(new ByteArrayTag(ChunkLoader112.TAG_BLOCKS, blocksArray));
		sectionCompoundTag.put(new ByteArrayTag(ChunkLoader112.TAG_DATA, dataArray));
		if (useAdd) sectionCompoundTag.put(new ByteArrayTag(ChunkLoader112.TAG_ADD, addArray));
		
		return sectionCompoundTag;
	}
	
	private IdentityHashMap<MinecraftMaterialState, AnvilDataNumeric> materialDataCache = new IdentityHashMap<>();
	protected final AnvilDataNumeric cachedGetNumericMaterialData(MinecraftMaterialState state) {
		if (!materialDataCache.containsKey(state)) {
			materialDataCache.put(state, state.getNumericAnvilData(anvilVersion.getVersionName()));
		}
		return materialDataCache.get(state);
	}

	protected ByteArrayTag biomesArray() {
		MinecraftBiome[] biomes = chunk.getBiomesArray();
		byte[] biomesArray = new byte[biomes.length];
		
		for (int i = 0; i < biomes.length; i++) {
			biomesArray[i] = (byte) cachedGetNumericBiomeId(biomes[i]);
		}
		
		return new ByteArrayTag(ChunkLoader112.TAG_BIOMES, biomesArray);
	}
	
	private IdentityHashMap<MinecraftBiome, Integer> biomeIdCache = new IdentityHashMap<>();
	protected final int cachedGetNumericBiomeId(MinecraftBiome biome) {
		if (!biomeIdCache.containsKey(biome)) {
			biomeIdCache.put(biome, biome.getNumericAnvilId(anvilVersion.getVersionName()));
		}
		return biomeIdCache.get(biome);
	}
	
	protected IntArrayTag heightmap() {
		return new IntArrayTag(ChunkLoader112.TAG_HEIGHTMAP, createHeightmap(chunk, PREDICATE_WORLD_SURFACE));
	}
}
