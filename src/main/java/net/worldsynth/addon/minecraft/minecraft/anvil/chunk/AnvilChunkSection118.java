/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.chunk;

import net.worldsynth.addon.minecraft.biome.MinecraftBiome;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialState;

public class AnvilChunkSection118 extends AnvilChunkSection {

	private MinecraftMaterialState[] materialsArray = new MinecraftMaterialState[4096];
	private MinecraftBiome[] biomesArray = new MinecraftBiome[64];
	private byte[] blockLightsArray = new byte[2048];
	private byte[] skyLightsArray = new byte[2048];
	
	public AnvilChunkSection118(byte sectionY) {
		super(sectionY);
	}
	
	public MinecraftMaterialState[] getMaterialsArray() {
		return materialsArray;
	}
	
	public void setMaterialsArray(MinecraftMaterialState[] materialsArray) {
		this.materialsArray = materialsArray;
	}
	
	public MinecraftMaterialState getMaterial(int x, int y, int z) {
		return materialsArray[y << 8 | z << 4 | x];
	}
	
	public void setMaterial(int x, int y, int z, MinecraftMaterialState material) {
		materialsArray[y << 8 | z << 4 | x] = material;
	}
	
	public MinecraftBiome[] getBiomesArray() {
		return biomesArray;
	}
	
	public void setBiomesArray(MinecraftBiome[] biomesArray) {
		this.biomesArray = biomesArray;
	}
	
	public MinecraftBiome getBiome(int x, int y, int z) {
		return biomesArray[(y & ~3) << 2 | z & ~3 | x >> 2];
	}
	
	public void setBiome(int x, int y, int z, MinecraftBiome biome) {
		biomesArray[(y & ~3) << 2 | z & ~3 | x >> 2] = biome;
	}
	
	public byte[] getBlockLightsArray() {
		return blockLightsArray;
	}
	
	public void setBlockLightsArray(byte[] blockLightsArray) {
		this.blockLightsArray = blockLightsArray;
	}
	
	public byte getBlockLight(int x, int y, int z) {
		int index = y << 8 | z << 4 | x;
		if ((index & 1) == 0) {
			return (byte) ((blockLightsArray[index >> 1] >> 4) & 15);
		}
		else {
			return (byte) (blockLightsArray[index >> 1] & 15);
		}
	}
	
	public void setBlockLight(int x, int y, int z, byte value) {
		int index = y << 8 | z << 4 | x;
		if ((index & 1) == 0) {
			blockLightsArray[index >> 1] = (byte)(blockLightsArray[index >> 1] & 240 | value & 15);
		}
		else {
			blockLightsArray[index >> 1] = (byte)(blockLightsArray[index >> 1] & 15 | (value & 15) << 4);
		}
	}
	
	public byte[] getSkyLightsArray() {
		return skyLightsArray;
	}
	
	public void setSkyLightsArray(byte[] skyLightsArray) {
		this.skyLightsArray = skyLightsArray;
	}
	
	public byte getSkyLight(int x, int y, int z) {
		int index = y << 8 | z << 4 | x;
		if ((index & 1) == 0) {
			return (byte) ((skyLightsArray[index >> 1] >> 4) & 15);
		}
		else {
			return (byte) (skyLightsArray[index >> 1] & 15);
		}
	}
	
	public void setSkyLight(int x, int y, int z, byte value) {
		int index = y << 8 | z << 4 | x;
		if ((index & 1) == 0) {
			skyLightsArray[index >> 1] = (byte)(skyLightsArray[index >> 1] & 240 | value & 15);
		}
		else {
			skyLightsArray[index >> 1] = (byte)(skyLightsArray[index >> 1] & 15 | (value & 15) << 4);
		}
	}
}
