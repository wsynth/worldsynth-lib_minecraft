/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft;

public enum GameMode {
	SURVIVAL(0),
	CREATIVE(1),
	ADVENTURE(2),
	SPECTATOR(3);
	
	private final int gameModeId;
	
	private GameMode(int gameModeId) {
		this.gameModeId = gameModeId;
	}
	
	public int getGameModeId() {
		return gameModeId;
	}
}
