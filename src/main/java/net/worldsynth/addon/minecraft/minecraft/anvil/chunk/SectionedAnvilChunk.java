/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.chunk;

import java.util.ArrayList;
import java.util.List;

import com.github.steveice10.opennbt.tag.builtin.Tag;

import net.worldsynth.addon.minecraft.minecraft.IChunk;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilVersion;
import net.worldsynth.addon.minecraft.minecraft.anvil.IChunkStatus;

public abstract class SectionedAnvilChunk<C extends AnvilChunkSection> implements IChunk {
	
	private AnvilVersion anvilVersion;
	private final int chunkX, chunkZ; // Chunk coordinates
	
	private final List<C> chunkSections;
	
	private List<Tag> entitiesList = new ArrayList<Tag>();

	private IChunkStatus chunkStatus;
	private byte lightPopulated;
	private long inhabitedTime;
	private long lastUpdate;

	public SectionedAnvilChunk(int chunkX, int chunkZ, AnvilVersion anvilVersion) {
		this.chunkX = chunkX;
		this.chunkZ = chunkZ;
		this.anvilVersion = anvilVersion;
		
		chunkSections = new ArrayList<C>(maxSectionY() - minSectionY());
	}

	public AnvilVersion getAnvilVersion() {
		return anvilVersion;
	}

	@Override
	public int getChunkX() {
		return chunkX;
	}

	@Override
	public int getChunkZ() {
		return chunkZ;
	}
	
	@Override
	public int getMinY() {
		return minSectionY()*16;
	}
	
	@Override
	public int getMaxY() {
		return maxSectionY()*16 + 15;
	}
	
	public abstract int minSectionY();
	
	public abstract int maxSectionY();
	
	public int sectionsCount() {
		return chunkSections.size();
	}
	
	public int maxSectionsCount() {
		return maxSectionY() - minSectionY() + 1;
	}
	
	public boolean sectionExists(int sectionY) {
		if (sectionY < minSectionY() || sectionY > maxSectionY()) {
			throw new IndexOutOfBoundsException();
		}
		
		int chunkIndex = sectionY - minSectionY();
		return chunkIndex < chunkSections.size();
	}
	
	public C getSection(int sectionY) {
		if (sectionY < minSectionY() || sectionY > maxSectionY()) {
			throw new IndexOutOfBoundsException();
		}
		
		int sectionIndex = sectionY - minSectionY();
		if (sectionIndex >= chunkSections.size()) {
			// Create the section ensuring the existence of all section under the new section
			for (int i = chunkSections.size(); i <= sectionIndex; i++) {
				chunkSections.add(constructSection((byte) (i + minSectionY())));
			}
		}
		return chunkSections.get(sectionIndex);
	}
	
	public C getSectionByIndex(int sectionIndex) {
		if (sectionIndex < 0 || sectionIndex > maxSectionsCount()) {
			throw new IndexOutOfBoundsException();
		}
		
		if (sectionIndex >= chunkSections.size()) {
			// Create the section ensuring the existence of all section under the new section
			for (int i = chunkSections.size(); i <= sectionIndex; i++) {
				chunkSections.add(constructSection((byte) (i + minSectionY())));
			}
		}
		return chunkSections.get(sectionIndex);
	}
	
	public List<C> getChunkSections() {
		return chunkSections;
	}
	
	public void setChunkSections(C[] chunkSections) {
		this.chunkSections.clear();
		for(C section: chunkSections) {
			this.chunkSections.add(section);
		}
	}
	
	public void setChunkSections(ArrayList<C> chunkSections) {
		this.chunkSections.clear();
		this.chunkSections.addAll(chunkSections);
	}
	
	protected abstract C constructSection(byte sectionY);

	public byte isLightPopulated() {
		return lightPopulated;
	}

	public void setLightPopulated(byte lightPopulated) {
		this.lightPopulated = lightPopulated;
	}

	public List<Tag> getEntitiesList() {
		return entitiesList;
	}

	public void setEntitiesList(List<Tag> entitiesList) {
		this.entitiesList = entitiesList;
	}

	public IChunkStatus getChunkStatus() {
		return chunkStatus;
	}

	public void setChunkStatus(IChunkStatus chunkStatus) {
		this.chunkStatus = chunkStatus;
	}

	public long getInhabitedTime() {
		return inhabitedTime;
	}

	public void setInhabitedTime(long inhabitedTime) {
		this.inhabitedTime = inhabitedTime;
	}

	public long getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(long lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
}
