/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import net.worldsynth.addon.minecraft.minecraft.Difficulty;
import net.worldsynth.addon.minecraft.minecraft.GameMode;
import net.worldsynth.addon.minecraft.minecraft.Generator;
import net.worldsynth.addon.minecraft.minecraft.IWorld;
import net.worldsynth.addon.minecraft.minecraft.anvil.level.Level;

public class AnvilWorld implements IWorld {
	private final File worldDir;
	
	private final HashMap<Integer, AnvilDimension> dimensionsMap = new HashMap<>();
	
	public AnvilWorld(File worldDir) {
		this.worldDir = worldDir;
		worldDir.mkdirs();
	}
	
	@Override
	public AnvilDimension getDimension(int dimensionId) {
		AnvilDimension dimension = dimensionsMap.get(dimensionId);
		if (dimension == null) {
			if (dimensionId == 0) {
				dimension = new AnvilDimension(worldDir);
			}
			else {
				dimension = new AnvilDimension(new File(worldDir, "DIM" + dimensionId));
			}
			
			dimensionsMap.put(dimensionId, dimension);
		}
		
		return dimension;
	}
	
	public void createLevelFile(AnvilVersion anvilVersion, String levelId, Generator generator, long seed, GameMode gameMode, Difficulty difficulty, int spawnX, int spawnY, int spawnZ) throws IOException {
		Level level = anvilVersion.newLevel(levelId, generator, seed, gameMode, difficulty, spawnX, spawnY, spawnZ);
		level.save(new File(worldDir, "level.dat"));
	}
	
//	public void save() throws IOException {
//		for (AnvilDimension dimension: dimensionsMap.values()) {
//			dimension.save();
//		}
//	}
}
