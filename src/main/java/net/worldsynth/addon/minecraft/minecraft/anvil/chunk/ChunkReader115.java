/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.chunk;

import java.util.ArrayList;

import com.github.steveice10.opennbt.tag.builtin.ByteArrayTag;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.ListTag;
import com.github.steveice10.opennbt.tag.builtin.LongArrayTag;
import com.github.steveice10.opennbt.tag.builtin.Tag;

import net.worldsynth.addon.minecraft.biome.MinecraftBiome;
import net.worldsynth.addon.minecraft.biome.MinecraftBiomeProfile;
import net.worldsynth.addon.minecraft.material.MinecraftMaterial;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialState;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilVersion;
import net.worldsynth.addon.minecraft.minecraft.anvil.util.BitArrayUtil;
import net.worldsynth.biome.BiomeRegistry;

public class ChunkReader115 extends AnvilChunkReader<AnvilChunk115> {
	
	protected final AnvilChunk115 chunk;
	protected final AnvilVersion anvilVersion;

	public ChunkReader115(CompoundTag chunkTag) {
		int dataVersion = (int) chunkTag.get(ChunkLoader115.TAG_DATAVERSION).getValue();
		anvilVersion = AnvilVersion.getVersionByDataVersion(dataVersion);
		
		CompoundTag levelCompound = (CompoundTag) chunkTag.get(ChunkLoader115.TAG_LEVEL);
		int posX = (int) levelCompound.get(ChunkLoader115.TAG_XPOS).getValue();
		int posZ = (int) levelCompound.get(ChunkLoader115.TAG_ZPOS).getValue();
		
		chunk = new AnvilChunk115(posX, posZ, anvilVersion);
		
		// Read in chunk materials and biomes
		readMaterialsAndBiomes(levelCompound);
		
		// Read in entities
		ListTag entities = levelCompound.get(ChunkLoader115.TAG_ENTITIES);
		if (entities != null) {
			chunk.setEntitiesList(entities.getValue());
		}
		else {
			chunk.setEntitiesList(new ArrayList<Tag>());
		}
		
		// Read in other chunk information
		chunk.setLastUpdate((long) levelCompound.get(ChunkLoader115.TAG_LASTUPDATE).getValue());
		chunk.setInhabitedTime((long) levelCompound.get(ChunkLoader115.TAG_INHABITEDTIME).getValue());
	}
	
	@Override
	public AnvilChunk115 getChunk() {
		return chunk;
	}
	
	protected void readMaterialsAndBiomes(CompoundTag levelCompound) {
		// Read in chunk sections
		ListTag chunkSections = levelCompound.get(ChunkLoader115.TAG_SECTIONS);
		if (chunkSections.size() > 0) {
			chunk.setChunkSections(getChunkSections(chunkSections));
			
			// Read tile entities and mutate chunk materials
			ListTag tileEntities = levelCompound.get(ChunkLoader115.TAG_TILEENTITIES);
			if (tileEntities != null) {
				mutateMaterialTileEntities(tileEntities);
			}
			
			// Read tile ticks and mutate chunk materials
			ListTag tileTicks = levelCompound.get(ChunkLoader115.TAG_TILETICKS);
			if (tileTicks != null) {
				mutateMaterialsTileTicks(tileTicks);
			}
			
			// Read liquid ticks and mutate chunk materials
			ListTag liquidTicks = levelCompound.get(ChunkLoader115.TAG_LIQUIDTICKS);
			if (liquidTicks != null) {
				mutateMaterialsLiquidTicks(liquidTicks);
			}
		}
		else {
			// There are no chunk sections, do not read materials
			chunk.setChunkSections(new AnvilChunkSection112[0]);
		}
		
		// Read in biomes
		Tag biomes = levelCompound.get(ChunkLoader115.TAG_BIOMES);
		if (biomes != null) {
			chunk.setBiomesArray(getBiomes(biomes));
		}
		else {
			// There is no biome data, do not read biomes
			chunk.setBiomesArray(new MinecraftBiome[256]);
		}
	}
	
	protected AnvilChunkSection112[] getChunkSections(Tag sectionsTag) {
		ListTag sectionsListTag = (ListTag) sectionsTag;
		int sectionsCount = Math.min(16, sectionsListTag.size()-1);
		AnvilChunkSection112[] chunkSections = new AnvilChunkSection112[sectionsCount];
		
		for (int i = 0; i < sectionsCount; i++) {
			CompoundTag chunkSectionCompound = (CompoundTag) sectionsListTag.get(i+1);
			chunkSections[i] = chunkSectionFromNbt(chunkSectionCompound, anvilVersion);
		}
		return chunkSections;
	}
	
	protected AnvilChunkSection112 chunkSectionFromNbt(CompoundTag chunkSectionCompound, AnvilVersion anvilVersion) {
		byte sectionY = (byte) chunkSectionCompound.get(ChunkLoader115.TAG_Y).getValue();
		AnvilChunkSection112 chunkSection = new AnvilChunkSection112(sectionY);
		
		if (chunkSectionCompound.contains(ChunkLoader115.TAG_BLOCKLIGHT)) {
			chunkSection.setBlockLightsArray(((ByteArrayTag) chunkSectionCompound.get(ChunkLoader115.TAG_BLOCKLIGHT)).getValue());
		}
		
		if (chunkSectionCompound.contains(ChunkLoader115.TAG_SKYLIGHT)) {
			chunkSection.setSkyLightsArray(((ByteArrayTag) chunkSectionCompound.get(ChunkLoader115.TAG_SKYLIGHT)).getValue());
		}
		
		if (chunkSectionCompound.contains(ChunkLoader115.TAG_BLOCKSTATES)) {
			MaterialPalette palette = new MaterialPalette(anvilVersion, (ListTag) chunkSectionCompound.get(ChunkLoader115.TAG_PALETTE));
			long[] blockStatesBitArray = ((LongArrayTag) chunkSectionCompound.get(ChunkLoader115.TAG_BLOCKSTATES)).getValue();
			int bitsPerEntry = (blockStatesBitArray.length * 64) / 4096;
			int[] blockStates = BitArrayUtil.intArrayFromLongBitArray(bitsPerEntry, 4096, blockStatesBitArray);
			
			MinecraftMaterialState[] materials = new MinecraftMaterialState[4096];
			for (int i = 0; i < 4096; i++) {
				materials[i] = palette.getMaterialState(blockStates[i]);
			}
			chunkSection.setMaterialsArray(materials);
		}
		else {
			MinecraftMaterialState[] materials = new MinecraftMaterialState[4096];
			for (int i = 0; i < 4096; i++) {
				materials[i] = MinecraftMaterial.AIR.getDefaultState();
			}
			chunkSection.setMaterialsArray(materials);
		}
		
		return chunkSection;
	}
	
	protected MinecraftBiome[] getBiomes(Tag biomesTag) {
		int[] biomes = (int[]) biomesTag.getValue();
		MinecraftBiomeProfile mcBiomeProfile = (MinecraftBiomeProfile) BiomeRegistry.getProfile("minecraft");
		MinecraftBiome[] biomesArray = new MinecraftBiome[1024];
		for (int i = 0; i < biomes.length; i++) {
			biomesArray[i] = mcBiomeProfile.getNumericLookup(anvilVersion.getVersionName()).getBiome(biomes[i]);
		}
		return biomesArray;
	}
}
