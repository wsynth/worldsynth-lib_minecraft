/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import net.worldsynth.addon.minecraft.minecraft.anvil.chunk.*;
import net.worldsynth.addon.minecraft.minecraft.anvil.level.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.worldsynth.addon.minecraft.minecraft.Difficulty;
import net.worldsynth.addon.minecraft.minecraft.GameMode;
import net.worldsynth.addon.minecraft.minecraft.Generator;

public enum AnvilVersion {
	
	// Unknown version
	VERSION_NA("NA", -1, null, null),
	// 1.9
//	VERSION_1_9("1.9", 169),
//	VERSION_1_9_1("1.9.1", 175),
//	VERSION_1_9_2("1.9.2", 176),
//	VERSION_1_9_3("1.9.3", 183),
//	VERSION_1_9_4("1.9.4", 184),
	// 1.10
//	VERSION_1_10("1.10", 510),
//	VERSION_1_10_1("1.10.1", 511),
//	VERSION_1_10_2("1.10.2", 512),
	// 1.11
//	VERSION_1_11("1.11", 819),
//	VERSION_1_11_1("1.11.1", 921),
//	VERSION_1_11_2("1.11.2", 922),
	// 1.12
	VERSION_1_12("1.12", 1139, ChunkLoader112.class, Level112.class),
	VERSION_1_12_1("1.12.1", 1241, ChunkLoader112.class, Level112.class),
	VERSION_1_12_2("1.12.2", 1343, ChunkLoader112.class, Level112.class),
	// 1.13
	VERSION_1_13("1.13", 1519, ChunkLoader113.class, Level113.class),
	VERSION_1_13_1("1.13.1", 1628, ChunkLoader113.class, Level113.class),
	VERSION_1_13_2("1.13.2", 1631, ChunkLoader113.class, Level113.class),
	// 1.14
	VERSION_1_14("1.14", 1952, ChunkLoader114.class, Level114.class),
	VERSION_1_14_1("1.14.1", 1957, ChunkLoader114.class, Level114.class),
	VERSION_1_14_2("1.14.2", 1963, ChunkLoader114.class, Level114.class),
	VERSION_1_14_3("1.14.3", 1968, ChunkLoader114.class, Level114.class),
	VERSION_1_14_4("1.14.4", 1976, ChunkLoader114.class, Level114.class),
	// 1.15
	VERSION_1_15("1.15", 2225, ChunkLoader115.class, Level115.class),
	VERSION_1_15_1("1.15.1", 2227, ChunkLoader115.class, Level115.class),
	VERSION_1_15_2("1.15.2", 2230, ChunkLoader115.class, Level115.class),
	// 1.16
	VERSION_1_16("1.16", 2566, ChunkLoader116.class, Level116.class),
	VERSION_1_16_1("1.16.1", 2567, ChunkLoader116.class, Level116.class),
	VERSION_1_16_2("1.16.2", 2578, ChunkLoader116.class, Level116.class),
	VERSION_1_16_3("1.16.3", 2580, ChunkLoader116.class, Level116.class),
	VERSION_1_16_4("1.16.4", 2584, ChunkLoader116.class, Level116.class),
	VERSION_1_16_5("1.16.5", 2586, ChunkLoader116.class, Level116.class),
	// 1.17
	VERSION_1_17("1.17", 2724, ChunkLoader117.class, Level117.class),
	VERSION_1_17_1("1.17.1", 2730, ChunkLoader117.class, Level117.class),
	// 1.18
	VERSION_1_18("1.18", 2860, ChunkLoader118.class, Level118.class),
	VERSION_1_18_1("1.18.1", 2865, ChunkLoader118.class, Level118.class),
	VERSION_1_18_2("1.18.2", 2975, ChunkLoader118.class, Level118.class),
	// 1.19
	VERSION_1_19("1.19", 3105, ChunkLoader119.class, Level119.class),
	VERSION_1_19_1("1.19.1", 3117, ChunkLoader119.class, Level119.class),
	VERSION_1_19_2("1.19.2", 3120, ChunkLoader119.class, Level119.class),
	VERSION_1_19_3("1.19.3", 3218, ChunkLoader119.class, Level119.class),
	VERSION_1_19_4("1.19.4", 3337, ChunkLoader119.class, Level119.class),
	// 1.20
	VERSION_1_20("1.20", 3463, ChunkLoader120.class, Level120.class),
	VERSION_1_20_1("1.20.1", 3465, ChunkLoader120.class, Level120.class),
	VERSION_1_20_2("1.20.2", 3578, ChunkLoader120.class, Level120.class);
	
	private static final Logger logger = LogManager.getLogger(AnvilVersion.class);
	
	private final String name;
	private final int dataVersion;
	private final Class<? extends AnvilChunkLoader<?>> chunkLoader;
	private final Class<? extends Level> levelClass;
	
	private AnvilVersion(String versionName, int dataVersion, Class<? extends AnvilChunkLoader<?>> chunkLoader, Class<? extends Level> levelClass) {
		this.name = versionName;
		this.dataVersion = dataVersion;
		this.chunkLoader = chunkLoader;
		this.levelClass = levelClass;
	}
	
	public String getVersionName() {
		return name;
	}
	
	// TODO Remove AnvilVersion getMaterialVersionName(), just use getVersionName()
	public String getMaterialVersionName() {
		return name;
	}
	
	public int getDataVersion() {
		return dataVersion;
	}
	
	
	@SuppressWarnings("rawtypes")
	public AnvilChunkLoader getNewChunkLoader() {
		Constructor<? extends AnvilChunkLoader> loaderConstructor;
		try {
			loaderConstructor = chunkLoader.getConstructor();
			return loaderConstructor.newInstance();
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Level newLevel(String levelName, Generator generator, long seed, GameMode gameMode, Difficulty difficulty, int spawnX, int spawnY, int spawnZ) {
		try {
			if (levelClass == Level112.class) {
				return new Level112(levelName, this, generator, seed, gameMode, difficulty, spawnX, spawnY, spawnZ);
			}
			else if (levelClass == Level113.class) {
				return new Level113(levelName, this, generator, seed, gameMode, difficulty, spawnX, spawnY, spawnZ);
			}
			else if (levelClass == Level114.class) {
				return new Level114(levelName, this, generator, seed, gameMode, difficulty, spawnX, spawnY, spawnZ);
			}
			else if (levelClass == Level115.class) {
				return new Level115(levelName, this, generator, seed, gameMode, difficulty, spawnX, spawnY, spawnZ);
			}
			else if (levelClass == Level116.class) {
				return new Level116(levelName, this, generator, seed, gameMode, difficulty, spawnX, spawnY, spawnZ);
			}
			else if (levelClass == Level117.class) {
				return new Level117(levelName, this, generator, seed, gameMode, difficulty, spawnX, spawnY, spawnZ);
			}
			else if (levelClass == Level118.class) {
				return new Level118(levelName, this, generator, seed, gameMode, difficulty, spawnX, spawnY, spawnZ);
			}
			else if (levelClass == Level119.class) {
				return new Level119(levelName, this, generator, seed, gameMode, difficulty, spawnX, spawnY, spawnZ);
			}
			else if (levelClass == Level120.class) {
				return new Level120(levelName, this, generator, seed, gameMode, difficulty, spawnX, spawnY, spawnZ);
			}
			else {
				throw new RuntimeException("Level construction is not implemented for " + levelClass.getSimpleName());
			}
		} catch (IOException e) {
			//TODO Handle this exception differently
			logger.error("Could not create chunk loader", e);
		}
		return null;
	}
	
	public static AnvilVersion getVersionByDataVersion(int dataVersion) {
		for (AnvilVersion v: values()) {
			if (dataVersion <= v.getDataVersion()) {
				return v;
			}
		}
		return values()[values().length-1];
	}
}
