/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft;

import net.worldsynth.addon.minecraft.biome.MinecraftBiome;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialState;

public interface IDimension {
	
	public MinecraftMaterialState getMaterial(int x, int y, int z);
	public void setMaterial(int x, int y, int z, MinecraftMaterialState material);
	
	public MinecraftBiome getBiome(int x, int z);
	public void setBiome(int x, int z, MinecraftBiome biome);
	
	public MinecraftBiome getBiome(int x, int y, int z);
	public void setBiome(int x, int y, int z, MinecraftBiome biome);
	
	public byte getBlockLight(int x, int y, int z);
	public void setBlockLight(int x, int y, int z, byte blockLight);
	
	public byte getSkyLight(int x, int y, int z);
	public void setSkyLight(int x, int y, int z, byte skyLight);
}
