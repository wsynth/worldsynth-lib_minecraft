/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.chunk;

import java.util.ArrayList;
import java.util.HashMap;

import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.ListTag;
import com.github.steveice10.opennbt.tag.builtin.StringTag;
import com.github.steveice10.opennbt.tag.builtin.Tag;

import net.worldsynth.addon.minecraft.biome.MinecraftBiome;
import net.worldsynth.addon.minecraft.biome.MinecraftBiomeBuilder;
import net.worldsynth.addon.minecraft.biome.MinecraftBiomeProfile;
import net.worldsynth.addon.minecraft.biome.MinecraftBiomeProfile.FlattenedLookup;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilVersion;
import net.worldsynth.biome.BiomeRegistry;

public class BiomePalette {
	
	private AnvilVersion anvilVersion;
	
	private ArrayList<MinecraftBiome> paletteList = new ArrayList<MinecraftBiome>();
	private HashMap<MinecraftBiome, Integer> paletteMap = new HashMap<MinecraftBiome, Integer>();
	
	public static final String TAG_NAME = "Name";
	
	public BiomePalette(AnvilVersion anvilVersion) {
		this.anvilVersion = anvilVersion;
	}
	
	public BiomePalette(AnvilVersion anvilVersion, ListTag paletteListTag) {
		this.anvilVersion = anvilVersion;
		FlattenedLookup flattenedLookup = ((MinecraftBiomeProfile) BiomeRegistry.getProfile("minecraft")).getFlattenedLookup(anvilVersion.getVersionName());
		for (Tag e: paletteListTag.getValue()) {
			StringTag compoundEntry = (StringTag) e;
			String flattenedId = compoundEntry.getValue();
			getPaletteIndex(getMinecraftBiome(flattenedLookup, flattenedId));
		}
	}
	
	private MinecraftBiome getMinecraftBiome(FlattenedLookup flattenedLookup, String flattenedId) {
		MinecraftBiome biome = flattenedLookup.getBiome(flattenedId);
		
		if (biome == null) {
			MinecraftBiomeBuilder biomeBuilder = new MinecraftBiomeBuilder(flattenedId, "Unknown " + flattenedId);
			biome = biomeBuilder.createBiome();
		}
		
		return biome;
	}
	
	public int getPaletteIndex(MinecraftBiome biome) {
		if (!paletteMap.containsKey(biome)) {
			paletteMap.put(biome, paletteList.size());
			paletteList.add(biome);
			CompoundTag materialPaletteEntry = new CompoundTag("");
			materialPaletteEntry.put(new StringTag(TAG_NAME, biome.getFlattenedAnvilId(anvilVersion.getVersionName())));
		}
		return paletteMap.get(biome);
	}
	
	public MinecraftBiome getBiome(int paletteIndex) {
		return paletteList.get(paletteIndex);
	}
	
	public int getPaletteSize() {
		return paletteList.size();
	}
	
	public ListTag toNbt(String tagName) {
		ListTag paletteListTag = new ListTag(tagName);
		
		for (MinecraftBiome b: paletteList) {
			paletteListTag.add(new StringTag(TAG_NAME, b.getFlattenedAnvilId(anvilVersion.getVersionName())));
		}
		
		return paletteListTag;
	}
}
