/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft;

public enum WorldType {
	DEFAULT("default"),
	LARGEBIOMES("largeBiomes"),
	FLAT("flat", SuperflatOptions.FLAT),
	AMPLIFIED("amplified"),
//	WORLDSYNTH("worldsynth"),
	CUSTOM("custom"),
	VOID("flat", SuperflatOptions.VOID);
	
	private final String name;
	private final SuperflatOptions options;
	
	private WorldType(String name) {
		this.name = name;
		this.options = null;
	}
	
	private WorldType(String name, SuperflatOptions options) {
		this.name = name;
		this.options = options;
	}
	
	public String getName() {
		return name;
	}
	
	public SuperflatOptions getOptions() {
		return options;
	}
}
