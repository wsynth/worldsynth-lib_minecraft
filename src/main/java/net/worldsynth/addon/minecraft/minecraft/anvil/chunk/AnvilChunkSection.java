/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.chunk;

public abstract class AnvilChunkSection {
	
	protected final byte sectionY;
	
	public AnvilChunkSection(byte sectionY) {
		this.sectionY = sectionY;
	}
	
	public final byte getSectionY() {
		return sectionY;
	}
}
