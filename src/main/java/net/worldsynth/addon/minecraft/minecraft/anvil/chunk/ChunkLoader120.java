/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.chunk;

import com.github.steveice10.opennbt.tag.builtin.CompoundTag;

public class ChunkLoader120 implements AnvilChunkLoader<AnvilChunk118> {

	// Chunk tags
	public static final String TAG_CHUNK = "Chunk";
	public static final String TAG_DATAVERSION = "DataVersion";
	
	public static final String TAG_STATUS = "Status";

	public static final String TAG_XPOS = "xPos";
	public static final String TAG_YPOS = "yPos";
	public static final String TAG_ZPOS = "zPos";

	public static final String TAG_LASTUPDATE = "LastUpdate";
	public static final String TAG_INHABITEDTIME = "InhabitedTime";

	public static final String TAG_SECTIONS = "sections";
	public static final String TAG_ENTITIES = "entities";
	public static final String TAG_BLOCKENTITIES = "block_entities";
	public static final String TAG_BLOCKTICKS = "block_ticks";
	public static final String TAG_FLUIDTICKS = "fluid_ticks";

	public static final String TAG_LIGHTS = "Lights";
	public static final String TAG_POSTPROCESSING = "PostProcessing";
	
	public static final String TAG_STRUCTURES = "structures";
	public static final String TAG_STRUCTURES_REFERENCES = "References";
	public static final String TAG_STRUCTURES_STARTS = "starts";
	
	// Chunk section tags
	public static final String TAG_SECTION = "Section";
	public static final String TAG_Y = "Y";
	public static final String TAG_BLOCKLIGHT = "BlockLight";
	public static final String TAG_SKYLIGHT = "SkyLight";

	public static final String TAG_BLOCKSTATES = "block_states";
	public static final String TAG_BLOCKSTATES_DATA = "data";
	public static final String TAG_BIOMES = "biomes";
	public static final String TAG_BIOMES_DATA = "data";
	public static final String TAG_PALETTE = "palette";

	@Override
	public CompoundTag chunkToNbt(AnvilChunk118 chunk) {
		return new ChunkWriter118(chunk).getChunkCompund();
	}

	@Override
	public SectionedAnvilChunk<?> chunkFromNbt(CompoundTag chunkTag) {
		return new ChunkReader118(chunkTag).getChunk();
	}
}
