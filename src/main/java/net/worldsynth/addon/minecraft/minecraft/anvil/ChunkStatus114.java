/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil;

public enum ChunkStatus114 implements IChunkStatus {
	EMPTY("empty"),
	STRUCTURE_STARTS("structure_starts"),
	STRUCTURE_REFERENCES("structure_references"),
	BIOMES("biomes"),
	NOISE("noise"),
	SURFACE("surface"),
	CARVERS("carvers"),
	LIQUID_CARVERS("liquid_carvers"),
	FEATURES("features"),
	LIGHT("light"),
	SPAWN("spawn"),
	HEIGHTMAPS("heightmaps"),
	FULL("full");

	private final String name;

	private ChunkStatus114(String name) {
		this.name = name;
	}
	
	@Override
	public String getName() {
		return name;
	}
}
