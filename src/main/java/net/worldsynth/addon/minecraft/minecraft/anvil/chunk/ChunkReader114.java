/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.chunk;

import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.ListTag;
import com.github.steveice10.opennbt.tag.builtin.Tag;

public class ChunkReader114 extends ChunkReader113 {

	public ChunkReader114(CompoundTag chunkTag) {
		super(chunkTag);
	}
	
	@Override
	protected AnvilChunkSection112[] getChunkSections(Tag sectionsTag) {
		ListTag sectionsListTag = (ListTag) sectionsTag;
		int sectionsCount = Math.min(16, sectionsListTag.size()-1);
		AnvilChunkSection112[] chunkSections = new AnvilChunkSection112[sectionsCount];
		
		for (int i = 0; i < sectionsCount; i++) {
			CompoundTag chunkSectionCompound = (CompoundTag) sectionsListTag.get(i+1);
			chunkSections[i] = chunkSectionFromNbt(chunkSectionCompound, anvilVersion);
		}
		return chunkSections;
	}
}
