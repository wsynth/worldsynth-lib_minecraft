/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.chunk;

import java.util.ArrayList;

import com.github.steveice10.opennbt.tag.builtin.ByteArrayTag;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.ListTag;
import com.github.steveice10.opennbt.tag.builtin.LongArrayTag;
import com.github.steveice10.opennbt.tag.builtin.Tag;

import net.worldsynth.addon.minecraft.biome.MinecraftBiome;
import net.worldsynth.addon.minecraft.material.MinecraftMaterial;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialState;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilVersion;
import net.worldsynth.addon.minecraft.minecraft.anvil.util.WastefulBitArrayUtil;

public class ChunkReader118 extends AnvilChunkReader<AnvilChunk118> {
	
	protected final AnvilChunk118 chunk;
	protected final AnvilVersion anvilVersion;

	public ChunkReader118(CompoundTag chunkTag) {
		int dataVersion = (int) chunkTag.get(ChunkLoader118.TAG_DATAVERSION).getValue();
		anvilVersion = AnvilVersion.getVersionByDataVersion(dataVersion);
		
		int posX = (int) chunkTag.get(ChunkLoader118.TAG_XPOS).getValue();
		int posZ = (int) chunkTag.get(ChunkLoader118.TAG_ZPOS).getValue();
		
		int posY = (int) chunkTag.get(ChunkLoader118.TAG_YPOS).getValue();
		
		chunk = new AnvilChunk118(posX, posZ, anvilVersion);
		
		// Read in chunk materials and biomes
		readMaterialsAndBiomes(chunkTag);
		
		// Read in entities
		ListTag entities = chunkTag.get(ChunkLoader118.TAG_ENTITIES);
		if (entities != null) {
			chunk.setEntitiesList(entities.getValue());
		}
		else {
			chunk.setEntitiesList(new ArrayList<Tag>());
		}
		
		// Read in other chunk information
		if (chunkTag.contains(ChunkLoader118.TAG_LASTUPDATE)) {
			chunk.setLastUpdate((long) chunkTag.get(ChunkLoader118.TAG_LASTUPDATE).getValue());
		}
		if (chunkTag.contains(ChunkLoader118.TAG_INHABITEDTIME)) {
			chunk.setInhabitedTime((long) chunkTag.get(ChunkLoader118.TAG_INHABITEDTIME).getValue());
		}
	}
	
	@Override
	public AnvilChunk118 getChunk() {
		return chunk;
	}
	
	protected void readMaterialsAndBiomes(CompoundTag levelCompound) {
		// Read in chunk sections
		ListTag chunkSections = levelCompound.get(ChunkLoader118.TAG_SECTIONS);
		if (chunkSections.size() > 0) {
			chunk.setChunkSections(getChunkSections(chunkSections));
			
			// Read tile entities and mutate chunk materials
			ListTag tileEntities = levelCompound.get(ChunkLoader118.TAG_BLOCKENTITIES);
			if (tileEntities != null) {
				mutateMaterialTileEntities(tileEntities);
			}
			
			// Read tile ticks and mutate chunk materials
			ListTag tileTicks = levelCompound.get(ChunkLoader118.TAG_BLOCKTICKS);
			if (tileTicks != null) {
				mutateMaterialsTileTicks(tileTicks);
			}
			
			// Read liquid ticks and mutate chunk materials
			ListTag liquidTicks = levelCompound.get(ChunkLoader118.TAG_FLUIDTICKS);
			if (liquidTicks != null) {
				mutateMaterialsLiquidTicks(liquidTicks);
			}
		}
		else {
			// There are no chunk sections, do not read materials
			chunk.setChunkSections(new AnvilChunkSection118[0]);
		}
	}
	
	protected AnvilChunkSection118[] getChunkSections(Tag sectionsTag) {
		ListTag sectionsListTag = (ListTag) sectionsTag;
		int sectionsCount = sectionsListTag.size();
		AnvilChunkSection118[] chunkSections = new AnvilChunkSection118[sectionsCount];
		
		for (int i = 0; i < sectionsCount; i++) {
			CompoundTag chunkSectionCompound = (CompoundTag) sectionsListTag.get(i);
			chunkSections[i] = chunkSectionFromNbt(chunkSectionCompound, anvilVersion);
		}
		return chunkSections;
	}
	
	protected AnvilChunkSection118 chunkSectionFromNbt(CompoundTag chunkSectionCompound, AnvilVersion anvilVersion) {
		byte sectionY = (byte) chunkSectionCompound.get(ChunkLoader118.TAG_Y).getValue();
		AnvilChunkSection118 chunkSection = new AnvilChunkSection118(sectionY);
		
		// Block light
		if (chunkSectionCompound.contains(ChunkLoader118.TAG_BLOCKLIGHT)) {
			chunkSection.setBlockLightsArray(((ByteArrayTag) chunkSectionCompound.get(ChunkLoader118.TAG_BLOCKLIGHT)).getValue());
		}
		
		// Sky light
		if (chunkSectionCompound.contains(ChunkLoader118.TAG_SKYLIGHT)) {
			chunkSection.setSkyLightsArray(((ByteArrayTag) chunkSectionCompound.get(ChunkLoader118.TAG_SKYLIGHT)).getValue());
		}
		
		// Block states
		if (chunkSectionCompound.contains(ChunkLoader118.TAG_BLOCKSTATES)) {
			CompoundTag blockStatesCompound = (CompoundTag) chunkSectionCompound.get(ChunkLoader118.TAG_BLOCKSTATES);
			
			MaterialPalette materialsPalette = new MaterialPalette(anvilVersion, (ListTag) blockStatesCompound.get(ChunkLoader118.TAG_PALETTE));
			if (materialsPalette.getPaletteSize() > 1) {
				long[] blockStatesBitArray = ((LongArrayTag) blockStatesCompound.get(ChunkLoader118.TAG_BLOCKSTATES_DATA)).getValue();
				int bitsPerEntry = (blockStatesBitArray.length * 64) / 4096;
				int[] materialIndexes = WastefulBitArrayUtil.intArrayFromLongBitArray(bitsPerEntry, 4096, blockStatesBitArray);
				
				MinecraftMaterialState[] materials = new MinecraftMaterialState[4096];
				for (int i = 0; i < 4096; i++) {
					materials[i] = materialsPalette.getMaterialState(materialIndexes[i]);
				}
				chunkSection.setMaterialsArray(materials);
			}
			else {
				MinecraftMaterialState[] materials = new MinecraftMaterialState[4096];
				for (int i = 0; i < 4096; i++) {
					materials[i] = materialsPalette.getMaterialState(0);
				}
				chunkSection.setMaterialsArray(materials);
			}
		}
		else {
			MinecraftMaterialState[] materials = new MinecraftMaterialState[4096];
			for (int i = 0; i < materials.length; i++) {
				materials[i] = MinecraftMaterial.AIR.getDefaultState();
			}
			chunkSection.setMaterialsArray(materials);
		}
		
		// Biomes
		if (chunkSectionCompound.contains(ChunkLoader118.TAG_BIOMES)) {
			CompoundTag biomesCompound = (CompoundTag) chunkSectionCompound.get(ChunkLoader118.TAG_BIOMES);
			
			BiomePalette biomesPalette = new BiomePalette(anvilVersion, (ListTag) biomesCompound.get(ChunkLoader118.TAG_PALETTE));
			if (biomesPalette.getPaletteSize() > 1) {
				long[] biomesBitArray = ((LongArrayTag) biomesCompound.get(ChunkLoader118.TAG_BIOMES_DATA)).getValue();
				int bitsPerEntry = (biomesBitArray.length * 64) / 64;
				int[] biomeIndexes = WastefulBitArrayUtil.intArrayFromLongBitArray(bitsPerEntry, 64, biomesBitArray);
				
				MinecraftBiome[] biomes = new MinecraftBiome[64];
				for (int i = 0; i < biomes.length; i++) {
					biomes[i] = biomesPalette.getBiome(biomeIndexes[i]);
				}
				chunkSection.setBiomesArray(biomes);
			}
			else {
				MinecraftBiome[] biomes = new MinecraftBiome[64];
				for (int i = 0; i < biomes.length; i++) {
					biomes[i] = biomesPalette.getBiome(0);
				}
				chunkSection.setBiomesArray(biomes);
			}
		}
		else {
			MinecraftBiome[] biomes = new MinecraftBiome[64];
			for (int i = 0; i < biomes.length; i++) {
				biomes[i] = MinecraftBiome.UNKNOWN;
			}
			chunkSection.setBiomesArray(biomes);
		}
		
		return chunkSection;
	}
}
