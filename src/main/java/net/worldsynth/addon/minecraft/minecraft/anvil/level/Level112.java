/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.level;

import java.io.IOException;

import com.github.steveice10.opennbt.tag.builtin.ByteTag;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.IntTag;
import com.github.steveice10.opennbt.tag.builtin.LongTag;
import com.github.steveice10.opennbt.tag.builtin.StringTag;

import net.worldsynth.addon.minecraft.minecraft.Difficulty;
import net.worldsynth.addon.minecraft.minecraft.GameMode;
import net.worldsynth.addon.minecraft.minecraft.Generator;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilVersion;

public class Level112 extends Level {

	public Level112(String levelName, AnvilVersion anvilVersion, Generator generator, long seed, GameMode gameMode, Difficulty difficulty, int spawnX, int spawnY, int spawnZ) throws IOException {
		super(levelName, anvilVersion, generator, seed, gameMode, difficulty, spawnX, spawnY, spawnZ);
	}
	
	public Level112(CompoundTag levelTag) {
		super(levelTag);
	}
	
	@Override
	protected String defaultLevelDataResource() {
		return "defaultlevel112.snbt";
	}
	
	@Override
	protected void putGeneratorTags(CompoundTag dataCompound, AnvilVersion anvilVersion, Generator generator, long seed) {
		dataCompound.put(new LongTag(TAG_RANDOM_SEED, seed));
		
		dataCompound.put(new StringTag(TAG_GENERATOR_NAME, generator.getWorldTypeName()));
		dataCompound.put(new IntTag(TAG_GENERATOR_VERSION, generator.getGeneratorVersion()));
		
		dataCompound.put(new ByteTag(TAG_MAP_FEATURES, (byte) 0));
		
		if (generator.getSuperflatOptions() != null) {
			dataCompound.put(new StringTag(TAG_GENERATOR_OPTIONS, generator.getSuperflatOptions().optionsString112(anvilVersion)));
		}
		else {
			dataCompound.put(new StringTag(TAG_GENERATOR_OPTIONS, ""));
		}
	}
}
