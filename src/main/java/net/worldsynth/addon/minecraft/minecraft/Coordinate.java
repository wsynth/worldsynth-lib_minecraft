/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft;

public class Coordinate {
	public final int x, z;
	
	public Coordinate(int x, int z) {
		this.x = x;
		this.z = z;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj instanceof Coordinate) {
			Coordinate castObj = (Coordinate) obj;
			return castObj.x == x && castObj.z == z;
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return x * 31 + z;
	}
}
