/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.chunk;

import java.util.ArrayList;
import java.util.List;

import com.github.steveice10.opennbt.tag.builtin.ByteTag;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.IntTag;
import com.github.steveice10.opennbt.tag.builtin.ListTag;
import com.github.steveice10.opennbt.tag.builtin.LongTag;
import com.github.steveice10.opennbt.tag.builtin.ShortTag;
import com.github.steveice10.opennbt.tag.builtin.StringTag;

import net.worldsynth.addon.minecraft.minecraft.anvil.ChunkStatus114;

public class ChunkWriter114 extends ChunkWriter113 {

	public ChunkWriter114(AnvilChunk113 chunk) {
		super(chunk);
	}
	
	@Override
	protected CompoundTag levelCompound() {
		CompoundTag levelCompound = new CompoundTag(ChunkLoader113.TAG_LEVEL);
		levelCompound.put(new IntTag(ChunkLoader113.TAG_XPOS, chunk.getChunkX()));
		levelCompound.put(new IntTag(ChunkLoader113.TAG_ZPOS, chunk.getChunkZ()));
		
		levelCompound.put(new LongTag(ChunkLoader113.TAG_INHABITEDTIME, chunk.getInhabitedTime()));
		levelCompound.put(new LongTag(ChunkLoader113.TAG_LASTUPDATE, chunk.getLastUpdate()));
		
		levelCompound.put(new ListTag(ChunkLoader113.TAG_ENTITIES, chunk.getEntitiesList()));
		
		levelCompound.put(createTileEntitiesListTag(ChunkLoader113.TAG_TILEENTITIES));
		levelCompound.put(createTileTicksListTag(ChunkLoader113.TAG_TILETICKS));
		levelCompound.put(createLiquidTicksListTag(ChunkLoader113.TAG_LIQUIDTICKS));

		levelCompound.put(new StringTag(ChunkLoader113.TAG_STATUS, chunk.getChunkStatus().getName()));
		
		ChunkStatus114 chunkStatus = (ChunkStatus114) chunk.getChunkStatus();
		List<List<Short>> chunkLightsLists = chunkStatus.compareTo(ChunkStatus114.FULL) >= 0 ? null : new ArrayList<List<Short>>();
		levelCompound.put(chunkSectionsList(chunkLightsLists));
		
		if (chunkLightsLists != null) {
			ListTag chunkLightsListsTag = new ListTag(ChunkLoader113.TAG_LIGHTS);
			for (List<Short> sectionLightsList: chunkLightsLists) {
				ListTag sectionLightsListTag = new ListTag("");
				for (short s: sectionLightsList) {
					sectionLightsListTag.add(new ShortTag("", s));
				}
				chunkLightsListsTag.add(sectionLightsListTag);
			}
			levelCompound.put(chunkLightsListsTag);
		}
		
		levelCompound.put(biomesArray());
//		levelCompound.put(heightmaps());
		
		return levelCompound;
	}
	
	@Override
	protected ListTag chunkSectionsList(List<List<Short>> chunkLightsLists) {
		ListTag sectionsListTag = new ListTag(ChunkLoader113.TAG_SECTIONS);
		
		// Add the "Y: -1" section
		CompoundTag ym1 = new CompoundTag(ChunkLoader113.TAG_SECTION);
		ym1.put(new ByteTag(ChunkLoader113.TAG_Y, (byte) -1));
		sectionsListTag.add(ym1);
		
		// Add all the other sections
		for (AnvilChunkSection112 section: chunk.getChunkSections()) {
			List<Short> sectionLightsList = chunkLightsLists == null ? null : new ArrayList<Short>();
			sectionsListTag.add(chunkSectionToNbt(section, sectionLightsList));
			
			if (sectionLightsList != null) {
				chunkLightsLists.add(sectionLightsList);
			}
		}

		return sectionsListTag;
	}
}
