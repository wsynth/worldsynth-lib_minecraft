/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.chunk;

import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.ListTag;

import net.worldsynth.addon.minecraft.material.MinecraftMaterialState;
import net.worldsynth.addon.minecraft.material.MutatedMinecraftMaterialStateBuilder;

public abstract class AnvilChunkReader<C extends SectionedAnvilChunk<?>> {
	
	public abstract C getChunk();
	
	
	/*=============================================================
	 * 
	 * Tile entities and ticks stuff
	 * 
	 *=============================================================*/
	
	/**
	 * Mutates materials in a chunk with tile entities
	 * 
	 * @param chunk
	 * @param tileEntities
	 */
	protected void mutateMaterialTileEntities(ListTag tileEntities) {
		for (int i = 0; i < tileEntities.size(); i++) {
			CompoundTag tileEntity = (CompoundTag) tileEntities.get(i);
			int x = (int) tileEntity.get("x").getValue();
			int y = (int) tileEntity.get("y").getValue();
			int z = (int) tileEntity.get("z").getValue();
			
			x = x & 15;
			z = z & 15;
			
//			// Remove tile entity id tag
//			tileEntity.remove("id");
			
			MinecraftMaterialState material = getChunk().getMaterial(x, y, z);
			MutatedMinecraftMaterialStateBuilder mutationBuiler = new MutatedMinecraftMaterialStateBuilder(material);
			mutationBuiler.setTileEntity(tileEntity);
			material = mutationBuiler.createMaterialState();
			
			getChunk().setMaterial(x, y, z, material);
		}
	}
	
	/**
	 * Mutates materials in a chunk with tile ticks
	 * 
	 * @param chunk
	 * @param tileTicks
	 */
	protected void mutateMaterialsTileTicks(ListTag tileTicks) {
		for (int i = 0; i < tileTicks.size(); i++) {
			CompoundTag tileTick = (CompoundTag) tileTicks.get(i);
			int x = (int) tileTick.get("x").getValue();
			int y = (int) tileTick.get("y").getValue();
			int z = (int) tileTick.get("z").getValue();
			
			x = x & 15;
			z = z & 15;
			
//			// Remove tile tick id tag
//			tileTick.remove("i");
			
			MinecraftMaterialState material = getChunk().getMaterial(x, y, z);
			MutatedMinecraftMaterialStateBuilder mutationBuiler = new MutatedMinecraftMaterialStateBuilder(material);
			mutationBuiler.setTileTick(tileTick);
			material = mutationBuiler.createMaterialState();
			
			getChunk().setMaterial(x, y, z, material);
		}
	}
	
	/**
	 * Mutates materials in a chunk with liquid ticks
	 * 
	 * @param chunk
	 * @param liquidTicks
	 */
	protected void mutateMaterialsLiquidTicks(ListTag liquidTicks) {
		for (int i = 0; i < liquidTicks.size(); i++) {
			CompoundTag liquidTick = (CompoundTag) liquidTicks.get(i);
			int x = (int) liquidTick.get("x").getValue();
			int y = (int) liquidTick.get("y").getValue();
			int z = (int) liquidTick.get("z").getValue();
			
			x = x & 15;
			z = z & 15;
			
//			// Remove tile tick id tag
//			tileTick.remove("i");
			
			MinecraftMaterialState material = getChunk().getMaterial(x, y, z);
			MutatedMinecraftMaterialStateBuilder mutationBuiler = new MutatedMinecraftMaterialStateBuilder(material);
			mutationBuiler.setLiquidTick(liquidTick);
			material = mutationBuiler.createMaterialState();
			
			getChunk().setMaterial(x, y, z, material);
		}
	}
}
