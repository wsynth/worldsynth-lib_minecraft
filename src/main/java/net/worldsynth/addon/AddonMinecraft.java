/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon;

import net.worldsynth.WorldSynthDirectoryConfig;
import net.worldsynth.addon.minecraft.MinecraftModuleRegister;
import net.worldsynth.addon.minecraft.biome.MinecraftBiomeProfile;
import net.worldsynth.addon.minecraft.customobject.Bo2CustomObjectFormat;
import net.worldsynth.addon.minecraft.customobject.NbtStructureCustomobjectFormat;
import net.worldsynth.addon.minecraft.customobject.SchematicCustomobjectFormat;
import net.worldsynth.addon.minecraft.customobject.SpongeSchematicCustomobjectFormat;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialProfile;
import net.worldsynth.biome.BiomeProfile;
import net.worldsynth.biome.addon.IBiomeAddon;
import net.worldsynth.customobject.CustomObjectFormat;
import net.worldsynth.customobject.addon.ICustomObjectFormatAddon;
import net.worldsynth.editor.WorldSynthEditorApp;
import net.worldsynth.material.MaterialProfile;
import net.worldsynth.material.addon.IMaterialAddon;
import net.worldsynth.module.AbstractModuleRegister;
import net.worldsynth.module.addon.IModuleAddon;

import java.util.ArrayList;
import java.util.List;

public class AddonMinecraft implements IAddon, IModuleAddon, ICustomObjectFormatAddon, IMaterialAddon, IBiomeAddon {

	public static void main(String[] args) {
		WorldSynthEditorApp.startEditor(args, new AddonMinecraft());
	}

	@Override
	public void initAddon(WorldSynthDirectoryConfig directoryConfig) {
	}

	@Override
	public List<AbstractModuleRegister> getAddonModuleRegisters() {
		ArrayList<AbstractModuleRegister> moduleRegisters = new ArrayList<>();
		moduleRegisters.add(new MinecraftModuleRegister());
		return moduleRegisters;
	}

	@Override
	public List<CustomObjectFormat> getAddonCustomObjectFormats() {
		ArrayList<CustomObjectFormat> objectFormats = new ArrayList<>();
		objectFormats.add(new Bo2CustomObjectFormat());
		objectFormats.add(new SchematicCustomobjectFormat());
		objectFormats.add(new SpongeSchematicCustomobjectFormat());
		objectFormats.add(new NbtStructureCustomobjectFormat());
		return objectFormats;
	}

	@Override
	public List<MaterialProfile<?, ?>> getAddonMaterialProfiles() {
		ArrayList<MaterialProfile<?, ?>> materialProfiles = new ArrayList<>();
		materialProfiles.add(new MinecraftMaterialProfile());
		return materialProfiles;
	}

	@Override
	public List<BiomeProfile<?>> getAddonBiomeProfiles() {
		ArrayList<BiomeProfile<?>> biomeProfiles = new ArrayList<>();
		biomeProfiles.add(new MinecraftBiomeProfile());
		return biomeProfiles;
	}
}
