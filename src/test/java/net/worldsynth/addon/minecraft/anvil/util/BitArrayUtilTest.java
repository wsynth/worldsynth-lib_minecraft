/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.anvil.util;

import static org.junit.Assert.assertArrayEquals;

import java.util.Arrays;

import org.junit.Test;

import net.worldsynth.addon.minecraft.minecraft.anvil.util.BitArrayUtil;

public class BitArrayUtilTest {
	
	@Test
	public void toAndFromBitArrayShouldBeInverseOfEachOther() {
		//Assert int
		for (int i = 0; i < 64; i++) {
			int[] input = new int[16*16*16];
			Arrays.fill(input, i);
			int bitsPerEntry = Math.max((int) Math.ceil(Math.log(i+1) / Math.log(2)), 4);
			assertArrayEquals("Int array filled with " + i + " should convert back and forth", input, BitArrayUtil.intArrayFromLongBitArray(bitsPerEntry, 16*16*16, BitArrayUtil.toLongBitArray(bitsPerEntry, input)));
		}
	}
}
