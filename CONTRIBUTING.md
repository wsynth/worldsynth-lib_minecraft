# Contributing to WorldSynth

Thank you for your interest in contributing to WorldSynth.
While some contributors help by working fixing bugs and creating new features, it is worth strong emphasis that even non-programmers can help make WorldSynth better. You might already have an idea of something you would want to work on, but if not, here are just a few examples of ways you can help:
- Fix a bug and submit a merge request.
- Chose a feature you want to see developed and make it.
- Help answer questions in forums like our discord or subreddit.
- Make a tutorial or video on how to do something cool with WorldSynth.
- Find a new bug and report it.
- Create an enhancement proposal for a feature you want.

## Making a bug report or a feature suggestion
One very helpful way of contributing whether you can write code or not for the project, is to report bugs or other complications you encounter while using WorldSynth. There are many cases where bugs and undesirable behaviour sneaks into a project without the developers noticing, and you as a user encountering this might be the hero that makes them aware of an issue.
#### BUG
When submitting a bug to the issue tacker, give a description of what the problem is and how to reproduce it so that we can figure out what is causing the bug, and mark the issue with the "Type: Bug" tag.
#### FEATURE
If you want to submit a suggestion for a new feature, give a description of the feature you suggest so that a developer can understand your suggestion, and also give an explanation of how it would be helpful or what problem it solves, preferably with example if it is not too obvious. Mark the issue with the "Type: feature" tag.
#### ENHANCEMENT
If you want to suggest an improvement to an already existing feature, give a description of the improvement you suggest so that a developer can understand your suggestion, and also give an explanation of how it would be helpful or what problem it solves, preferably with example if it is not too obvious. Mark the issue with the "Type: enhancement" tag.

## Contributing code to the project
By contributing code or other resources to a WorldSynth project repository, You accept and agree to the following conditions for Your present and future Contributions submitted to the project. Except for the license granted herein, You reserve all right, title, and interest in and to Your Contributions.
- All Contributions are licensed under the Mozilla Public License Version 2.0 (MPL 2.0), and other licenses if specified in select files or for select project*.

To work on the project as a first-time contributor you will want to fork the repository you want to work on, so that you can make a merge request when you have your contribution working properly.  
Contributors with proven interest in helping development may be granted developer status in the WorldSynth projects or group, which enables you to work in unprotected development branches.

###### *Clarification on purpose of use for other licenses than MPL 2.0:
- Additional licenses may be in use for select source files for the purpose of preserving upstream compatibility for such select source files copied into WorldSynth source file collection from other projects trough compatible licensing.
- Additional licenses may be in use for select projects that have as main purpose to serve as:  
a) examples for how to use WorldSynth as library in larger works.  
b) examples for how to creating addons for WorldSynth.

## Looking for something to work on?
Have a look for interesting issues with the "Status: available" tag and ask any questions you must have about it. If you find something of interest that you want to work on, that is great.  
If you already have something you want to work on that is not in in the issue list, you are welcome to work on that also and submit a merge request, but it may be a good idea to make a suggestion issue on it so that a maintainer can review what you want to do before you get to far if we were to see any potential issues or improvements with it.

## Contribution flow
When contributing code to WorldSynth, you need to submit a merge request to get your change merged, merge requests are subject to review by maintainers (and developers) before it is merged into a production branch, if the target branch for the merge request is an unprotected development branch it may also be merged by a responsible developer for that branch after being subject to their review.  

When we receive a merge request, we really want to get it merged, but there may be times when it will not be merged. If a merge contribution is not merged, the reason for not merging it will be disclosed, and hopefully it can be improved upon so it can be reviewed again and merged later if that is possible, if not the merge request may be closed by a maintainer, or a responsible developer for the target branch.
